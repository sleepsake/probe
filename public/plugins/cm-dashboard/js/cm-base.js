$(document).ready(function() {
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});

	$(window).resize(function() {
		var navbarHeight = $('.navbar').outerHeight();

		// $('.grid-main-content').css({'height': 'calc(100vh - ' + navbarHeight + 'px)', 'margin-top': navbarHeight + 'px'});
	});

	$(window).trigger('resize');

	// FlatPickr
	$('.form-date').flatpickr({
		enableTime: false,
		altInput: true,
		altFormat: "F d, Y",
		dateFormat: "Y-m-d"
	});

	$('.form-time').flatpickr({
		enableTime: true,
		noCalendar: true,
		dateFormat: "h:i K"
	});

	$('.btn-add-field').on("click", function(e){
		e.preventDefault();
		$(this).parent().append('<input type="text" class="form-control"');
	});
});

(function () {
	'use strict';
	window.addEventListener('load', function () {
		// Fetch all the forms we want to apply custom Bootstrap validation styles to
		var forms = document.getElementsByClassName('needs-validation');
		// Loop over them and prevent submission
		var validation = Array.prototype.filter.call(forms, function (form) {
			form.addEventListener('submit', function (event) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();