$(document).ready(function() {
	$('#dtList').DataTable({
		"lengthMenu": [
			[3, 5, 10, 25, 50, 100, -1],
			[3, 5, 10, 25, 50, 100, "All"]
		],
		"columnDefs": [
			{ "orderable": false, "targets": -1 }
		],
		fixedColumns: {
			leftColumns: 2,
			rightColumns: 1
		},
		scrollX: true
	});
});