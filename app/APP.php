<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class APP extends Model
{
    protected $table = 'apps';

    public function PPMPs()
    {
        return $this->hasMany('App\AppPPMP', 'app_id');
    }
}
