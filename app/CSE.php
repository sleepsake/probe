<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CSE extends Model
{
    //
    protected $table = 'cse';
    protected $fillable = [
        'id',
        'product_code',
        'item_description',
        'unit_of_measure',
        'category',
        'price'
    ];
}
