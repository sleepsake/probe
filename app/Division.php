<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    //
    protected $table = 'users_divisions';

    public function get_by_name($name)
    {
        $division = $this->where('name', '=', $name)->first();
        return $division;
    }

    public function get_by_id($id)
    {
        $division = $this->where('id', '=', $id)->first();

        if($division == null)
        {
            return redirect()->route('dropdowns.divisions')->with('pop-error', 'Division is non-existent');
        }

        return $division;
    }
}
