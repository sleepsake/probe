<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PPMPItems extends Model
{
    protected $table = 'ppmp_items';

    public function PPMP()
    {
        return $this->belongsTo('App\PPMP', 'ppmp_id');
    }

    public function PAPItem()
    {
        return $this->belongsTo('App\PAPItems', 'pap_item_id');
    }
}
