<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PAPItems extends Model
{
    protected $guarded = ['id'];
    protected $table = 'pap_items';

    public function Category()
    {
        return $this->belongsTo('\App\Categories', 'category_id');
    }

    public function SubCategory()
    {
        return $this->belongsTo('\App\SubCategories', 'sub_category_id');
    }

    public function PAP()
    {
        return $this->belongsTo('\App\PAP', 'pap_id');
    }

    public function UnitOfMeasure()
    {
        return $this->belongsTo('App\UnitOfMeasure', 'unit_of_measure_id');
    }
}
