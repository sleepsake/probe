<?php

namespace App\Http\Middleware;

use Closure;

class gatekeep
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles) // Comma to accept arrays
    {
        $user = $request->user();
        $userRole = $user->role;

        if (!in_array($userRole, $roles))
        {
            return response( 'Unauthorized' , 401 );
        }

        return $next($request);
    }
}
