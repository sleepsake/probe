<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware("gatekeep:Super Admin");
    }

    public function list()
    {
        return view('management.items.list');
    }
}
