<?php

namespace App\Http\Controllers;
use App\Categories;
use App\PAP;
use App\PAPItems;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProgramRequestController extends Controller
{
    public function scheduling(Request $request, $category=1)
    {
        $this->middleware("gatekeep:Integrator,Super Admin");

        $current_year = date('Y');
        $get_category = Categories::find($category);

        $data['pap_items'] = PAPItems::where('category_id', '=', $get_category->id)
            ->whereYear('created_at', '=', $current_year)->get();

        $data['current_user'] = Auth::user();

        $data['has_category'] = $category;


        if(!empty($category)){
            $category_id = Categories::where('id', '=', $category)->first();
            $data['has_category'] = $category;
            $data['paps'] = PAPItems::where('category_id', '=', $category_id->id)->get();
        } else {
            $data['paps'] = PAP::whereYear('created_at', '=', $current_year)->get();
        }


        return view('ppmp.search', $data);
    }

    public function profile(PAP $pap)
    {
        $this->middleware("gatekeep:PAP User,Budget Officer,Integrator,Super Admin");
        $this->authorize('manage-pap', $pap);

        // if(!in_array($pap->status, ['Submitted', 'Approved', 'For Revision'])) {
        //     return redirect()->back()->with(['pop-error' => 'You dont have the permission to update this entry.']);
        // }

        $data['pap'] = $pap;

        return view('pap.profile', $data);
    }

    public function review(PAP $pap)
    {
        $this->middleware("gatekeep:PAP User,Budget Officer,Integrator,Super Admin");

        // if(!in_array($pap->status, ['Submitted', 'Approved', 'For Revision'])) {
        //     return redirect()->back()->with(['pop-error' => 'You dont have the permission to update this entry.']);
        // }

        $data['pap'] = $pap;

        return view('pap.review', $data);
    }

    public function forReview(Request $request)
    {
        $data['paps'] = PAP::orderByDesc('id');
        // return $request->get('status');

        $data['paps'] = $data['paps']->where('status', 'Submitted')->where('is_deleted', '=', 0);

        if(Auth::user()->role != 'Super Admin' && $request->user()->role != 'Budget Officer'){
            $data['paps'] = $data['paps']->where('user_id', Auth::user()->id);
        }

        $data['paps'] = $data['paps']->get();
        $data['BudgetCount'] = count($data['paps']);

        return view('apb.for-review', $data);
    }

    public function updateStatus(PAP $pap ,String $status)
    {
        $pap->update([
            'status' => $status,
        ]);

        return redirect()->route('request.review', $pap->id)->with(['pop-success' => 'Update successful!']);
    }

    public function UpdateRemarks(PAP $pap, Request $request)
    {
        $pap->update([
            'remarks' => $request->remarks
        ]);

        return response()->json('success', 200);
    }

    public function addRemarks(Request $request){
        // return $pAPItems;
        PAPItems::where('id', $request->id)->update([
            'remarks' => $request->remarks
        ]);

        return response()->json('success', 200);
    }
}
