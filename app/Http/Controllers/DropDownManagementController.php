<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Cluster;
use App\ClusterSubcategory;
use App\Division;
use App\ItemCategory;
use App\ItemSubCategory;
use App\ModeOfProcurement;
use App\ProgramType;
use App\SubCategories;
use App\UnitOfMeasure;
use Illuminate\Http\Request;

class DropDownManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware("gatekeep:Super Admin");
    }

    public function home()
    {
        return view('management.dropdown.home');
    }

    public function itemCategories()
    {
        return redirect()->route('dropdowns.item.subcategory', ['category_id' => Categories::where('is_deleted', 0)->orderBy('name')->first()->id]);
    }

    public function save_item_categories(Request $request)
    {
        $item_category_name = $request->get('item_category_name');
        if($item_category_name == null)
        {
            return redirect()->route('dropdowns.categories')->with('pop-error', 'Item Category field is required');
        }

        # Check if name exists
        $item_category = new ItemCategory();
        $i = $item_category->get_by_name($item_category_name);

        if($i != null)
        {
            return redirect()->route('dropdowns.categories')->with('pop-error', 'Item Category exists');
        }

        $item_category->name = $item_category_name;
        $item_category->is_procurement = $request->is_procurement;
        $item_category->save();

        return redirect()->route('dropdowns.item.subcategory', ['category_id', $item_category->id])
            ->with('pop-success', 'Successfully added an item category');
    }

    public function update_item_categories(Request $request)
    {
        $item_category_name = $request->get('item_category_name');
        # Check if id exists
        $item_category = new ItemCategory();
        $i = $item_category->get_by_id($request->get('item_category_id'));

        if($i == null)
        {
            return redirect()->route('dropdowns.categories')->with('pop-error', 'Item Category non-existent');
        }

        $i->name = !empty($item_category_name) ? $item_category_name : $i->name;
        $i->save();

        return redirect()->route('dropdowns.item.subcategory', ['category_id', $i->id])
            ->with('pop-success', 'Successfully added an item category');
    }

    public function delete_item_categories(Request $request)
    {
        # Check if id exists
        $item_category = new ItemCategory();
        $i = $item_category->get_by_id($request->get('item_category_id'));

        if($i == null)
        {
            return redirect()->route('dropdowns.categories')->with('pop-error', 'Item Category non-existent');
        }

        $i->is_deleted = 1;
        $i->save();

        return redirect()->route('dropdowns.categories')->with('pop-success', 'Successfully deleted an item category');
    }

    public function item_subcategory($category_id)
    {
        $data['categories'] = ItemCategory::where('is_deleted', 0)->orderBy('name')->get();
        $data['category_id'] = $category_id;
        $data['c'] = ItemCategory::find($category_id);
        return view('management.dropdown.itemcategories.show_subcategory', $data);
    }

    public function save_item_subcategory(Request $request, $category_id)
    {
        $item_subcategory_name = $request->get('item_subcategory_name');
        if($item_subcategory_name == null)
        {
            return redirect()->route('dropdowns.item.subcategory', ['category_id' => $category_id])
                ->with('pop-error', 'Item Subcategory name is required');
        }

        # Check if name exists
        $item_subcategory = new ItemSubCategory();
        $i = $item_subcategory->get_by_name($item_subcategory_name);

        if($i != null)
        {
            return redirect()->route('dropdowns.item.subcategory', ['category_id' => $category_id])
                ->with('pop-error', 'Item Subcategory exists');
        }

        $item_subcategory->category_id = $category_id;
        $item_subcategory->name = $item_subcategory_name;
        $item_subcategory->save();

        return redirect()->route('dropdowns.item.subcategory', ['category_id' => $category_id])
            ->with('pop-success', 'Successfully added subcategory');
    }

    public function update_item_subcategory(Request $request, $category_id)
    {

        $item_subcategory_name = $request->get('item_subcategory_name');
        # Check if id exists
        $item_subcategory = new ItemSubCategory();
        $i = $item_subcategory->get_by_id($request->get('item_subcategory_id'));

        if($i == null)
        {
            return redirect()->route('dropdowns.item.subcategory', ['category_id' => $category_id])
                ->with('pop-error', 'Item Subcategory non-existent');
        }

        $i->name = !empty($item_subcategory_name) ? $item_subcategory_name : $i->name;
        $i->save();

        return redirect()->route('dropdowns.item.subcategory', ['category_id' => $category_id])
            ->with('pop-success', 'Successfully Updated an Item subcategory');
    }

    public function delete_item_subcategory(Request $request, $category_id)
    {
        # Check if id exists
        $item_subcategory = new ItemSubCategory();
        $i = $item_subcategory->get_by_id($request->get('item_subcategory_id'));

        $i->is_deleted = 1;
        $i->save();

        return redirect()->route('dropdowns.item.subcategory', ['category_id' => $category_id])
            ->with('pop-success', 'Successfully Deleted an Item subcategory');
    }

    public function programTypes()
    {
        $data['program_types'] = ProgramType::get_all();
        return view('management.dropdown.programtypes.list', $data);
    }

    public function save_program_type(Request $request)
    {
        $program_name = $request->get('program_type');

        if($program_name == null){
            return redirect()->route('dropdowns.programTypes')->with('pop-error', 'Measurement field is required');
        }

        # Check if the program name is existing
        $program_type = ProgramType::get_by_name($program_name);

        if($program_type != null){
            return redirect()->route('dropdowns.programTypes')->with('pop-error', 'Program exists');
        }

        $p = new ProgramType();
        $p->name = $program_name;
        $p->save();

        return redirect()->route('dropdowns.programTypes')->with('pop-success', 'Successfully added a program type');
    }

    public function update_program_type(Request $request)
    {
        $program_name = $request->get('program_type');
        # Check if the id exists or not (Model File)
        ProgramType::get_by_id($request->get('program_id'));

        $p = ProgramType::find($request->get('program_id'));
        $p->name =  !empty($program_name) ? $program_name : $p->name;
        $p->save();

        return redirect()->route('dropdowns.programTypes')->with('pop-success', 'Successfully updated the program');
    }

    public function delete_program_type(Request $request)
    {
        # Check if the id exists or not (Model File)
        ProgramType::get_by_id($request->get('program_id'));

        $p = ProgramType::find($request->get('program_id'));
        $p->is_deleted = 1;
        $p->save();

        return redirect()->route('dropdowns.programTypes')->with('pop-success', 'Successfully delete the program');
    }

    public function programClusters()
    {
        return redirect()->route('dropdowns.programClusters.subcategory', ['cluster_id' => 1]);
    }

    public function save_program_clusters(Request $request)
    {
        $program_name = $request->get('program_cluster_name');

        if($program_name == null)
        {
            return redirect()->route('dropdowns.programClusters')->with('pop-error', 'Program Cluster Name field is required');
        }

        # Check if program name exists
        $program = new Cluster();
        $p = $program->get_by_name($request->get('program_cluster_name'));

        if($p != null)
        {
            return redirect()->route('dropdowns.programClusters')->with('pop-error', 'Program Cluster exists');
        }

        $program->name = $program_name;
        $program->save();

        return redirect()->route('dropdowns.programClusters.subcategory', ['cluster_id' => $program->id])
            ->with('pop-success', 'Successfully Added a Program Cluster');
    }

    public function update_program_clusters(Request $request)
    {
        $program_name = $request->get('cluster_name');

        # Check if program exists
        $program = new Cluster();
        $p = $program->get_by_id($request->get('cluster_id'));

        $p->name = !empty($program_name) ? $program_name : $p->name;
        $p->save();

        return redirect()->route('dropdowns.programClusters')->with('pop-success', 'Successfully Update a Program Cluster');
    }

    public function delete_program_clusters(Request $request)
    {
        # Check if program exists
        $program = new Cluster();
        $p = $program->get_by_id($request->get('cluster_id'));

        $p->delete();
        return redirect()->route('dropdowns.programClusters')->with('pop-success', 'Successfully Deleted a Program Clusteer');
    }

    public function program_cluster_subcategory($cluster_id)
    {
        $data['clusters'] = Cluster::all();
        $data['cluster_id'] = $cluster_id;
        $data['current_cluster'] = Cluster::find($cluster_id);
        return view('management.dropdown.programclusters.show_subcategory', $data);
    }

    public function save_program_cluster_subcategory(Request $request, $cluster_id)
    {
        $subcategory_name = $request->get('subcategory_name');
        if($subcategory_name == null)
        {
            return redirect()->route('dropdowns.programClusters.subcategory', ['cluster_id' => $cluster_id])
                ->with('pop-error', 'Subcategory Name field is required');
        }

        # Check if subcategory exists
        $subcategory = new ClusterSubcategory();
        $s = $subcategory->get_by_name($subcategory_name);

        if($s != null)
        {
            return redirect()->route('dropdowns.programClusters.subcategory', ['cluster_id' => $cluster_id])
                ->with('pop-error', 'Subcategory exists');
        }

        $subcategory->program_id = $cluster_id;
        $subcategory->name = $subcategory_name;
        $subcategory->save();
        return redirect()->route('dropdowns.programClusters.subcategory', ['cluster_id' => $cluster_id])
            ->with('pop-success', 'Successfully Added a subcategory');
    }

    public function update_program_cluster_subcategory(Request $request, $cluster_id)
    {
        $subcategory_name = $request->get('subcategory_name');
        # Check if subcategory exists
        $subcategory = new ClusterSubcategory();
        $s = $subcategory->get_by_id($request->get('subcategory_id'));

        if($s == null)
        {
            return redirect()->route('dropdowns.programClusters.subcategory', ['cluster_id' => $cluster_id])
                ->with('pop-error', 'Subcategory is non-existent');
        }

        $s->program_id = $cluster_id;
        $s->name = !empty($subcategory_name) ? $subcategory_name : $s->name;
        $s->save();

        return redirect()->route('dropdowns.programClusters.subcategory', ['cluster_id' => $cluster_id])
            ->with('pop-success', 'Successfully update a subcategory');
    }

    public function delete_program_cluster_subcategory(Request $request, $cluster_id)
    {
        # Check if subcategory exists
        $subcategory = new ClusterSubcategory();
        $s = $subcategory->get_by_id($request->get('subcategory_id'));
        $s->is_deleted = 1;
        $s->save();
        return redirect()->route('dropdowns.programClusters.subcategory', ['cluster_id' => $cluster_id])
            ->with('pop-success', 'Successfully deleted a subcategory');
    }

    public function units()
    {
        $data['measurements'] = UnitOfMeasure::all();
        return view('management.dropdown.units.list', $data);
    }

    public function save_unit_of_measurement(Request $request)
    {
        $measurement_type = $request->get('measurement_type');

        if($measurement_type == null) {
            return redirect()->route('dropdowns.units')->with('pop-error', 'Measurement field is required');
        }

        # Check if measurement exists
        $unit = new UnitOfMeasure();
        $u = $unit->get_by_name($measurement_type);

        if($u != null){
            return redirect()->route('dropdowns.units')->with('pop-error', 'Measurement exists');
        }

        $unit->name = $measurement_type;
        $unit->save();
        return redirect()->route('dropdowns.units')->with('pop-success', 'Successfully added a unit of measurement');
    }

    public function update_unit_of_measurement(Request $request)
    {
        $measurement_type = $request->get('measurement_type');
        # Check if measurement exists
        $measurement = new UnitOfMeasure();
        $m = $measurement->get_by_id($request->get('measurement_id'));

        $m->name = !empty($measurement_type) ? $measurement_type : $m->name;
        $m->save();

        return redirect()->route('dropdowns.units')->with('pop-success', 'Successfully updated a measurement');

    }

    public function delete_unit_of_measurement(Request $request)
    {
        # Check if measurement exists
        $measurement = new UnitOfMeasure();
        $m = $measurement->get_by_id($request->get('measurement_id'));

        $m->delete();

        return redirect()->route('dropdowns.units')->with('pop-success', 'Successfully deleted a unit of measure');
    }

    public function modeOfProcurement()
    {
        $data['procurements'] = ModeOfProcurement::all();
        return view('management.dropdown.modeofprocurement.list', $data);
    }

    public function save_mode_of_procurement(Request $request)
    {
        $mode_of_procurement = $request->get('mode_of_procurement');
        if($mode_of_procurement == null)
        {
            return redirect()->route('dropdowns.mode_of_procurement')->with('pop-error', 'Mode Of Procurement field is required');
        }

        # Check if mode exists
        $mode = new ModeOfProcurement();
        $m = $mode->get_by_name($mode_of_procurement);

        if($m != null)
        {
            return redirect()->route('dropdowns.mode_of_procurement')->with('pop-error', 'Mode Of Procurement exists');
        }

        $mode->name = $mode_of_procurement;
        $mode->save();

        return redirect()->route('dropdowns.mode_of_procurement')->with('pop-success', 'Successfully Added a mode of procurement');
    }

    public function update_mode_of_procurement(Request $request)
    {
        $mode_of_procurement = $request->get('mode_of_procurement');
        # Check if mode exists
        $mode = new ModeOfProcurement();
        $m = $mode->get_by_id($request->get('procurement_id'));

        if($m == null){
            return redirect()->route('dropdowns.mode_of_procurement')->with('pop-error', 'Mode Of Procurement exists');
        }

        $m->name = !empty($mode_of_procurement) ? $mode_of_procurement : $m->name;
        $m->save();

        return redirect()->route('dropdowns.mode_of_procurement')->with('pop-success', 'Successfully updated a mode of procurement');
    }

    public function delete_mode_of_procurement(Request $request)
    {
        # Check if mode exists
        $mode = new ModeOfProcurement();
        $m = $mode->get_by_id($request->get('procurement_id'));

        $m->delete();
        return redirect()->route('dropdowns.mode_of_procurement')->with('pop-success', 'Successfully deleted a mode of procurement');
    }

    public function divisions()
    {
        $data['divisions'] = Division::all();
        return view('management.dropdown.divisions.list', $data);
    }

    public function save_divisions(Request $request)
    {
        $division_name = $request->get('division_name');
        $division_description = $request->get('division_description');

        if($division_name == null)
        {
            return redirect()->route('dropdowns.divisions')->with('pop-error', 'Division name field is required');
        }

        # Check if division exists
        $division = new Division();
        $d = $division->get_by_name($division_name);

        if($d != null)
        {
            return redirect()->route('dropdowns.divisions')->with('pop-error', 'Divsion exists');
        }

        $division->name = $division_name;
        $division->description = $division_description;
        $division->save();

        return redirect()->route('dropdowns.divisions')->with('pop-success', 'Successfully Added Division');
    }

    public function update_divisions(Request $request)
    {
        $division_name = $request->get('division_name');
        $division_description = $request->get('division_description');

        # Check if division exists
        $division = new Division();
        $d = $division->get_by_id($request->get('division_id'));

        if($d == null)
        {
            return redirect()->route('dropdowns.divisions')->with('pop-error', 'Division is non-existent');
        }

        $d->name = !empty($division_name) ? $division_name : $d->name;
        $d->description = !empty($division_description) ? $division_description : $d->description;
        $d->save();

        return redirect()->route('dropdowns.divisions')->with('pop-success', 'Successfully update a division');
    }

    public function delete_divisions(Request $request)
    {
        # Check if division exists
        $division = new Division();
        $d = $division->get_by_id($request->get('division_id'));

        $d->delete();
        return redirect()->route('dropdowns.divisions')->with('pop-success', 'Successfully deleted a division');
    }
}
