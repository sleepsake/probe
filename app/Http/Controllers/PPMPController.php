<?php

namespace App\Http\Controllers;

use App\Categories;
use App\ModeOfProcurement;
use App\PAP;
use App\PAPItems;
use App\PPMP;
use App\PPMPItems;
use App\SubCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PPMPController extends Controller
{
    public function __construct()
    {
        $this->middleware("gatekeep:Integrator,Super Admin");
    }

    public function index(Request $request)
    {
//        $data['ppmps'] = PAP::wherein('status', ['Approved'])->orderByDesc('id');
//
//        if($request->get('type') && $request->get('type') != 'All') {
//            $data['ppmps'] = $data['ppmps']->where('type', $request->get('type'));
//        }
        $where = "";
//        if(in_array($request->type, ['Program', 'Activity', 'Project'])) {
//            $where = " AND p.type = '{$request->type}'";
//        }

        if(!empty($request->category)) {
            $where = " AND pi.category_id = '{$request->category}'";
        } else {
            $categoryId = Categories::orderBy('name')->first()->id;
            $where = " AND pi.category_id = '{$categoryId}'";
        }

        $data['ppmps'] = DB::select("
                            SELECT
                            p.code,
                            p.mooe_co,
                            p.user_id,
                            pi.*
                            FROM pap_items pi
                            LEFT JOIN pap p ON p.id = pi.pap_id
                            WHERE pi.for_procurement = 'P' AND p.status IN ('Submitted', 'Approved')
                            {$where}
                            AND pi.id NOT IN (SELECT pap_item_id as id FROM ppmp_items)
                        ");
//        dd($data);

//        $data['ppmps'] = $data['ppmps']->whereIn('status', ['Draft', 'For Revision', 'Submitted']);

//        $data['ppmps'] = $data['ppmps']->get();

        return view('ppmp.index', $data);
    }

    public function create($category, $array_from_list=null)
    {

        if(!empty($array_from_list)){
            $data['pap_items'] = $pap_items = explode('-', $array_from_list);
            $subcategories = [];
            foreach ($pap_items as $pap_item){
                $item = PAPItems::find($pap_item);
                if(!in_array($item->SubCategory->name, $subcategories)){
                    array_push($subcategories, $item->SubCategory->name);
                }
            }
            $data['subcategories'] = collect($subcategories);
        }

        $data['category'] = $cat = Categories::where('id', '=', $category)->first();
        $data['ppmps'] = PPMP::whereYear('created_at', '=', date('Y'))->where('category_id', '=', $cat->id)->get();
        $data['array_from_list'] = $array_from_list;
        $data['mode_of_procurement'] = ModeOfProcurement::orderBy('name')->get();
        return view('ppmp.create', $data);
    }

    public function ppmp_create_save(Request $request, $category)
    {
        $cat = Categories::where('id', '=', $category)->first();
        if(!empty($request->get('ppmp_id')))
        {
            PPMPItems::where('ppmp_id', '=', $request->get('ppmp_id'))->delete();
            $ppmp = PPMP::find($request->get('ppmp_id'));
        } else {
            $ppmp = new PPMP();
        }

        $ppmp->ppmp_name = $request->get('ppmp_name');
        $ppmp->sub_category_id = $cat->SubCategory->name ?? 0;
        $ppmp->category_id = $cat->id;
        $ppmp->user_id = Auth::user()->id;
        $ppmp->general_description = $request->get('ppmp_description');
        $ppmp->esd = $request->get('ppmp_esd');
        $ppmp->mode_of_procurement_id = $request->get('mode_of_procurement_id');
        $ppmp->date = $request->get('ppmp_date');
        $ppmp->advertisement = $request->get('advertisement');
        $ppmp->submission_of_bids = $request->get('submission_of_bids');
        $ppmp->notice_of_award = $request->get('notice_of_award');
        $ppmp->contract_signing = $request->get('contract_signing');
        $ppmp->save();

        if(count($request->get('ppmp_items')) >= 1){
            foreach($request->get('ppmp_items') as $item){
                $ppmp_item = new PPMPItems();
                $ppmp_item->pap_item_id = $item;
                $ppmp_item->ppmp_id = $ppmp->id;
                $ppmp_item->save();
            }
        }


        return redirect()->route('request.ppmp.mine');
    }

    public function consolidate($category, $array_from_list, $ppmp_id)
    {
        $data['consolidate_ppmp_items'] = PPMPItems::where('ppmp_id', '=', $ppmp_id)->get();
        if(!empty($array_from_list)){
            $data['pap_items'] = $pap_items = explode('-', $array_from_list);
            $subcategories = [];
            // Incoming Subcategories
            foreach ($pap_items as $pap_item){
                $item = PAPItems::find($pap_item);
                if(!in_array($item->SubCategory->name, $subcategories)){
                    array_push($subcategories, $item->SubCategory->name);
                }
            }

            // Consolidating Subcategories
            foreach($data['consolidate_ppmp_items'] as $ppmp_item){
                if(!in_array($ppmp_item->PAPItem->Subcategory->name, $subcategories)){
                    array_push($subcategories, $ppmp_item->PAPItem->SubCategory->name);
                }
            }

            $data['subcategories'] = collect($subcategories);
        }

        $data['category'] = $cat = Categories::where('id', '=', $category)->first();
        $data['ppmps'] = PPMP::whereYear('created_at', '=', date('Y'))->where('category_id', '=', $cat->id)->get();
        $data['array_from_list'] = $array_from_list;
        $data['mode_of_procurement'] = ModeOfProcurement::orderBy('name')->get();
        $data['current_ppmp'] = PPMP::find($ppmp_id);


        return view('ppmp.consolidate', $data);
    }

    public function vault($year = "2020")
    {
        $data['year'] = $year;
        $data['ppmps'] = PPMP::whereYear('created_at', '=', $year)->get();

        $cost = [];
        foreach($data['ppmps'] as $item){
            $ppmp_items = $item->Items;
            foreach ($ppmp_items as $ppmp_i){
                $pap_item = \App\PAPItems::where('id', '=', $ppmp_i->pap_item_id)->first();
//                dd($pap_item);
                array_push($cost, $pap_item->total_cost);
            }
        }

        $estimated_cost = array_sum($cost);
        return view('ppmp.vault',$data);
    }

    public function postCreate(Request $request)
    {
//        dd($request->all());
        if(!$request->ppmp_id) {
            $ppmp = new PPMP();
            $ppmp->category_id = Categories::find(SubCategories::find($request->sub_category_id)->category_id)->id;
            $ppmp->sub_category_id = $request->sub_category_id;
            $ppmp->general_description = $request->general_description;
            $ppmp->esd = $request->esd;
            $ppmp->mode_of_procurement = $request->mode_of_procurement;
            $ppmp->date = $request->date;
            $ppmp->advertisement = $request->advertisement;
            $ppmp->submission_of_bids = $request->submission_of_bids;
            $ppmp->notice_of_award = $request->notice_of_award;
            $ppmp->contract_signing = $request->contract_signing;
            $ppmp->user_id = Auth::user()->id;
            $ppmp->save();

            foreach($request->items as $papItemID) {
                $ppmpItem = new PPMPItems();
                $ppmpItem->ppmp_id = $ppmp->id;
                $ppmpItem->pap_item_id = $papItemID;
                $ppmpItem->save();
            }

            return redirect('/dashboard/ppmp')->with(['pop-success' => 'Create PPMP successful!']);
        } else {
            foreach($request->items as $papItemID) {
                $ppmpItem = new PPMPItems();
                $ppmpItem->ppmp_id = $request->ppmp_id;
                $ppmpItem->pap_item_id = $papItemID;
                $ppmpItem->save();
            }

            return redirect('/dashboard/ppmp')->with(['pop-success' => 'PPMP Update successful!']);
        }

    }

    public function profile($id)
    {
        $data['ppmp'] = PPMP::find($id);
        $data['ppmp_items'] = PPMPItems::where('ppmp_id', '=', $data['ppmp']->id)->get();
        $cost = [];
        foreach($data['ppmp_items'] as $item){
            $pap_item = PAPItems::where('id', '=', $item->pap_item_id)->first();
            array_push($cost, $pap_item->total_cost);
        }
        $data['total_cost'] = array_sum($cost);

        return view('ppmp.profile', $data);
    }

    public function profile_update($id)
    {
        $data['ppmp'] = PPMP::find($id);
        $data['ppmp_items'] = PPMPItems::where('ppmp_id', '=', $data['ppmp']->id)->get();
        $cost = [];
        foreach($data['ppmp_items'] as $item){
            $pap_item = PAPItems::where('id', '=', $item->pap_item_id)->first();
            array_push($cost, $pap_item->total_cost);
        }

        $total_cost = collect($cost)->sum();
        $data['total_cost'] = $total_cost;


        $data['mode_of_procurement'] = ModeOfProcurement::orderBy('name')->get();
        return view('ppmp.update', $data);
    }

    public function profile_update_put(Request $request, $id)
    {

        $ppmp = PPMP::find($id);
        $category = SubCategories::where('name', '=', $request->get('ppmp_sub_category_id'))->first();
        $ppmp->ppmp_name = $request->get('ppmp_name');
        $ppmp->category_id = $category->category_id;
        $ppmp->sub_category_id = $category->id;
        $ppmp->user_id = Auth::id();
        $ppmp->general_description = $request->get('ppmp_description');
        $ppmp->esd = $request->get('ppmp_esd');
        $ppmp->mode_of_procurement_id = $request->get('mode_of_procurement_id');
        $ppmp->date = $request->get('ppmp_date');
        $ppmp->advertisement = $request->get('advertisement');
        $ppmp->submission_of_bids = $request->get('submission_of_bids');
        $ppmp->notice_of_award = $request->get('notice_of_award');
        $ppmp->contract_signing = $request->get('contract_signing');
        $ppmp->save();

        PPMPItems::where('ppmp_id', '=', $ppmp->id)->delete();

        if(count($request->get('ppmp_items')) >= 1){
            foreach($request->get('ppmp_items') as $item){
                $ppmp_item = new PPMPItems();
                $ppmp_item->pap_item_id = $item;
                $ppmp_item->ppmp_id = $ppmp->id;
                $ppmp_item->save();
            }
        }

        return redirect()->route('request.ppmp.mine');

    }
    public function profile_deleted($id)
    {
        $ppmp = PPMP::find($id);
        $ppmp->is_deleted = 1;
        $ppmp->save();

        return redirect()->route('request.ppmp.mine');
    }
    public function view(PAP $pap)
    {
        if(!in_array($pap->status, ['Submitted', 'Approved'])) {
            return redirect()->back()->with(['pop-error' => 'You dont have the permission to update this entry.']);
        }

        $data['pap'] = $pap;

        return view('ppmp.view', $data);
    }

    public function postUpdate(Request $request, PAP $pap)
    {
        if(!$pap->PPMP) {
            $ppmp = new PPMP();
            $ppmp->user_id = Auth::user()->id;
            $ppmp->pap_id = $pap->id;
            $ppmp->advertisement = $request->advertisement;
            $ppmp->submission_of_bids = $request->submission_of_bids;
            $ppmp->notice_of_award = $request->notice_of_award;
            $ppmp->contract_signing = $request->contract_signing;
            $ppmp->save();
        } else {
            $ppmp = $pap->PPMP;
            $ppmp->advertisement = $request->advertisement;
            $ppmp->submission_of_bids = $request->submission_of_bids;
            $ppmp->notice_of_award = $request->notice_of_award;
            $ppmp->contract_signing = $request->contract_signing;
            $ppmp->save();
        }

        return redirect()->back()->with(['pop-success' => 'PPMP Update successful!']);
    }

    public function mine()
    {
        $current_user_role = Auth::user()->role;
        $accessors = ['Super Admin'];
        if(in_array($current_user_role, $accessors)){
            $data['ppmps'] = PPMP::where('is_deleted', '=', 0)->get();
        } else {
            $data['ppmps'] = PPMP::where('user_id', '=', Auth::user()->id)->where('is_deleted', '=', 0)->get();
        }

        return view('ppmp.mine', $data);
    }

    public function items(Request $request)
    {
//        $data['ppmps'] = \DB::select(DB::raw("
//                SELECT
//                    p.*,
//                    p.general_description,
//                    GROUP_CONCAT(c.name) as `category`,
//                    GROUP_CONCAT(pap.program) as `program`,
//                    SUM(pap_i.total_cost) as estimated_budget
//                FROM
//                ppmp p
//                LEFT JOIN ppmp_items pi ON pi.ppmp_id = p.id
//                LEFT JOIN pap_items pap_i ON pap_i.id = pi.pap_item_id
//                LEFT JOIN pap on pap.id = pap_i.pap_id
//                LEFT JOIN categories c ON c.id = pap_i.category_id
//
//                GROUP BY p.id, p.user_id,probe.p.general_description"))->get();

//        $data['paps'] = DB::select("
//                            SELECT
//                            COUNT(pi.id) as count
//                            FROM pap_items pi
//                            LEFT JOIN pap p ON p.id = pi.pap_id
//                            WHERE pi.for_procurement = 'P' AND p.status IN ('Submitted', 'Approved')
//                            AND pi.id NOT IN (SELECT pap_item_id as id FROM ppmp_items)
//                        ");

        if(!empty($request->category)) {
            $categoryId = $request->category;
        } else {
            $categoryId = Categories::orderBy('name')->first()->id;
        }

        $data['ppmps'] = PPMP::where('category_id', $categoryId)->orderByDesc('date')->get();

//        dd($data);

        return view('ppmp.items', $data);
    }

    public function getAction(Request $request)
    {
        $subCategoryIds = [];
        foreach($request->subCategories as $subCategoryId) {
                $subCategoryIds[] = $subCategoryId;
        }

        $result = [];
        $ppmpItems = PPMPItems::whereIn('pap_items.sub_category_id', [$subCategoryIds])->leftJoin('pap_items', 'pap_items.id', '=', 'ppmp_items.pap_item_id')->leftJoin('pap', 'pap.id', '=', 'pap_items.pap_id')->get();
        foreach($ppmpItems->groupBy('program, cluster') as $collections) {
            $x = 0;
            foreach($collections as $ppmpItem) {
                $result[$x] = PPMP::find($ppmpItem->ppmp_id);
                $y = 0;
                foreach($result[$x]->Items as $item) {
                    $ppmp_items[$y] = PAPItems::find($item->pap_item_id);
                    $ppmp_items[$y]['sub_category'] = SubCategories::find($ppmp_items[$y]->sub_category_id)->name;
                    $ppmp_items[$y]['end_user'] = \App\User::find(PAP::find($ppmp_items[$y]->pap_id)->user_id)->department;
                    $y++;
                }

                $result[$x]['ppmp_items'] = $ppmp_items;
                $x++;
            }
        }

        return response()->json($result);

    }
}
