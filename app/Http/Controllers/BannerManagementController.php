<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BannerManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware("gatekeep:Super Admin");
    }

    public function showBannerForm()
    {
        $data = DB::table('banner_magement')->where('id', 1)->first();
        return view('banner.form', ['data' => $data]);
    }

    public function update(Request $request)
    {
        $system_short_name = $request->input('system_short_name');
        $system_long_name = $request->input('system_long_name');

        DB::table('banner_magement')->where('id', 1)->update(['system_short_name' => $system_short_name, 'system_long_name' => $system_long_name]);

        return redirect()->route('banner.form')->with(['pop-success' => 'Update successful!']);;
    }
}
