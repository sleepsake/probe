<?php

namespace App\Http\Controllers;

use App\PPMP;
use App\APP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class APPController extends Controller
{
    public function __construct()
    {
        $this->middleware("gatekeep:BAC,Super Admin");
    }

    public function index($year = "")
    {
        // $data['ppmps'] = DB::select("SELECT * FROM ppmp WHERE ppmp.id NOT IN (SELECT ppmp_id as id FROM app)");
        $data['year'] = $year;
        if (empty($year))
        {
            $data['year'] = $year = date("Y");
        }

        if (Auth::user()->role == 'Super Admin')
        {
            $data['apps'] = APP::whereYear("created_at", '=', $year)->get();
        } else {
            $data['apps'] = \App\APP::whereYear("created_at",$year)->where("user_id", Auth::user()->id)->get();
        }

        return view('app.index', $data);
    }

    public function add()
    {
        $data['ppmps'] = \App\PPMP::all();
        return view('app.add', $data);
    }

    public function postAdd(Request $request)
    {
        $app = new \App\APP;

        $app->title = $request->title;
        $app->description = $request->description;
        $app->user_id = Auth::user()->id;

        $app->save();

        foreach($request->ppmps as $p)
        {
            $app_ppmp = new \App\AppPPMP;

            $app_ppmp->app_id = $app->id;
            $app_ppmp->ppmp_id = $p;
            $app_ppmp->user_id = Auth::user()->id;

            $app_ppmp->save();
        }

        return redirect()->route('app.list')->with(['pop-success' => 'APP Successfully Created!']);
    }

    public function update(APP $app)
    {
        $data['ppmps'] = \App\PPMP::all();
        $data['app'] = $app;
        return view('app.update', $data);
    }

    public function postUpdate(Request $request, APP $app)
    {
        $app->title = $request->title;
        $app->description = $request->description;

        $app->save();

        $ppmps = $request->ppmps;
        if (!empty($ppmps))
        {
            \App\AppPPMP::where("app_id",$app->id)->delete();

            foreach ($ppmps as $p)
            {
                $app_ppmp = new \App\AppPPMP;

                $app_ppmp->app_id = $app->id;
                $app_ppmp->ppmp_id = $p;
                $app_ppmp->user_id = Auth::user()->id;

                $app_ppmp->save();
            }
        }

        return redirect()->route('app.update', $app->id)->with(['pop-success' => 'APP Successfully Updated!']);
    }

    public function addToApp(Request $request)
    {
        if(!empty($request->items)) {
            foreach($request->items as $item) {
                $app = new \App\APP();
                $app->user_id = Auth::user()->id;
                $app->ppmp_id = $item;
                $app->save();
            }

            return 'success';
        }
    }

    public function items()
    {
        $data['apps'] = \App\APP::with('PPMP')->get();

        return view('app.items', $data);
    }
}
