<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use \App\Categories;

class UserManagementController extends Controller
{
    public function __construct()
    {
//        (Ralph) I commented this block due to it restricts the normal user to access his/her user management
//        $this->middleware("gatekeep:User Admin,Super Admin");
    }

    public function index()
    {
        $data['users'] = User::all();

        return view('user-management.index', $data);
    }

    public function add()
    {
        $data['ppmpCategory'] = Categories::orderBy('name')->get();
        return view('user-management.add',$data);
    }

    public function postAdd(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput()
                ->with(['pop-error' => $validator->errors()->first()]);
        }

        $user = User::create($request->all());
        $user->password = Hash::make($request->password);
        $user->save();

        $categories = $request->categories;
        if (!empty($categories))
        {
            \App\UserCategory::where("user_id",$user->id)->delete();

            foreach ($categories as $c)
            {
                $uc = new \App\UserCategory;
                $uc->user_id = $user->id;
                $uc->category_id = $c;
                $uc->save();
            }
        }

        return redirect("/dashboard/user-management")
            ->with(['pop-success' => 'Add User successful!']);
    }

    public function update(User $user)
    {
        $current_user = Auth::user()->id;
        if($current_user != $user->id){
            return redirect()->route('update_user', ['user' => $current_user]);
        }

        $data['ppmpCategory'] = Categories::orderBy('name')->get();
        $data['user'] = $user;
        $data['uc'] = $uc = $user->UserCategories;

        return view('user-management.update', $data);
    }

    public function postUpdate(Request $request, User $user)
    {
        $rules = [
            'name' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput()
                ->with(['pop-error' => $validator->errors()->first()]);
        }

        if(!empty($request->password)) {
            $validator = Validator::make($request->all(), ['password' => 'required|confirmed|min:8']);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput()
                    ->with(['pop-error' => $validator->errors()->first()]);
            }

            $user->password = Hash::make($request->password);
        }


        $user->name = $request->name;
        $user->division_id = $request->division_id;
//        $user->role = $request->role;
        $user->save();


//        $categories = $request->categories;
//        if (!empty($categories))
//        {
//            \App\UserCategory::where("user_id",$user->id)->delete();
//
//            foreach ($categories as $c)
//            {
//                $uc = new \App\UserCategory;
//                $uc->user_id = $user->id;
//                $uc->category_id = $c;
//                $uc->save();
//            }
//        }

        return redirect("/dashboard/user-management/{$user->id}")
            ->with(['pop-success' => 'User update successful!']);
    }
}










//
