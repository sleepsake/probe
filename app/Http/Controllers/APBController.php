<?php

namespace App\Http\Controllers;

use App\PAP;
use App\PAPItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class APBController extends Controller
{
    public function __construct()
    {
        $this->middleware("gatekeep:Budget Officer,Super Admin");
    }

    public function index(Request $request)
    {
        if($request->status === 'Approved') {
            $data['apbs'] = PAP::where('status', 'Approved')->orderByDesc('id');
        } else {
            $data['apbs'] = PAP::wherein('status', ['Submitted', 'For Revision'])->orderByDesc('id');
        }


//        if($request->get('type') && $request->get('type') != 'All') {
//            $data['apbs'] = $data['apbs']->where('type', $request->get('type'));
//        }

//        $data['apbs'] = $data['apbs']->whereIn('status', ['Draft', 'For Revision', 'Submitted']);

        $data['apbs'] = $data['apbs']->get();

        return view('apb.index', $data);
    }

    public function create()
    {
        return view('apb.create');
    }

    public function view(PAP $apb)
    {
        if(!in_array($apb->status, ['Submitted', 'Approved'])) {
            return redirect()->back()->with(['pop-error' => 'You dont have the permission to update this entry.']);
        }

        $data['pap'] = $apb;

        return view('apb.view', $data);
    }

    public function postUpdate(Request $request, PAP $apb)
    {
        $apb->remarks = $request->remarks;
        $apb->approver_id = Auth::user()->id;

        if($request->action == 'approve') {
            $apb->status = 'Approved';
        }

        if($request->action == 'for_revision') {
            $apb->status = 'For Revision';
        }

        $apb->save();


        if(!empty($request->pap_item)) {
            foreach($request->pap_item as $id => $data) {
                PAPItems::where('id', $id)->update(['remarks' => $data['remarks']]);
            }
        }

        return redirect('/dashboard/apb')->with(['pop-success' => 'APB Update successful!']);
    }
}
