<?php

namespace App\Http\Controllers;

use App\Categories;
use App\PAP;
use App\PAPItems;
use App\ProgramType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PAPController extends Controller
{
    public function __construct()
    {
        $this->middleware("gatekeep:PAP User,Budget Officer,Integrator,BAC,Super Admin");
    }

    public function index(Request $request, $status = "All")
    {
        if(Auth::user()->role == 'Super Admin'){
            $data['paps'] = PAP::orderByDesc('id');
        } else {
            $data['paps'] = PAP::where("user_id",$request->user()->id)->orderByDesc('id');
        }

        if($request->get('type') && $request->get('type') != 'All') {
            $data['paps'] = $data['paps']->where('type', $request->get('type'));
        }

        if($status != 'All') {
            $data['paps'] = $data['paps']->where('status', $status);
        }

//        $data['paps'] = $data['paps']->whereIn('status', ['Draft', 'For Revision', 'Submitted']);
        if(Auth::user()->role == 'Super Admin'){
            $data['paps'] = $data['paps']->where('is_deleted', '=', 0)->get();
        } else {
            $data['paps'] = $data['paps']->where('user_id', Auth::user()->id)->where('is_deleted', '=', 0)->get();
        }


        $data['status'] = $status;

        return view('pap.index', $data);
    }

    public function vault(Request $request, $year = "2020")
    {
        $this->middleware("gatekeep:Super Admin");

        $data['paps'] = PAP::orderByDesc('id')->where('status','Approved');

        if($request->get('type') && $request->get('type') != 'All') {
            $data['paps'] = $data['paps']->where('type', $request->get('type'));
        }

        if(Auth::user()->role == 'Super Admin'){
            $data['paps'] = $data['paps']->whereYear('created_at', $year)
                ->where('is_deleted', '=', 0)->get();
        } else {
            $data['paps'] = $data['paps']->whereYear('created_at', $year)
                ->where('user_id', Auth::user()->id)
                ->where('is_deleted', '=', 0)->get();
        }

        $data['year'] = $year;

        return view('pap.vault', $data);
    }

    public function create()
    {
        return view('pap.create');
    }

    public function postCreate(Request $request)
    {

        if(empty($request->item))
        {
            return redirect()->route('pap_create')->withInput()->with(['pop-error' => 'Cannot submit without items']);
        }

        $pap = new PAP();
        $pap->code = $request->code;
        $pap->project_description = $request->project_description;
        $pap->date = $request->date;
        $pap->program_type_id = $request->program_type_id;
        $pap->program_id = $request->program_id;
        $pap->cluster_id = $request->cluster_id;
        $pap->source_of_funds = 'GOP';
        $arr = ['MOOE', 'Capital Outlay'];
        $pap->mooe_co = $arr[array_rand($arr, 1)];
        $pap->user_id = Auth::user()->id;

        $pap->advertisement = $request->advertisement;
        $pap->submission_of_bids = $request->submission_of_bids;
        $pap->notice_of_award = $request->notice_of_award;
        $pap->contract_signing = $request->contract_signing;

        if($request->action == 'draft') {
            $pap->status = 'Draft';
        }

        if($request->action == 'submit') {
            $pap->status = 'Submitted';
        }

        $pap->save();

        foreach($request->item as $i) {
            $item = new PAPItems();
            $item->pap_id = $pap->id;
            $item->item_name = $i['item_name'];
            $item->quantity = $i['quantity'];
            $item->unit_cost = $i['unit_cost'];
            $item->total_cost = $i['total_cost'];
            $item->category_id = $i['category_id'];
            $item->sub_category_id = $i['sub_category_id'];
            $item->unit_of_measure_id = $i['unit_of_measure_id'];
            $item->for_procurement = $i['for_procurement'];
            $item->specifications = $i['specifications'];
            $item->save();
        }

        return redirect('/dashboard/pap')->with(['pop-success' => 'PAP Successfully Created. Code: ' . $pap->code]);


    }

    public function update(PAP $pap)
    {
        if(!in_array($pap->status, ['Draft', 'For Revision'])) {
            return redirect()->back()->with(['pop-error' => 'This PAP cannot be updated.']);
        }

        return view('pap.update', ['pap' => $pap, 'programs' => ProgramType::where('is_deleted',0)->get()]);
    }

    public function postUpdate(Request $request, PAP $pap)
    {
        $pap->code = $request->code;
        $pap->project_description = $request->project_description;
        $pap->program_id = $request->program;
        $pap->program_type_id = $request->type;
        $pap->cluster_id = $request->cluster;
        $pap->advertisement = $request->advertisement;
        $pap->submission_of_bids = $request->submission_of_bids;
        $pap->notice_of_award = $request->notice_of_award;
        $pap->contract_signing = $request->contract_signing;
        $pap->date = $request->date;
        
        $action = $request->get('action');
        if($action == 'submit'){
            $pap->status = 'Submitted';
        }

        if($action == 'draft'){
            $pap->status = 'Draft';
        }

        $pap->save();

        PAPItems::where('pap_id', $pap->id)->delete();

        $items = $request->get('item');

        if(count($items) < 1){
            return redirect()->route('pap_create')->withInput()->with(['pop-error' => 'Cannot submit without items']);
        }

        foreach ($items as $index => $item){
            $i = new PAPItems();
            $i->pap_id = $pap->id;
            $i->item_name = $item['item_name'];
            $i->quantity = $item['quantity'];
            $i->unit_of_measure_id = $item['unit_of_measure_id'];
            $i->unit_cost = $item['unit_cost'];
            $i->total_cost = $item['total_cost'];
            $i->category_id = $item['category_id'];
            $i->sub_category_id = $item['sub_category_id'];
            $i->for_procurement = $item['for_procurement'];
            $i->specifications = $item['specifications'];
            if(isset($item['remarks'])) {
                $i->remarks = $item['remarks'];
            }
            $i->save();
        }

        return redirect('/dashboard/pap')->with(['pop-success' => 'PAP Successfully Created. Code: ' . $pap->code]);
    }

    public function delete_pap(Request $request, $pap_id)
    {
        $pap = PAP::find($pap_id);
        $pap->is_deleted=1;
        $pap->save();
        return redirect()->back()->with(['pop-success' => 'Sucessfully Delete a PAP']);

    }
}
