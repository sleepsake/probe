<?php

namespace App\Http\Controllers;

use App\CSE;
use App\Imports\CSEImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\Console\Input\Input;


class CSEController extends Controller
{
    public function index()
    {
        $data['cses'] = CSE::all();
        return view('cse.ps-dbm', $data);
    }

    public function create_form()
    {
        return view('cse.create');
    }

    public function create_save(Request $request)
    {
        $cse = new CSE();
        $cse->product_code = $request->get('cse_product_code');
        $cse->item_description = $request->get('cse_item_description');
        $cse->unit_of_measure = $request->get('cse_unit_of_measure');
        $cse->category = $request->get('cse_category');
        $cse->price = $request->get('cse_price');
        $cse->save();

        return redirect()->route('cse_list')->with(
            'success_create_cse', "Successfully Added a CSE"
        );
    }

    public function update_form($id)
    {
        $data['cse'] = CSE::find($id);
        return view('cse.update', $data);
    }

    public function update_save(Request $request, $id)
    {
        $cse = CSE::find($id);
        $cse->product_code = $request->get('cse_product_code');
        $cse->item_description = $request->get('cse_item_description');
        $cse->unit_of_measure = $request->get('cse_unit_of_measure');
        $cse->category = $request->get('cse_category');
        $cse->price = $request->get('cse_price');
        $cse->save();

        return redirect()->route('cse_list');
    }

    public function delete_cse(Request $request, $id)
    {
        $cse = CSE::find($id);
        $cse->delete();
        return redirect()->route('cse_list');
    }

    public function app()
    {
        return view('cse.app');
    }

    public function import_excel(Request $request)
    {
        $import = Excel::import(new CSEImport(), $request->file('cse_import_excel'));

        return redirect()->route('cse_list')->with('success', 'Successfully Imported Excel File.');
    }
}
