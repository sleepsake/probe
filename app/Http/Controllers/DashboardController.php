<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //

    public function index()
    {
        if(Auth::user()->role == 'User Admin') {
            return redirect('/dashboard/user-management');
        }

        return view('dashboard.index');
    }
}
