<?php

namespace App\Http\View\Composers;

use App\PAP;
use Illuminate\View\View;

class BudgetReviewComposer
{
    public function compose(View $view)
    {
        if(!isset($BudgetCount)){
            $paps = PAP::orderByDesc('id')->where('status', 'submitted');
            if(auth()->user()->role != 'Super Admin' && auth()->user()->role != 'Budget Officer'){
                $paps->where('user_id', auth()->user()->id);
            }
            $view->with('budgetReviewCount', $paps->count());
        } else {
            $view->with('budgetReviewCount', 0);
        }
    }
}
