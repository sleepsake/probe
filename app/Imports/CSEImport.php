<?php

namespace App\Imports;

use App\CSE;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;

class CSEImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        unset($rows[0]);
        foreach($rows as $row)
        {
            CSE::create([
                'id' => abs($row[0]),
                'category' => $row[1],
                'product_code' => $row[2],
                'item_description' => $row[3],
                'unit_of_measure' => $row[4],
                'price' => $row[5],
            ]);
        }


    }
}
