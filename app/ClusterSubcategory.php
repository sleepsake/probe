<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClusterSubcategory extends Model
{
    //
    protected $table = 'programs_clusters';

    public function get_by_name($name)
    {
        $subcategory = $this->where('name', '=', $name)->where('is_deleted', '=', 0)->first();
        return $subcategory;
    }

    public function get_by_id($id)
    {
        $subcategory = $this->where('id', '=', $id)->where('is_deleted', '=', 0)->first();
        return $subcategory;
    }
}
