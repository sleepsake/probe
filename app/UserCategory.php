<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCategory extends Model
{
    protected $table = "user_categories";

    public function Category()
    {
        return $this->belongsTo("\App\Categories", "category_id");
    }

    public function User()
    {
        return $this->belongsTo("\App\User", "user_id");
    }
}
