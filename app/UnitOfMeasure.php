<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitOfMeasure extends Model
{
    protected $table = 'unit_of_measures';

    public function get_by_name($name)
    {

        $measurement = $this->where('name', '=', $name)->first();
        return $measurement;
    }

    public function get_by_id($id)
    {
        $measurement = $this->where('id', '=', $id)->first();
        if($measurement == null)
        {
            return redirect()->route('dropdowns.units')->with('pop-error', 'Unit of measurement non-existent');
        }
        return $measurement;
    }
}
