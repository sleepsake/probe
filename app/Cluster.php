<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cluster extends Model
{
    //
    protected $table = 'programs';

    public function get_by_name($name)
    {
        $program = $this->where('name', '=', $name)->first();
        return $program;
    }

    public function get_by_id($id)
    {
        $program = $this->where('id', '=', $id)->first();
        if($program == null)
        {
            return redirect()->route('dropdowns.programClusters')->with('pop-error', 'Program is non-existent');
        }
        return $program;
    }

    public function SubCategories()
    {
        return $this->hasMany('App\ClusterSubcategory', 'program_id')->where('is_deleted', '=', 0);
    }
}
