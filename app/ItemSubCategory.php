<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemSubCategory extends Model
{
    //
    protected $table = 'sub_categories';

    public function get_by_name($name)
    {
        $item_subcategory = $this->where('name', '=', $name)->where('is_deleted', '=', 0)->first();
        return $item_subcategory;
    }

    public function get_by_id($id)
    {
        $item_subcategory = $this->where('id', '=', $id)->where('is_deleted', '=', 0)->first();
        return $item_subcategory;
    }
}
