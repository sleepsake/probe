<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PPMP extends Model
{
    protected $table = 'ppmp';

    public function PAP()
    {
        return $this->hasOne('App\PAP', 'pap_id');
    }

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function APP()
    {
        return $this->hasMany('App\AppPPMP', 'ppmp_id');
    }

    public function Items()
    {
        return $this->hasMany('\App\PPMPItems', 'ppmp_id');
    }

    public function Milestones()
    {
        $dates[0]['date'] = $this->advertisement;
        $dates[1]['date'] = $this->submission_of_bids;
        $dates[2]['date'] = $this->notice_of_award;
        $dates[3]['date'] = $this->contract_signing;
        $dates = collect($dates)->sortBy('date');
//        dd($dates);

        $ym = "";
        $result = "";
        foreach($dates as $d) {
            if(empty($result)) {
                $result = date('M - j', strtotime($d['date']));
                $lastDate = date('Ym', strtotime($d['date']));
            } else {
                if($lastDate == date('Ym', strtotime($d['date']))) {
                    $result .= ", ". date('j', strtotime($d['date']));
                } else {
                    $result .= " ". date('M - j', strtotime($d['date']));
                }
            }
        }


        return $result;
    }

    public function Schedule()
    {
        $dates[0]['date'] = $this->advertisement;
        $dates[1]['date'] = $this->submission_of_bids;
        $dates[2]['date'] = $this->notice_of_award;
        $dates[3]['date'] = $this->contract_signing;
        $dates = collect($dates)->sortBy('date');

        $from = date('M', strtotime($dates[0]['date']));
        $to = date('M', strtotime($dates[3]['date']));
        return "Posting: {$from} - {$to}";
    }

    // public function APP()
    // {
    //     return $this->belongsToMany('App\App', 'app', 'ppmp_id', 'ppmp_id');
    // }

    public function getQuarter($date)
    {
        $d = date('m', strtotime($date));
        if(in_array($d, ['1', '2', '3'])) {
            $q = '1st Quarter';
        } elseif(in_array($d, ['4', '5', '6'])) {
            $q = '2nd Quarter';
        } elseif(in_array($d, ['7', '8', '9'])) {
            $q = '3rd Quarter';
        } elseif(in_array($d, ['12', '11', '12'])) {
            $q = '4th Quarter';
        }

        return $q;
    }

    public function getDate($month, $ppmp)
    {
        foreach(['advertisement', 'submission_of_bids', 'notice_of_award', 'contract_signing'] as $column) {
            $m = date('n', strtotime($ppmp->{$column}));
            $months[$m][] = date('j', strtotime($ppmp->{$column}));
        }

        if(!empty($months[$month])) {
            sort($months[$month]);
            return implode(",", $months[$month]);
        }
    }

    public function Category()
    {
        return $this->hasOne('\App\Categories', 'id');
    }

    public function Procurement()
    {
        return $this->belongsTo('\App\ModeOfProcurement', 'mode_of_procurement_id');
    }

    public function SubCategory()
    {
        return $this->belongsTo('\App\SubCategories', 'sub_category_id');
    }
}
