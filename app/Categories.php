<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';

    public function SubCategories()
    {
        return $this->hasMany('App\SubCategories', 'category_id');
    }
}
