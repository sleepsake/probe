<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PAP extends Model
{
    protected $table = 'pap';

    protected $guarded = ['id'];

    public function items()
    {
        return $this->hasMany('App\PAPItems', 'pap_id');
    }

    public function PPMP()
    {
        return $this->hasOne('App\PPMP', 'pap_id');
    }

    public function Category()
    {
        return $this->hasOne('App\Categories', 'category_id');
    }

    public function Cluster()
    {
        return $this->hasOne('App\Cluster', 'id', 'program_id');
    }

    public function Type()
    {
        return $this->hasOne('App\ProgramType', 'id', 'program_type_id');
    }

    public function SubCategory()
    {
        return $this->hasOne('App\Categories', 'sub_category_id');
    }

    public function statusFormat()
    {
        if($this->status == 'Draft') {
            return "<strong class='text-danger'>Draft</strong>";
        } elseif($this->status == 'Submitted') {
            return "<strong class='text-info'>Submitted</strong>";
        } elseif($this->status == 'Approved') {
            return "<strong class='text-success'>Approved</strong>";
        } else {
            return "<strong class='text-warning'>For Revision</strong>";
        }
    }

    public function getCategoryAttribute()
    {
        return $this->Category->name;
    }

    public function getSubCategoryAttribute()
    {
        return $this->SubCategory->name;
    }

    public function User()
    {
        return $this->belongsTo("App\User", 'user_id');
    }

    public function Approver()
    {
        return $this->belongsTo("App\User", 'approver_id');
    }

    public function clusterCode()
    {
        if($this->cluster == 'General Management and Supervision') {
            return 'GAS';
        } elseif($this->cluster == "Processing of veterans' claims") {
            return "MFO 1.1";
        } elseif($this->cluster == 'For educational benefits, expanded hospitalization program and burial benefits') {
            return "MFO 1.2";
        } elseif($this->cluster == 'For the investigation, verification of records, strengthening of internal control system and conduct of management and systems audit') {
            return "MFO 1.3";
        } elseif($this->cluster == 'Administration and management of national military shrines') {
            return "MFO 2.1";
        } elseif($this->cluster == 'Celebration of veteran-related events') {
            return "MFO 2.2";
        } elseif($this->cluster == "Policy Formulation for the promotion of veterans' welfare") {
            return "MFO 3.1";
        }
    }
}
