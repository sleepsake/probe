<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    //
    protected $table = 'categories';

    public function get_by_name($name)
    {
        $item = $this->where('name', '=', $name)->where('is_deleted', '=', 0)->first();
        return $item;
    }

    public function get_by_id($id)
    {
        $item = $this->where('id', '=', $id)->where('is_deleted', '=', 0)->first();
        return $item;
    }

    public function SubCategories()
    {
        return $this->hasMany('App\ItemSubCategory', 'category_id')->where('is_deleted', '=', 0);
    }
}
