<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\SkipsFailures;

class GenerateAPPExport implements FromView
{

    public function view(): View
    {
        return view('app.generate', [
//            'invoices' => Invoice::all()
        ]);
    }
}