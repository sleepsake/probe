<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;

class GeneratePPMPExport implements FromView, WithEvents, WithColumnFormatting
{

    public function view(): View
    {
        return view('ppmp.generate');
    }

    public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {
//                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function(AfterSheet $event) {
//                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getDelegate()->getDefaultColumnDimension()->setWidth(8);
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(23);
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(23);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(18);

                $event->sheet->getDelegate()->getStyle('A7')->getAlignment()->setVertical('center');
                $event->sheet->getDelegate()->getStyle('B7')->getAlignment()->setVertical('center');
                $event->sheet->getDelegate()->getStyle('C7')->getAlignment()->setVertical('center');
                $event->sheet->getDelegate()->getStyle('D7')->getAlignment()->setVertical('center');
                $event->sheet->getDelegate()->getStyle('E7')->getAlignment()->setVertical('center');

//                $event->sheet->getDelegate()->getStyle('E16')->getNumberFormat()->setFormatCode('#,##0.00');

//                $event->sheet->styleCells(
//                    'B2:G8', [
//                        'borders' => [
//                            'outline' => [
//                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
//                                'color' => ['argb' => 'FFFF0000'],
//                            ],
//                        ]
//                    ]
//                );
            },
        ];
    }
}
