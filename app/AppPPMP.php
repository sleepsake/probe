<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppPPMP extends Model
{
    protected $table = "app_ppmps";

    public function PPMP()
    {
        return $this->belongsTo('App\PPMP', 'ppmp_id');
    }

    public function APP()
    {
        return $this->belongsTo('App\APP', 'app_id');
    }

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
