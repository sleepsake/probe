<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model
{
    protected $table = 'sub_categories';

    public function Category()
    {
        return $this->belongsTo('App\Categories', 'category_id');
    }
}
