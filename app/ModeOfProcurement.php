<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModeOfProcurement extends Model
{
    //
    protected $table = 'mode_of_procurement';

    public function get_by_name($name)
    {
        $mode = $this->where('name', '=', $name)->first();
        return $mode;
    }

    public function get_by_id($id)
    {
        $mode = $this->where('id', '=', $id)->first();
        if($mode == null)
        {
            return redirect()->route('dropdowns.mode_of_procurement')->with('pop-error', 'Mode of Procurement non-existent');
        }
        return $mode;
    }
}
