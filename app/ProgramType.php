<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramType extends Model
{
    //
    protected $table = 'program_types';

    public static function get_by_name($name)
    {
        $program = static::
            where('name', '=', $name)->
            where('is_deleted', '=', 0)->first();

        return $program;
    }

    public static function get_by_id($id)
    {
        $program = static::
            where('id', '=', $id)->
            where('is_deleted', '=', 0)->first();

        if($program == null)
        {
            return redirect()->route('dropdowns.programTypes')->with('pop-error', 'Program is non-existent');
        }
        return $program;
    }

    public static function get_all()
    {
        return static::where('is_deleted', '=', 0)->get();
    }
}

