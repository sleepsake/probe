<?php
Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false
]);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clean', function () {
    \App\PAP::truncate();
    \App\PAPItems::truncate();
    \App\PPMP::truncate();
    return 'Success';
});

Route::get('/logout', function() {
   Auth::logout();

   return redirect('/');
});

Route::get('/apb-generate', function() {
    return Excel::download(new \App\Exports\GenerateAPBExport(), 'APB.xlsx');
//    return view(    'apb.generate_apb');
});

// Route::get('/testing', function () {
//     $user = \App\User::find(2);
//     \Auth::login($user);
// });

Route::get('/', function () {
//    return view('welcome');
//    foreach(\App\User::all() as $user) {
//        $user->password = \Illuminate\Support\Facades\Hash::make('password');
//        $user->save();
//    }
})->middleware('auth');

Route::middleware('auth')->prefix('dashboard')->group(function() {

    // MIKO ROUTES
    Route::get('/requests/profile/{pap}', 'ProgramRequestController@profile')->name('request.profile');
    Route::get('/requests/pap/vault/{year?}', 'PAPController@vault')->name('request.pap.vault')->middleware("gatekeep:Super Admin");;


    Route::get('/requests/ppmp/mine', 'PPMPController@mine')->name('request.ppmp.mine');
    Route::get('/requests/ppmp/profile/{id}', 'PPMPController@profile')->name('request.ppmp.profile');
    Route::get('/requests/ppmp/profile/{id}/update', 'PPMPController@profile_update')->name('request.ppmp.profile.update');
    Route::put('/requests/ppmp/profile/{id}/update', 'PPMPController@profile_update_put')->name('request.ppmp.profile.update.put');
    Route::delete('/requests/ppmp/profile/{id}', 'PPMPController@profile_deleted')->name('request.ppmp.profile.delete');
    Route::get('/requests/ppmp/vault/{year?}', 'PPMPController@vault')->name('request.ppmp.vault')->middleware("gatekeep:Super Admin");;
    Route::get('/requests/budget-review', 'ProgramRequestController@forReview')->name('request.review');
    Route::get('/requests/review/{pap}', 'ProgramRequestController@review')->name('request.review.profile');

    Route::get('/requests/ppmp/{category?}', 'ProgramRequestController@scheduling')->name('request.ppmp');
    Route::get('/requests/ppmp/{category?}/create/{array_from_list?}', 'PPMPController@create')->name('request.ppmp.create');
    Route::post('/requests/ppmp/{category?}/create', 'PPMPController@ppmp_create_save')->name('request.ppmp.create.save');

    Route::get('/requests/ppmp/{category?}/create/{array_from_list?}/consolidate/{ppmp_id}', 'PPMPController@consolidate')->name('requests.ppmp.consolidate');

    // Must be below due to the category


    Route::get('/dropdowns', 'DropDownManagementController@home')->name("dropdowns");

    Route::get('/dropdowns/item-categories', 'DropDownManagementController@itemCategories')->name("dropdowns.categories");
    Route::post('/dropdowns/item-categories', 'DropDownManagementController@save_item_categories')->name('dropdowns.save.categories');
    Route::put('/dropdowns/item-categories', 'DropDownManagementController@update_item_categories')->name('dropdowns.update.categories');
    Route::delete('/dropdowns/item-categories', 'DropDownManagementController@delete_item_categories')->name('dropdowns.delete.categories');

    Route::get('/dropdowns/item-categories/{category_id}', 'DropDownManagementController@item_subcategory')->name('dropdowns.item.subcategory');
    Route::post('/dropdowns/item-categories/{category_id}', 'DropDownManagementController@save_item_subcategory')->name('dropdowns.save.item.subcategory');
    Route::put('/dropdowns/item-categories/{category_id}', 'DropDownManagementController@update_item_subcategory')->name('dropdowns.update.item.subcategory');
    Route::delete('/dropdowns/item-categories/{category_id}', 'DropDownManagementController@delete_item_subcategory')->name('dropdowns.delete.item.subcategory');

    Route::get('/dropdowns/program-types', 'DropDownManagementController@programTypes')->name("dropdowns.programTypes");
    Route::post('/dropdowns/program-types', 'DropDownManagementController@save_program_type')->name('dropdowns.save.programTypes');
    Route::put('/dropdowns/program-types}', 'DropDownManagementController@update_program_type')->name('dropdowns.update.programTypes');
    Route::delete('/dropdowns/program-types', 'DropDownManagementController@delete_program_type')->name('dropdowns.delete.programTypes');

    Route::get('/dropdowns/program-and-clusters', 'DropDownManagementController@programClusters')->name("dropdowns.programClusters");
    Route::post('/dropdowns/program-and-clusters', 'DropDownManagementController@save_program_clusters')->name('dropdowns.save.programClusters');
    Route::put('/dropdowns/program-and-clusters', 'DropDownManagementController@update_program_clusters')->name('dropdowns.update.programClusters');
    Route::delete('/dropdowns/program-and-clusters', 'DropDownManagementController@delete_program_clusters')->name('dropdowns.delete.programClusters');

    Route::get('/dropdowns/program-and-clusters/{cluster_id}', 'DropDownManagementController@program_cluster_subcategory')->name('dropdowns.programClusters.subcategory');
    Route::post('/dropdowns/program-and-clusters/{cluster_id}', 'DropDownManagementController@save_program_cluster_subcategory')->name('dropdowns.save.programClusters.subcategory');
    Route::put('/dropdowns/program-and-clusters/{cluster_id}', 'DropDownManagementController@update_program_cluster_subcategory')->name('dropdowns.update.programClusters.subcategory');
    Route::delete('/dropdowns/program-and-clusters/{cluster_id}', 'DropDownManagementController@delete_program_cluster_subcategory')->name('dropdowns.delete.programClusters.subcategory');

    Route::get('/dropdowns/units', 'DropDownManagementController@units')->name("dropdowns.units");
    Route::post('/dropdowns/units', 'DropDownManagementController@save_unit_of_measurement')->name('dropdowns.save.units');
    Route::put('/dropdowns/units', 'DropDownManagementController@update_unit_of_measurement')->name('dropdowns.update.units');
    Route::delete('/dropdowns/units', 'DropDownManagementController@delete_unit_of_measurement')->name('dropdowns.delete.units');

    Route::get('/dropdowns/mode-of-procurement', 'DropDownManagementController@modeOfProcurement')->name("dropdowns.mode_of_procurement");
    Route::post('/dropdowns/mode-of-procurement', 'DropDownManagementController@save_mode_of_procurement')->name('dropdowns.save.mode_of_procurement');
    Route::put('/dropdowns/mode-of-procurement', 'DropDownManagementController@update_mode_of_procurement')->name('dropdowns.update.mode_of_procurement');
    Route::delete('/dropdowns/mode-of-procurement', 'DropDownManagementController@delete_mode_of_procurement')->name('dropdowns.delete.mode_of_procurement');

    Route::get('/dropdowns/divisions', 'DropDownManagementController@divisions')->name("dropdowns.divisions");
    Route::post('/dropdowns/divisions', 'DropDownManagementController@save_divisions')->name('dropdowns.save.divisions');
    Route::put('/dropdowns/divisions', 'DropDownManagementController@update_divisions')->name('dropdowns.update.divisions');
    Route::delete('/dropdowns/divisions', 'DropDownManagementController@delete_divisions')->name('dropdowns.delete.divisions');

    Route::get('/banner/form', 'BannerManagementController@showBannerForm')->name("banner.form");

    // END
    //mark
    Route::post('/banner/form/update', 'BannerManagementController@update')->name('banner.form.update');
    Route::post('/request/pap/update/{pap}/{status}', 'ProgramRequestController@updateStatus')->name('request.status.update');
    Route::post('/request/pap/item/add_remarks', 'ProgramRequestController@addRemarks')->name('request.item.add_remarks');
    Route::post('/request/pap/update_remarks/{pap}', 'ProgramRequestController@UpdateRemarks')->name('request.update_remarks');
    //end

    Route::get('/', 'DashboardController@index');

    Route::get('/pap', 'PAPController@index')->name('pap');
    Route::get('/pap/status/{status?}', 'PAPController@index')->name('pap.status');
    Route::get('/pap/create', 'PAPController@create')->name('pap_create');
    Route::post('/pap/create', 'PAPController@postCreate');
    Route::get('/pap/{pap}', 'PAPController@update')->name('pap_update');
    Route::post('/pap/{pap}', 'PAPController@postUpdate');
    Route::delete('/pap/delete/{pap_id}', 'PAPController@delete_pap')->name('pap_delete');

    Route::get('/cse', 'CSEController@index')->name('cse_list');
    Route::post('/cse/import_excel', 'CSEController@import_excel')->name('cse_import_excel');
    Route::get('/cse/create', 'CSEController@create_form')->name('create_cse_form');
    Route::post('/cse/create', 'CSEController@create_save')->name('create_cse_save');
    Route::get('/cse/update/{id}', 'CSEController@update_form')->name('update_cse_form');
    Route::put('/cse/update/{id}', 'CSEController@update_save')->name('update_cse_save');
    Route::delete('/cse/delete/{id}', 'CSEController@delete_cse')->name('delete_cse');
    Route::get('/cse/app', 'CSEController@app');

    Route::get('/apb', 'APBController@index');
    Route::get('/apb/create', 'APBController@create');
    Route::get('/apb/{apb}', 'APBController@view')->name('apb_update');
    Route::post('/apb/{apb}', 'APBController@postUpdate');
    Route::get('/ppmp', 'PPMPController@index');
    Route::get('/ppmp/items', 'PPMPController@items');
    Route::get('/ppmp/create', 'PPMPController@create');
    Route::post('/ppmp/create', 'PPMPController@postCreate');
    Route::post('/ppmp/getAction', 'PPMPController@getAction');


    Route::get('/ppmp/generate', function() {
        return Excel::download(new \App\Exports\GeneratePPMPExport(), 'PPMP.xlsx');
//        return view(    'ppmp.generate');
    });

    Route::get('/ppmp/{pap}', 'PPMPController@view')->name('ppmp_update');
    Route::post('/ppmp/{pap}', 'PPMPController@postUpdate');

    Route::get('/app/list/{year?}', 'APPController@index')->name('app.list');
    Route::get('/app/add', 'APPController@add')->name('app.add');
    Route::post('/app/add', 'APPController@postAdd')->name('app.add.post');
    Route::get('/app/update/{app}', 'APPController@update')->name('app.update');
    Route::post('/app/update/{app}', 'APPController@postUpdate')->name('app.update.post');
    Route::post('/app/addToAPP', 'APPController@addToApp');
    Route::get('/app/items', 'APPController@items');
    Route::get('/app/generate', function() {
        return Excel::download(new \App\Exports\GenerateAPPExport(), 'APP.xlsx');
//        return view('app.generate');
    })->name('app_generate');

    Route::get('/user-management', 'UserManagementController@index');
    Route::get('/user-management/add', 'UserManagementController@add');
    Route::post('/user-management/add', 'UserManagementController@postAdd');
    Route::get('/user-management/{user}', 'UserManagementController@update')->name('update_user');
    Route::post('/user-management/{user}', 'UserManagementController@postUpdate');
});



Route::get('/home', 'HomeController@index')->name('home');



Route::get('/populate/dropdowns', function() {
    DB::select(DB::raw("TRUNCATE program_types"));

    foreach(['Program', 'Activity', 'Project'] as $name) {
        $pap = new \App\ProgramType();
        $pap->name = $name;
        $pap->save();
    }


    DB::select(DB::raw("TRUNCATE programs"));
    DB::select(DB::raw("TRUNCATE programs_clusters"));

    $programs = [
        'GAS' => ['GAS 1.1'],
        "MFO 1" => [
            'MFO 1.1',
            'MFO 1.2',
            'MFO 1.3',
        ],
        'MFO 2' => [
            'MFO 2.1',
            'MFO 2.2'
        ],
        "MFO 3" => ['MFO 3.1']
    ];

    foreach($programs as $program => $clusters) {
        $p = new \App\Cluster();
        $p->name = $program;
        $p->save();

        foreach($clusters as $cluster) {
            $c = new \App\ClusterSubcategory();
            $c->program_id = $p->id;
            $c->name = $cluster;
            $c->save();
        }
    }

//    $c = \App\Categories::with('SubCategories')->get();
//    echo $c->toJson();

    DB::select(DB::raw("TRUNCATE categories"));
    DB::select(DB::raw("TRUNCATE sub_categories"));

    $categories = '[{"id":1,"name":"TRAVELLING EXPENSE","is_procurement":"NP","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":1,"category_id":1,"name":"Local Travel","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":2,"category_id":1,"name":"Foreign Travel","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":2,"name":"TRAINING & SCHOLARSHIP EXPENSES","is_procurement":"NP","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":3,"category_id":2,"name":"ICT Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":4,"category_id":2,"name":"Training\/Seminar Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":3,"name":"UTILITY EXPENSE","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":5,"category_id":3,"name":"Water","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":6,"category_id":3,"name":"Electricity","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":4,"name":"EXTRAORDINARY & MISC. EXPENSE","is_procurement":"NP","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":7,"category_id":4,"name":"Extraordinary & Misc. Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":5,"name":"PROFESSIONAL SERVICES EXPENSE","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":8,"category_id":5,"name":"Legal Services","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":9,"category_id":5,"name":"Auditing Services","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":10,"category_id":5,"name":"ICT Consultancy Services","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":11,"category_id":5,"name":"Other Professional Services","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":6,"name":"GENERAL SERVICES EXPENSE","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":12,"category_id":6,"name":"Janitorial Services","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":13,"category_id":6,"name":"Security Services","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":14,"category_id":6,"name":"Other General Services","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":7,"name":"TAXES, INS PREM. & OTHER FEES","is_procurement":"NP","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":15,"category_id":7,"name":"Taxes, Duties and Licenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":16,"category_id":7,"name":"Fidelity Bond Premiums","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":17,"category_id":7,"name":"Insurance Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":8,"name":"ADVERTISING EXPENSES","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":18,"category_id":8,"name":"Advertising Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":9,"name":"REPRESENTATION EXPENSES","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":19,"category_id":9,"name":"Representation Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":10,"name":"TRANSPORTATION & DELIVERY EXP","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":20,"category_id":10,"name":"Transportation & Delivery Exp","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":11,"name":"RENT\/LEASE EXPENSES","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":21,"category_id":11,"name":"Building and Structures","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":22,"category_id":11,"name":"Motor Vehicles","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":23,"category_id":11,"name":"Equipment","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":13,"name":"SUPPLIES & MATERIALS EXPENSE","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":27,"category_id":13,"name":"ICT Office Supplies","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":28,"category_id":13,"name":"Offiice Supplies Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":29,"category_id":13,"name":"Accountable Forms Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":30,"category_id":13,"name":"Med\/Dental Laboratry Supplies","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":31,"category_id":13,"name":"Fuel, oil & Lubricants","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":32,"category_id":13,"name":"Other Supplies Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":14,"name":"COMMUNICATION SERVICES EXPENSE","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":33,"category_id":14,"name":"Postage & Courier Services","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":34,"category_id":14,"name":"Telephone Exp-Mobile","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":35,"category_id":14,"name":"Telephone Exp-Landline","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":36,"category_id":14,"name":"Internet Subscription Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":37,"category_id":14,"name":"Cable,Telegrap\/Radio Exp","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":15,"name":"REPAIR & MAINTENANCE EXPENSE","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":38,"category_id":15,"name":"Buildings","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":39,"category_id":15,"name":"Other Structures","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":40,"category_id":15,"name":"Machinery","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":41,"category_id":15,"name":"Office Equipment","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":42,"category_id":15,"name":"ICT Equipment","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":43,"category_id":15,"name":"Motor Vehicles","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":44,"category_id":15,"name":"Furniture & Fixtures","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":45,"category_id":15,"name":"Other Property Plant & Eqpt","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":16,"name":"PRINTING AND PUBLICATION EXP","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":46,"category_id":16,"name":"Printing & Publication Expenses","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]},{"id":17,"name":"SUBCRIPTION EXPENSES","is_procurement":"P","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49","sub_categories":[{"id":47,"category_id":17,"name":"ICT Software Subscription","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"},{"id":48,"category_id":17,"name":"Other Subscription Expense","created_at":"2019-12-17 03:04:49","updated_at":"2019-12-17 03:04:49"}]}]';
    $categories = collect(json_decode($categories));

//    dd($categories);

    foreach($categories as $category) {
        $c = new \App\Categories();
        $c->name = $category->name;
        $c->is_procurement = $category->is_procurement;
        $c->save();


        foreach($category->sub_categories as $subCategory) {
            $sc = new \App\SubCategories();
            $sc->category_id = $c->id;
            $sc->name = $subCategory->name;
            $sc->save();
        }
    }


    DB::select(DB::raw("TRUNCATE mode_of_procurement"));

    $modeOfProcurements = [
        'Competitive Bidding',
        'Alternative Bidding - Small Value Procurement (Shopping)',
        'Alternative Bidding - Negotiated Procurement',
        'Alternative Bidding - Emergency Purchases Alternative Bidding - Repeat Order',
        'Alternative Bidding - Limited Source Bidding Alternative Bidding - Direct',
        'Contracting Alternative Bidding - Lease of Real Property and Venue'
    ];

    foreach($modeOfProcurements as $modeOfProcurement) {
        $mode = new \App\ModeOfProcurement();
        $mode->name = $modeOfProcurement;
        $mode->save();
    }



    DB::select(DB::raw("TRUNCATE users_divisions"));

    $divisions = [
        'VAMD',
        'VMHD',
        'VRMD',
        'MID',
        'LAD',
        'PMD',
        'ADMIN',
        'OA',
        'PMS',
        'GSS',
        'PAO'
    ];

    foreach($divisions as $division) {
        $d = new \App\Division();
        $d->name = $division;
        $d->description = '';
        $d->save();
    }

});
