@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('app.list') }}">App</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add APP Entry</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <div class="col-12">
                <h2 class="h2 d-inline-block">APP <small>(Add Entry)</small></h2>
                <form method="POST">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" class="form-control" id="" name="title" value="{{ old('title') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="">Year</label>
                                    <input class="form-control" value="{{ date("Y") }}" disabled>
                                </div>

                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" class="form-control">{{ old("description") }}</textarea>
                                </div>
                            </div>
                            <div class="col-8">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>PPMP Name</th>
                                            <th>Has APP?</th>
                                            <th>Category</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($ppmps as $key => $p)
                                            @php
                                                $category = \App\Categories::where('id', '=', $p->category_id)->first();
                                            @endphp
                                            <tr>
                                                <td> <input type="checkbox" name="ppmps[]" value="{{$p->id}}"> </td>
                                                <td>{{$p->ppmp_name}}</td>
                                                <td>
                                                    @if (count($p->APP) > 0)
                                                        <ul>
                                                            @foreach ($p->APP as $app)
                                                                <li>{{$app->APP->title}}</li>
                                                            @endforeach
                                                        </ul>
                                                    @else
                                                        None
                                                    @endif
                                                </td>
                                                <td>{{ $category->name }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="margin-top:10px">
                            <div class="col-12">
                                <hr>
                                <div class="text-center">
                                    <a href="{{ url('/dashboard/user-management') }}" class="btn btn-default">Cancel</a>
                                    <button id="" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {

            $(document).on("change","#role_select",function(e){
                role = $("#role_select").val();

                if (role != "User Admin")
                {
                    $("#allowed_categories").show();
                }
                else
                {
                    $("#allowed_categories").hide();
                }
            });

        });
    </script>
@endsection()
