@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('app.list') }}">App</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add APP Entry</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <div class="col-12">
                <h2 class="h2 d-inline-block">APP <small>(Update Entry)</small></h2>
                <form method="POST" action="{{route('app.update', $app->id)}}">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" class="form-control" id="" name="title" value="{{ old('title') ?? $app->title }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="">Year</label>
                                    <input class="form-control" value="{{ date("Y",strtotime($app->created_at)) }}" disabled>
                                </div>

                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" class="form-control">{{ old("description") ?? $app->description }}</textarea>
                                </div>
                            </div>
                            <div class="col-8">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>PPMP Name</th>
                                            <th>Has APP?</th>
                                            <th>Category</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($ppmps as $key => $p)
                                            @php
                                                $category = \App\Categories::where('id', '=', $p->category_id)->first();
                                            @endphp
                                            <tr>
                                                <td>
                                                    @if ($app->PPMPs->where("ppmp_id",$p->id)->count() > 0)
                                                        <input type="checkbox" name="ppmps[]" value="{{$p->id}}" checked>
                                                    @else
                                                        <input type="checkbox" name="ppmps[]" value="{{$p->id}}">
                                                    @endif
                                                </td>
                                                <td>{{$p->ppmp_name}}</td>
                                                <td>
                                                    @if (count($p->APP) > 0)
                                                        <ul>
                                                            @foreach ($p->APP as $a)
                                                                <li>{{$a->APP->title}}</li>
                                                            @endforeach
                                                        </ul>
                                                    @else
                                                        None
                                                    @endif
                                                </td>
                                                <td>{{ $category->name }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="margin-top:10px">
                            <div class="col-12">
                                <hr>
                                <div class="text-center">
                                    <a href="{{ route('app.list') }}" class="btn btn-default">Cancel</a>
                                    <button id="" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {

            $(document).on("change","#role_select",function(e){
                role = $("#role_select").val();

                if (role != "User Admin")
                {
                    $("#allowed_categories").show();
                }
                else
                {
                    $("#allowed_categories").hide();
                }
            });

        });
    </script>
@endsection()
