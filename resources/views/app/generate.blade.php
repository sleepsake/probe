<?php
$apps = \DB::select(
    "SELECT ppmp.*, categories.name as category, sc.name as sub_category FROM app_ppmps INNER JOIN ppmp ON ppmp.id = app_ppmps.ppmp_id
     LEFT JOIN categories ON categories.id = ppmp.category_id LEFT JOIN sub_categories sc ON sc.id = ppmp.sub_category_id"
    );
$apps = collect($apps)->groupBy('category');

?>
<table>
    <thead>

    <tr>
        <th rowspan="2">Code (PAP)</th>
        <th rowspan="2">Procurement Program/Project</th>
        <th rowspan="2">PMO / End-User</th>
        <th rowspan="2">Mode of Procurement</th>
        <th colspan="4">Schedule of Each Procurement Activity</th>
        <th rowspan="2">Source of Funds</th>
        <th colspan="3">Estimated Budget (PhP)</th>
        <th rowspan="2">Remarks</th>
    </tr>

    <tr>
        <th>Advertisement / Posting of IB / REI</th>
        <th>Submission / Opening of Bids</th>
        <th>Notice of Award</th>
        <th>Contract Signing</th>

        <th>Total</th>
        <th>MOOE</th>
        <th>CO</th>
    </tr>

    </thead>
    <tbody>
        @php $x = 1; @endphp
        @foreach($apps as $category => $subCategories)
            <tr>
                <td colspan="13">{{ "{$x}. {$category}" }}</td>
            </tr>

            @foreach($subCategories->groupBy('sub_category') as $subCategory => $app)
                @foreach($app as $a)
                    @php
                        $ppmp = \App\PPMP::find($a->id);


                    @endphp
                    <tr>
                        <td>{{ implode(\App\PAP::whereIn('id', \App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->get()->pluck('pap_id'))->get()->pluck('program')->toArray(), ', ') }}</td>
                        <td>{{ $ppmp->general_description }}</td>
                        <td>{{ $ppmp->User->department }}</td>
                        <td>{{ $ppmp->mode_of_procurement }}</td>
                        <td>{{ $ppmp->getQuarter($ppmp->advertisement) }}</td>
                        <td>{{ $ppmp->getQuarter($ppmp->submission_of_bids) }}</td>
                        <td>{{ $ppmp->getQuarter($ppmp->notice_of_award) }}</td>
                        <td>{{ $ppmp->getQuarter($ppmp->contract_signing) }}</td>
                        <td>GOP</td>
                        <td>{{ \App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->get()->sum('total_cost') }}</td>
                        <td>{{ \App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->get()->sum('total_cost') }}</td>
                        <td>0</td>
                        <td></td>
                    </tr>
                @endforeach

            @endforeach

        @endforeach
    </tbody>
</table>
