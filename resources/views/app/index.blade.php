@extends('layouts.dashboard')

@section('main')
    <div class="container cm-container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">APP</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">{{ $year }} - ANNUAL PROCUREMENT PLAN (APP)</h2>
                    </div>

                    <div class="col-auto">
                        <a href="{{route('app.add')}}" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add APP</a>
                    </div>
                </div>

                <hr>

                <ul class="nav nav-pills nav-fill">
                    @for ($i = date("Y") ; $i >= date("Y") - 5; $i--)
                        <li class="nav-item">
                            <a href="{{route('app.list', $i)}}" class="nav-link <?php echo ($i == $year)? "active":"";?>" href="#">{{$i}}</a>
                        </li>
                    @endfor
                </ul>

            </div>
        </div>
    </div>

    <div class="container cm-container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>App Title</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($apps as $app)
                                <tr>
                                    <td>{{ $app->title }}</td>
                                    <td>{{ $app->description }}</td>
                                    <td>

                                        <div class="btn-group">
                                            <a href="{{route('app.update', $app->id)}}" class="btn btn-sm btn-primary"> <i class="fa fa-edit"></i> Update </a>
                                            <a href="{{ route('app_generate') }}" class="btn btn-sm btn-light"> <i class="fa fa-file-excel"></i> Export </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable();

        });
    </script>
@endsection()
