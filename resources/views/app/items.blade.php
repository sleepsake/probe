@extends('layouts.dashboard')

@section('main')
    <div class="container cm-container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">APP</li>
                    </ol>
                </nav>

                <a href="{{ url('/dashboard/app/generate') }}" class="btn btn-sm btn-primary mt-5 float-right">Generate APP</a>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">{{ date('Y') }} - ANNUAL PROCUREMENT PLAN (APP)</h2>
                    </div>

                    <div class="col-auto">
                        <a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add APP</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container cm-container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Procurement Program/Project</th>
                                <th>PMO</th>
                                <th>Quantity</th>
                                <th>Mode Of Procurement</th>
                                <th>Estimated Budget</th>
                                <th>Schedule / Milestones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($apps as $app)
                                @php $ppmp = $app->PPMP @endphp
                                <tr>
                                    <td>{{ implode(\App\PAP::whereIn('id', \App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->get()->pluck('pap_id'))->get()->pluck('program')->toArray(), ', ') }}</td>
                                    <td>{{ $ppmp->general_description }}</td>
                                    <td>{{ $ppmp->User->department }}</td>
                                    <td>P {{ number_format(\App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->get()->sum('quantity'), 2) }}</td>
                                    <td>{{ $ppmp->mode_of_procurement }}</td>
                                    <td>P {{ number_format(\App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->get()->sum('total_cost'), 2) }}</td>
                                    <td>{{ $ppmp->Schedule() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection()
