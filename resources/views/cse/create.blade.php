@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard/pap') }}">PAP</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Program Activity Project</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">CREATE PROGRAM ACTIVITY PROJECTS (PAPs)</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('create_cse_save') }}" method="POST">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="">Product Code</label>
                                    <input type="text" name="cse_product_code" class="form-control" id="" placeholder="">
                                </div>
                                <div class="form-group col-6">
                                    <label for="">Item Description</label>
                                    <input type="text" name="cse_item_description" class="form-control" id="" placeholder="">
                                </div>
                                <div class="form-group col">
                                    <label for="">Unit of Measure</label>
                                    <input type="text" name="cse_unit_of_measure" class="form-control" id="" placeholder="">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="">Category</label>
                                    <input type="text" name="cse_category" class="form-control" id="" placeholder="">
                                </div>
                                <div class="form-group col">
                                    <label for="">Price</label>
                                    <input type="number" name="cse_price" step="0.1" class="form-control" id="" placeholder="">
                                </div>
                            </div>

                            <hr>

{{--                            <button type="button" class="btn btn-sm btn-primary mb-5"><i class="fa fa-plus"></i> Add item</button>--}}


{{--                            <table id="example" class="table " style="width:100%">--}}
{{--                                <thead>--}}
{{--                                <tr>--}}
{{--                                    <th>Item / Requirements</th>--}}
{{--                                    <th>Account Title</th>--}}
{{--                                    <th>Quantity</th>--}}
{{--                                    <th>Unit Cost</th>--}}
{{--                                    <th>Total Amount</th>--}}
{{--                                    <th>Procurement (P) or Non Procurement (NP)</th>--}}
{{--                                </tr>--}}
{{--                                </thead>--}}
{{--                                <tbody>--}}
{{--                                    <tr>--}}
{{--                                        <td>Meals</td>--}}
{{--                                        <td>REPRESENTATION TAXES</td>--}}
{{--                                        <td>50</td>--}}
{{--                                        <td>P 1,500.00</td>--}}
{{--                                        <td>P 75,000.00</td>--}}
{{--                                        <td>P</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <td>Tarpaulin</td>--}}
{{--                                        <td>OTHER SUPPLIES AND MATERIAL EXPENSES</td>--}}
{{--                                        <td>1</td>--}}
{{--                                        <td>P 3,000.00</td>--}}
{{--                                        <td>P 3,000.00</td>--}}
{{--                                        <td>P</td>--}}
{{--                                    </tr>--}}
{{--                                </tbody>--}}
{{--                                <tfoot>--}}
{{--                                    <tr>--}}
{{--                                        <th colspan="3"></th>--}}
{{--                                        <th>Total Amount</th>--}}
{{--                                        <th>P 78,000,000.00</th>--}}
{{--                                        <th></th>--}}
{{--                                    </tr>--}}
{{--                                </tfoot>--}}
{{--                            </table>--}}

                            <div class="text-right">
                                <button type="button" class="btn btn-default">Cancel</button>
                                <button type="submit" id="submitPAP" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            // $('#submitPAP').click(function() {
            //     Swal.fire({
            //         type: 'success',
            //         title: 'Success!',
            //         html: `Program / Activity / Project Added <br>
            //                 <strong>PAP Code: 300100200400</strong>`
            //     })
            // });
        });
    </script>
@endsection()
