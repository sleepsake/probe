@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">CSE</li>
                    </ol>
                </nav>

            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">{{ date('Y') }} - COMMON SUPPLIES AND EQUIPMENT</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link " href="#">PS-DBM CSE</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="">APP-CSE</a>
                            </li>
                        </ul>

                        <hr>

                        <div class="row">
                            <div class="col-3">
                                <p>Item Category</p>
                                <div class="form-group">
                                    <select name="" id="" class="form-control">
                                        <option value="">VIEW ALL ITEMS</option>
                                        <option value="">Pesticides or Pest Repellents</option>
                                        <option value="">Solvents</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <br>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Product Code</th>
                                    <th>Item Description</th>
                                    <th>Unit of Measure</th>
                                    <th>Category</th>
                                    <th>Total QTY</th>
                                    <th>Total Price</th>
                                    <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-group form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        </div>
                                    </td>
                                    <td>10191509-IN-A01</td>
                                    <td>INSECTISIDE, aerosol type, net content: 600ml min</td>
                                    <td>can</td>
                                    <td>Pesticides or Pest Repellents</td>
                                    <td>20</td>
                                    <td>P 139.36</td>
                                    <td>P 139.36</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-group form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        </div>
                                    </td>
                                    <td>10191601-IN-A01</td>
                                    <td>ALCOHOL, ethyl, 68%-70% scented, 500ml (-5ml)</td>
                                    <td>bottle</td>
                                    <td>Solvents</td>
                                    <td>40</td>
                                    <td>P 3,992</td>
                                    <td>P 3,992</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection()