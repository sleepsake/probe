@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">CSE</li>
                    </ol>
                </nav>

                <button class="btn btn-sm btn-primary mt-5 float-right">GENERATE CSE</button>
                <a href="{{ route('create_cse_form') }}" class="btn btn-sm btn-primary mt-5 float-right mr-2">Add CSE</a>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">{{ date('Y') }} - COMMON SUPPLIES AND EQUIPMENT</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">PS-DBM CSE</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/dashboard/cse/app') }}">APP-CSE</a>
                            </li>
                        </ul>

                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ route('cse_import_excel') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">Import Excel File</label>
                                        <input type="file" name="cse_import_excel"
                                               class="form-control-file" id="exampleFormControlFile1"
                                               accept="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                        >
                                        <button class="btn btn-success">Upload Excel</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <hr />

                        <div class="row">
                            <div class="col-4">
                                <p>Item Category</p>
                                <div class="form-inline">
                                    <select name="" id="" class="form-control">
                                        <option value="">VIEW ALL ITEMS</option>
                                        <option value="">Pesticides or Pest Repellents</option>
                                        <option value="">Solvents</option>
                                    </select>
                                    <button class="btn btn btn-dark ml-3" data-toggle="modal" data-target="#addItemModal"><i class="fa fa-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>

                        <br>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Product Code</th>
                                    <th>Item Description</th>
                                    <th>Unit of Measure</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cses as $cse)
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-group form-check">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                            </div>
                                        </td>
                                        <td>{{ $cse->product_code }}</td>
                                        <td>{{ $cse->item_description }}</td>
                                        <td>{{ $cse->unit_of_measure }}</td>
                                        <td>{{ $cse->category }}</td>
                                        <td>₱ {{ number_format($cse->price, 2) }}</td>
                                        <th>
                                            <a href="{{ route('update_cse_form', ['id' => $cse->id]) }}" class="btn btn-sm btn-primary">Edit</a>
                                            <form action="{{ route('delete_cse', ['id' => $cse->id]) }}" method="POST">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-sm btn-danger">Delete</button>
                                            </form>
                                        </th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addItemModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 80%;">
            <div class="modal-content">
                <div class="form">
                    <div class="modal-body">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>UOM</th>
                                    <th>Price</th>
                                    <th>1st QTR <br>QTY</th>
                                    <th>1st QTR <br>QTY</th>
                                    <th>1st QTR <br>QTY</th>
                                    <th>1st QTR <br>QTY</th>
                                    <th>Total</th>
                                    <th>Total QTY <br>for the year</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>10191509-IN-A01</td>
                                    <td>Insecticide, aerosol type, net content: 600ml min</td>
                                    <td>can</td>
                                    <td>P 136.36</td>
                                    <td><div class="form-inline"><input type="text" class="form-control" value="0" style="width: 50px;"></div></td>
                                    <td><div class="form-inline"><input type="text" class="form-control" value="0" style="width: 50px;"></div></td>
                                    <td><div class="form-inline"><input type="text" class="form-control" value="0" style="width: 50px;"></div></td>
                                    <td><div class="form-inline"><input type="text" class="form-control" value="0" style="width: 50px;"></div></td>
                                    <td>P 0.00</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>12191601-IN-A01</td>
                                    <td>Insecticide, aerosol type, net content: 600ml min</td>
                                    <td>can</td>
                                    <td>P 43.992</td>
                                    <td><div class="form-inline"><input type="text" class="form-control" value="0" style="width: 50px;"></div></td>
                                    <td><div class="form-inline"><input type="text" class="form-control" value="0" style="width: 50px;"></div></td>
                                    <td><div class="form-inline"><input type="text" class="form-control" value="0" style="width: 50px;"></div></td>
                                    <td><div class="form-inline"><input type="text" class="form-control" value="0" style="width: 50px;"></div></td>
                                    <td>P 0.00</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class="modal-footer">
                        <button type="button" id="closeModal" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="addItemSubmit" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Add Item</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable();

            $('#addItemSubmit').click(function() {
                $("#closeModal").trigger('click');
                Swal.fire({
                    type: 'success',
                    title: 'Success!',
                    html: `Items successfully added to <br>
                            <strong>APP-CSE {{ date('Y') }}</strong>`
                }).then((result) => {

                });
            });


            @if(isset($success_create_cse))
                alert('test');
            @endif

            @if(session('success'))
                Swal.fire({
                type: 'success',
                title: 'Success!',
                html: `Items successfully added to <br>
                            <strong>APP-CSE {{ date('Y') }}</strong>`
                });
            @endif
        });
    </script>
@endsection()
