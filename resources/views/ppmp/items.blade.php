@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">PPMP</li>
                    </ol>
                </nav>

                <a href="{{ url('/dashboard/ppmp/generate') }}" class="btn btn-sm btn-primary mt-5 float-right">Generate PPMP</a>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">{{ date('Y') }} - PROGRAM ACTIVITY PROJECTS (PAPs)</h2>
                    </div>

                    <!-- <div class="col-auto">
                        <a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add new entry</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/dashboard/ppmp') }}">PAP</a>
                            </li>
                            <li class="nav-item">
                                @if(\App\PPMP::count() == 0)
                                <a class="nav-link disabled" href="#">PPMP</a>
                                @else
                                    <a class="nav-link active" href="#">PPMP</a>
                                @endif
                            </li>
                        </ul>

                        <hr>

                        <div>
                            <form action="">
                                <div class="form-row">
                                    <div class="form-group col-4">
                                        {{--<label>PAP Type</label>--}}
                                        {{--<select id="selectPAPType" class="form-control" name="type">--}}
                                            {{--<option value="All" {{ Request::get('type') == 'All' ? 'selected' : '' }}>All</option>--}}
                                            {{--<option value="Program" {{ Request::get('type') == 'Program' ? 'selected' : '' }}>Program</option>--}}
                                            {{--<option value="Activity" {{ Request::get('type') == 'Activity' ? 'selected' : '' }}>Activity</option>--}}
                                            {{--<option value="Project" {{ Request::get('type') == 'Project' ? 'selected' : '' }}>Project</option>--}}
                                        {{--</select>--}}
                                        <label>Category</label>
                                        <select id="selectCategory" class="form-control" name="category">
                                            @foreach(\App\Categories::orderBy('name')->get() as $category)
                                                <option value="{{ $category->id }}" {{ request('category') == $category->id ? 'selected' : '' }}>{{ $category->name }} ({{ \App\PPMP::where('category_id', $category->id)->count() }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{--<div class="form-group col-3">--}}
                                        {{--<label class="d-block">&nbsp;</label>--}}
                                        {{--<button type="button" id="goToPPMPForm" class="btn btn-primary d-inline-block mt-1">Submit</button>--}}
                                    {{--</div>--}}
                                </div>
                            </form>
                        </div>

                        <br>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Program</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Mode Of Procurement</th>
                                <th>Estimated Budget</th>
                                <th>Schedule / Milestones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ppmps as $ppmp)
                                <tr>
                                    <td>{{ implode(\App\PAP::whereIn('id', \App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->get()->pluck('pap_id'))->get()->pluck('program')->toArray(), ', ') }}</td>
                                    <td>{{ $ppmp->general_description }}</td>
                                    <td>
                                        {{ \App\SubCategories::find($ppmp->sub_category_id) ? \App\SubCategories::find($ppmp->sub_category_id)->name : '' }}
                                    </td>
                                    <td>{{ $ppmp->mode_of_procurement }}</td>
                                    <td>P {{ number_format(\App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->get()->sum('total_cost'), 2) }}</td>
                                    <td>{{ $ppmp->Milestones() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable();

            $("#selectCategory").change(function() {
                $(this).closest('form').submit();
            })
        });
    </script>
@endsection()
