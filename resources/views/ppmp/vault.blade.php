@extends('layouts.dashboard')

@section('main')
    <div class="container cm-container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">PAP</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">PROJECT PROCUREMENT MANAGEMENT PLANS (PPMPs)</h2>

                        <div class="alert subtle">
                            <i class="fa fa-exclamation-circle"></i> Select a year to see all PPMP Archives for that year.
                        </div>

                    </div>

                    <!-- <div class="col-auto">
                        <a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add new entry</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="container cm-container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                @php
                    $years = ['2020','2019','2018','2017','2016','2015']
                @endphp
                <nav  class="nav nav-pills selector nav-justified cm-pills">
                    @foreach($years as $y)
                        <a href="{{route('request.ppmp.vault', $y)}}" class="nav-item nav-link <?php echo ($y == $year)? "active":"";?>" style="margin-right:5px;margin-top:5px">{{$y}}</a>
                    @endforeach
                </nav>

                <div class="card" style="margin-top:10px">
                    <div class="card-body">
                        @php
                            $reviewers = ['Bruce Wayne', 'Oliver Queen', 'Selena Kyle', ' Diana Prince', 'Barry Allen', 'Clark Kent']
                        @endphp
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:115px">Timestamp</th>
                                    <th style="width:115px">PPMP Name</th>
                                    <th>Project Description</th>
                                    <th style="width:200px">Category</th>
                                    {{-- <th style="width:65px">Function</th> --}}
                                    <th style="width:115px">Budget Estimate</th>
                                    <th style="width:75px">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($ppmps as $ppmp)
                                    @php
                                        $cost = [];
                                        foreach($ppmp->Items as $item){

                                            $pap_item = \App\PAPItems::where('id', '=', $item->pap_item_id)->first();
                                            array_push($cost, $pap_item->total_cost);
                                        }
                                        $estimated_cost = array_sum($cost);
                                    @endphp
                                    <tr>
                                        <td>{{ date('F d, Y', strtotime($ppmp->created_at)) }}</td>
                                        <td>{{ $ppmp->ppmp_name ?? ''}} </td>
                                        <td>{{ $ppmp->general_description }}</td>
                                        <td>{{ $ppmp->Category->name }}</td>
                                        <td>{{ number_format($estimated_cost, 2) }}</td>
                                        <td>
                                        <div class="dropdown">
                                            <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{ route('request.ppmp.profile', ['id' => $ppmp->id]) }}">View Profile</a>
                                                <a class="open-modal dropdown-item" data-content="#ppmp_{{ $ppmp->id }}">Item QuickView</a>
                                            </div>

                                            <div class="modal fade" id="ppmp_{{$ppmp->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">items</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table class="table table-striped table-bordered">
                                                                <thead>
                                                                <tr>Item Name</tr>
                                                                </thead>
                                                                <tbody>
                                                                @php
                                                                    $items = \App\PPMPItems::where('ppmp_id', '=', $ppmp->id)->get();
                                                                @endphp
                                                                @foreach($items as $item)
                                                                    <tr>
                                                                        <td>{{ $item->PAPItem->item_name }}</td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal -->



    <script>
        $(document).ready(function() {
            $('#example').DataTable();

            $('.open-modal').click(function(){
                console.log('test');
               let data_content = $(this).attr('data-content');
                $(data_content).modal();
            });
        });
    </script>
@endsection()
