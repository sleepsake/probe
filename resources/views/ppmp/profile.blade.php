@extends('layouts.dashboard')

@section('main')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('request.ppmp.mine') }}">PPMP List</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $ppmp->ppmp_name }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <a href="{{ route('request.ppmp.profile.update', ['id' => $ppmp->id])  }}" style="float:right" class="btn btn-outline-info"><i class="fa fa-edit"></i> Update</a>
                        <h2 class="h2 d-inline-block">{{ $ppmp->ppmp_name }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">

                <div class="card cm-profile">
                    <div class="card-body">
                        <h5 class="card-title">Details</h5>
                        <div class="form-row">
                            <div class="form-group col-3">
                                <label for="">PPMP Name</label>
                                <p>{{ $ppmp->ppmp_name }}</p>
                            </div>
                            <div class="form-group col-9">
                                <label for="">PPMP Description</label>
                                <p>{{ $ppmp->general_description }}</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-3">
                                <label for="">Date</label>
                                <p>{{ date('F d, Y', strtotime($ppmp->date)) }}</p>
                            </div>
                            <div class="form-group col-3">
                                <label for="">Mode of Procurement</label>
                                <p>{{ $ppmp->Procurement->name ?? ''}}</p>
                            </div>
                            <div class="form-group col-3">
                                <label for="">Category</label>
                                @php
                                    $category = \App\Categories::where('id', '=', $ppmp->category_id)->first();
                                @endphp
                                <p>{{ $category->name }}</p>
                            </div>
                            <div class="form-group col-3">
                                <label for="">Extent / Size / Description</label>
                                <p>{{ $ppmp->esd }}</p>
                            </div>
                            {{-- <div class="form-group col">
                                <label for="">Remarks</label>
                                @if($pap->status == 'Approved')
                                    <p>{{ $pap->remarks }}</p>
                                @else
                                    <textarea name="remarks" id="" cols="30" rows="4" class="form-control" placeholder=""></textarea>
                                @endif

                            </div> --}}
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:10px">
                    <div class="col">
                        <div class="card cm-profile">
                            <div class="card-body">
                                <label for="">POSTING OF IB/REI</label>
                                <p><i class="fa fa-calendar"></i> {{ date('F d, Y', strtotime($ppmp->advertisement)) }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card cm-profile">
                            <div class="card-body">
                                <label for="">SUBMISSION/OPENING OF BIDS</label>
                                <p><i class="fa fa-calendar"></i> {{ date('F d, Y', strtotime($ppmp->submission_of_bids)) }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card cm-profile">
                            <div class="card-body">
                                <label for="">NOTICE OF AWARD</label>
                                <p><i class="fa fa-calendar"></i> {{ date('F d, Y', strtotime($ppmp->notice_of_award)) }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card cm-profile">
                            <div class="card-body">
                                <label for="">CONTRACT SIGNING</label>
                                <p><i class="fa fa-calendar"></i> {{ date('F d, Y', strtotime($ppmp->contract_signing)) }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" style="margin-top:10px">
                    <div class="card-body">
                        <h5 class="card-title">Items</h5>
                        <table id="items" class="table " style="width:100%">
                            <thead>
                            <tr>
                                <th>Item</th>
                                <th>Account Code</th>
                                <th>PAP Info</th>
                                <th class="number">Quantity</th>
                                <th class="number">Unit Cost</th>
                                <th class="number">Total Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($ppmp_items as $item)
                                <tr>
                                    <td>{{ $item->PAPItem->item_name }}
                                    <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#pap_item_{{ $item->id }}">
                                            <i class="fa fa-info-circle"></i>
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="pap_item_{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document" style="max-width: 70%">
                                                <div class="modal-content" style="top: 15em;">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{ $item->PAPItem->item_name }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="height: 250px; overflow: auto;">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Category</th>
                                                                    <th>Subcategory</th>
                                                                    <th>Quantity</th>
                                                                    <th>Unit of Measure</th>
                                                                    <th>Unit Cost</th>
                                                                    <th>Total Cost</th>
                                                                    <th>Remarks/Specification</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>{{ $item->PAPItem->Category->name ?? '' }}</td>
                                                                    <td>{{ $item->PAPItem->Subcategory->name ?? '' }}</td>
                                                                    <td>{{ $item->PAPItem->quantity ?? '' }}</td>
                                                                    <td>{{ $item->PAPItem->UnitOfMeasure->name ?? '' }}</td>
                                                                    <td>{{ number_format($item->PAPItem->unit_cost, 2) ?? '' }}</td>
                                                                    <td>{{ number_format($item->PAPItem->total_cost, 2) ?? '' }}</td>
                                                                    @if(!empty($item->PAPItem->specifications))
                                                                        <td>{{ $item->PAPItem->specifications }}</td>
                                                                    @elseif(!empty($item->PAPItem->remarks))
                                                                        <td>{{ $item->PAPItem->remarks }}</td>
                                                                    @else
                                                                        <td>No Technical Specifications</td>
                                                                    @endif
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $item->PAPItem->PAP->code }}</td>
                                    <td>
                                        @if (in_array($item->PAPItem->PAP->status, ['Pending', 'Submitted']))
                                            <span class="badge badge-info">{{ $item->PAPItem->PAP->status }}</span>
                                        @endif

                                        @if ($item->PAPItem->PAP->status == 'For Revision')
                                            <span class="badge badge-warning">For Revision</span>
                                        @endif

                                        @if ($item->PAPItem->PAP->status == 'Draft')
                                            <span class="badge badge-dark">Draft</span>
                                        @endif

                                        @if ($item->PAPItem->PAP->status == 'Approved')
                                            <span class="badge badge-success">Approved</span>
                                        @endif

                                        @if($item->PAPItem->PaP->status == 'Rejected')
                                            <span class="badge badge-danger">Rejected</span>
                                        @endif

                                    </td>
                                    <td class="number">{{ $item->PAPItem->quantity }}</td>
                                    <td class="number">{{ number_format($item->PAPItem->unit_cost, 2) }}</td>
                                    <td class="number">{{ number_format($item->PAPItem->total_cost, 2) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="4"></th>
                                <th class="number"></th>
                                <th class="number" id="totalAmount">{{ number_format($total_cost, 2) }}</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {

        });
    </script>
@endsection()
