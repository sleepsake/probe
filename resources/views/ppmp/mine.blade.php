@extends('layouts.dashboard')

@section('main')

    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">PPMP</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col-6">

                        <h2 class="h2 d-inline-block">My PPMP List</h2>
                    </div>

                    <div class="col-6">
                        <a href="{{ url('/dashboard/ppmp/generate') }}" class="btn btn-sm btn-primary float-right">Generate PPMP</a>
                    </div>

                    <!-- <div class="col-auto">
                        <a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add new entry</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <br>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>PPMP Name</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>SubCategory</th>
                                <th>Mode Of Procurement</th>
                                <th>Estimated Budget</th>
                                <th>Schedule / Milestones</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($ppmps as $ppmp)
                                    @php
                                        $ppmp_items = $ppmp->Items;

                                        $budget = [];
                                        $pap_item_subcategories = [];

                                        foreach ($ppmp_items as $ppmp_item){
                                            $pap_item = \App\PAPItems::where('id', '=', $ppmp_item->pap_item_id)->first();
                                            array_push($budget, $pap_item->total_cost ?? 0);

                                            if(!in_array($pap_item->SubCategory->name ?? 0, $pap_item_subcategories)){
                                                array_push($pap_item_subcategories, $pap_item->SubCategory->name ?? 0);
                                            }
                                        }
                                        $estimated_budget = array_sum($budget);

                                        $category = \App\Categories::where('id', '=', $ppmp->category_id)->first();
                                    @endphp


                                    <tr>
                                        <td>{{ $ppmp->ppmp_name ?? '' }}</td>
                                        <td>{{ $ppmp->general_description ?? '' }}</td>
                                        <td>{{ $category->name ?? '' }}</td>
                                        <td>
                                            <ul>
                                                @foreach($pap_item_subcategories as $pap_item_subcategory)
                                                    <li>{{ $pap_item_subcategory }}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>{{ $ppmp->Procurement->name ?? '' }}</td>
                                        <td>{{ number_format($estimated_budget, 2) ?? 0.00 }}</td>
                                        <td>{{ $ppmp->MileStones() ?? '' }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('request.ppmp.profile', ['id' => $ppmp->id] ?? '') }}"
                                                   class="btn btn-sm btn-primary">View <i class="fa fa-caret-right"></i>
                                                </a>

                                                @if(in_array(Auth::user()->role, ['Super Admin', 'Integrator']))
                                                <!-- Button trigger modal -->
                                                <button type="button" class="btn btn-sm btn-light" data-toggle="modal" data-target="#ppmp_{{ $ppmp->id }}">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                                @endif

                                                <!-- Modal -->
                                                <div class="modal fade" id="ppmp_{{ $ppmp->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Delete PPMP</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form action="{{ route('request.ppmp.profile.delete', ['id' => $ppmp->id]) }}" method="POST">
                                                                @csrf
                                                                {{ method_field('DELETE') }}

                                                                <div class="modal-body">
                                                                    <p>Are you sure to delete this PPMP</p>
                                                                    <p>"{{ $ppmp->general_description }}"</p>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-danger btn-sm">Save changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable();

            $("#selectCategory").change(function() {
                $(this).closest('form').submit();
            })
        });
    </script>
@endsection()
