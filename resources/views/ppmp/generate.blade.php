<table>
    <thead>
        <tr>
            <th rowspan="2" style="text-align: center;">Code</th>
            <th rowspan="2" style="text-align: center;">GENERAL DESCRIPTION</th>
            <th rowspan="2" style="text-align: center;">MODE OF PROCUREMENT</th>
            <th rowspan="2" style="text-align: center;">EXTENT/SIZE/DESCRIPTION</th>
            <th rowspan="2" style="text-align: center;">ESTIMATED BUDGET</th>
            <th colspan="3" style="text-align: center;">EPA</th>
            <th colspan="12" style="text-align: center;">SCHEDULE/MILESTONE OF ACTIVITIES</th>
        </tr>
        <tr>
            <th style="text-align: center;">Oct</th>
            <th style="text-align: center;">Nov</th>
            <th style="text-align: center;">Dec</th>

            @for($x = 1; $x <= 12; $x++)
                <th style="text-align: center;">{{ date('M', strtotime(date("Y-{$x}-d"))) }}</th>
            @endfor
        </tr>
    </thead>

    <tbody>

    @if(Auth::user()->role == 'Super Admin')
        @foreach(\App\SubCategories::whereHas("Category", function($query) {
            $query->where('is_procurement', 'like', 'P%');
        })->orderBy('name')->get() as $subCategory)
            @if(count(\App\PPMP::where('sub_category_id', '=', $subCategory->id)->get()) > 0)
                @php
                    $category = \App\Categories::where('id', '=', $subCategory->category_id)->first();
                @endphp
                <tr>
                    <td>{{ $category->name }}</td>
                </tr>
                <tr>
                    <td>{{ $subCategory->name }}</td>
                </tr>
            @foreach(\App\PPMP::where('sub_category_id', $subCategory->id)->get() as $ppmp)
                {{--            {{ dd($ppmp->Items->pluck('pap_item_id')) }}--}}
                @php
                    $pap_items = \App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->pluck('pap_id');
                    $category = \App\Categories::where('id', '=', $ppmp->category_id)->first();
                @endphp

                <tr>
                    <td>{{ implode(', ', \App\PAP::whereIn('id', $pap_items)->groupBy('program_id', 'code')->pluck('code')->toArray()) }}</td>
                    <td>{{ $ppmp->general_description }}</td>
                    <td>{{ $category->is_procurement }}</td>
                    <td>{{ $ppmp->esd }}</td>
                    <td>{{ \App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->get()->sum('total_cost') }}</td>
                    <td>{{ $ppmp->getDate(10, $ppmp) }}</td>
                    <td>{{ $ppmp->getDate(11, $ppmp) }}</td>
                    <td>{{ $ppmp->getDate(12, $ppmp) }}</td>
                    @for($x = 1; $x <= 12; $x++)
                        <td>{{ $ppmp->getDate($x, $ppmp) }}</td>
                    @endfor
                </tr>
            @endforeach
            <tr></tr>
            @endif
        @endforeach
    @else
        @foreach(\App\SubCategories::whereHas("Category", function($query) {
            $query->where('is_procurement', 'like', 'P%');
        })->orderBy('name')->get() as $subCategory)
            @if(count(\App\PPMP::where('sub_category_id', '=', $subCategory->id)->get()) > 0)
                @php
                    $category = \App\Categories::where('id', '=', $subCategory->category_id)->first();
                @endphp
                <tr>
                    <td>{{ $category->name }}</td>
                </tr>
                <tr>
                    <td>{{ $subCategory->name }}</td>
                </tr>
                @foreach(\App\PPMP::where('sub_category_id', $subCategory->id)->get() as $ppmp)
                    {{--            {{ dd($ppmp->Items->pluck('pap_item_id')) }}--}}
                    @php
                        $pap_items = \App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->pluck('pap_id');
                        $category = \App\Categories::where('id', '=', $ppmp->category_id)->first();
                    @endphp

                    <tr>
                        <td>{{ implode(', ', \App\PAP::whereIn('id', $pap_items)->groupBy('program_id', 'code')->pluck('code')->toArray()) }}</td>
                        <td>{{ $ppmp->general_description }}</td>
                        <td>{{ $category->is_procurement }}</td>
                        <td>{{ $ppmp->esd }}</td>
                        <td>{{ \App\PAPItems::whereIn('id', $ppmp->Items->pluck('pap_item_id'))->get()->sum('total_cost') }}</td>
                        <td>{{ $ppmp->getDate(10, $ppmp) }}</td>
                        <td>{{ $ppmp->getDate(11, $ppmp) }}</td>
                        <td>{{ $ppmp->getDate(12, $ppmp) }}</td>
                        @for($x = 1; $x <= 12; $x++)
                            <td>{{ $ppmp->getDate($x, $ppmp) }}</td>
                        @endfor
                    </tr>
                @endforeach
                <tr></tr>
            @endif
        @endforeach
    @endif
    </tbody>
</table>
