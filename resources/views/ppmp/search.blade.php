@extends('layouts.dashboard')

@section('main')

    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">PPMP</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">Create PPMP</h2>
                    </div>

                    <!-- <div class="col-auto">
                        <a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add new entry</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="alert subtle">
                            <i class="fa fa-exclamation-circle"></i> You may filter the item results by selecting the categories below.
                        </div>

                        <!-- This should be the list of categories that the user is allowed to generate and those that have pending items -->
                        @php
                            $active = "active";
                        @endphp
                        <nav  class="nav nav-pills selector nav-justified cm-pills">
                            @foreach(\App\Categories::orderBy('name')->get() as $category)
                                @php
                                    $user_category = \App\UserCategory::where('user_id', '=', $current_user->id)->where('category_id', '=', $category->id)->exists();
                                @endphp

                                @if($user_category)

                                    <a href="{{ route('request.ppmp', ['category' => $category->id]) }}"
                                       class="nav-item nav-link {{ $category->id == $has_category ? 'active' : '' }}" style="margin-right:5px;margin-top:5px">
                                        {{ ucfirst(strtolower($category->name)) }}
                                        @if(in_array($current_user->role, ['Super Admin', 'Integrator', 'PPMP User']))
                                            @php
                                                $pap_itemss = \App\PAPItems::where('category_id', '=', $category->id)->whereYear('created_at', date('Y'))->get();
                                                $approved_count = [];
                                                $not_approved_count = [];
                                                foreach($pap_itemss as $pap_item){
                                                    $exists = \App\PPMPItems::where('pap_item_id', '=', $pap_item->id)->exists();
                                                    if(!$exists){
                                                        if($pap_item->PAP->is_deleted == 0 && $pap_item->PAP->status == 'Approved'){
                                                            array_push($approved_count, $pap_item->id);
                                                            array_push($not_approved_count, $pap_item->id);
                                                        } else {
                                                            array_push($not_approved_count, $pap_item->id);
                                                        }
                                                    }
                                                }
                                            @endphp
                                            <br />
                                            <span class="approved_count">{{ count($approved_count) }}</span>
                                            <span class="not_approved_count d-none">{{ count($not_approved_count) }}</span>
                                        @endif
                                    </a>
                                @endif

                            @endforeach
                        </nav>
                        <br>
                        <input type="checkbox" id="show_approved" value="Approved" checked> Show Approved Only

                        <hr>

                        {{-- Approved only --}}
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>PAP Code</th>
                                    <th>PAP Status</th>
                                    <th>Item Description</th>
                                    <th>Sub Category</th>
                                    <th>User</th>
                                    <th>MOOE</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($pap_items as $pap_item)
                                @php
                                    $check_if_exists = \App\PPMPItems::where('pap_item_id', '=', $pap_item->id)->exists();
                                @endphp

                                @if(in_array($current_user->role, ['Super Admin', 'Integrator']) || $pap_item->PAP->User->id == $current_user->id)

                                    @if($pap_item->PAP->is_deleted == 0)

                                        @if(!$check_if_exists && $pap_item->PAP->status == 'Approved')

                                            <tr class="{{ $pap_item->PAP->status == 'Approved' ? 'approved' : 'not_approved' }}">
                                                <td>
                                                    <input id="item_{{ $pap_item->id }}" type="checkbox" class="pap_item" value="{{ $pap_item->id }}"/>
                                                </td>

                                                <td>{{ $pap_item->PAP->code }}</td>

                                                <td>
                                                    @if($pap_item->PAP->status == 'Submitted')
                                                        <span class="badge badge-info">Pending</span>
                                                    @endif

                                                    @if($pap_item->PAP->status == 'For Revision')
                                                        <span class="badge badge-warning">For Revision</span>
                                                    @endif

                                                    @if($pap_item->PAP->status == 'Draft')
                                                        <span class="badge badge-dark">Draft</span>
                                                    @endif

                                                    @if($pap_item->PAP->status == 'Approved')
                                                        <span class="badge badge-success">Approved</span>
                                                    @endif

                                                    @if($pap_item->PAP->status == 'Rejected')
                                                        <span class="badge badge-danger">Rejected</span>
                                                    @endif
                                                </td>

                                                <td>{{ $pap_item->item_name ?? '' }}</td>

                                                <td>{{ $pap_item->SubCategory->name ?? '' }}</td>

                                                <td>{{ $pap_item->PAP->User->name }}</td>

                                                <td>{{ $pap_item->PAP->mooe_co }}</td>

                                                <td>{{ $pap_item->quantity }}</td>

                                                <td>{{ number_format($pap_item->total_cost, 2) }}</td>
                                            </tr>
                                        @endif
                                    @endif
                                @endif
                            @endforeach
                            </tbody>
                        </table>


                        {{-- Approved and Not Approved  --}}
                        <table id="not_all_approved" class="table table-striped table-bordered " style="width:100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>PAP Code</th>
                                <th>PAP Status</th>
                                <th>Item Description</th>
                                <th>Sub Category</th>
                                <th>User</th>
                                <th>MOOE</th>
                                <th>Quantity</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pap_items as $pap_item)
                                @php
                                    $check_if_exists = \App\PPMPItems::where('pap_item_id', '=', $pap_item->id)->exists();
                                @endphp

                                @if(in_array($current_user->role, ['Super Admin', 'Integrator']) || $pap_item->PAP->User->id == $current_user->id)

                                    @if($pap_item->PAP->is_deleted == 0)

                                        @if(!$check_if_exists)

                                            <tr>
                                                <td>
                                                    <input id="item_{{ $pap_item->id }}" type="checkbox" class="pap_item" value="{{ $pap_item->id }}"/>
                                                </td>

                                                <td>{{ $pap_item->PAP->code }}</td>

                                                <td>
                                                    @if($pap_item->PAP->status == 'Submitted')
                                                        <span class="badge badge-info">Pending</span>
                                                    @endif

                                                    @if($pap_item->PAP->status == 'For Revision')
                                                        <span class="badge badge-warning">For Revision</span>
                                                    @endif

                                                    @if($pap_item->PAP->status == 'Draft')
                                                        <span class="badge badge-dark">Draft</span>
                                                    @endif

                                                    @if($pap_item->PAP->status == 'Approved')
                                                        <span class="badge badge-success">Approved</span>
                                                    @endif

                                                    @if($pap_item->PAP->status == 'Rejected')
                                                        <span class="badge badge-danger">Rejected</span>
                                                    @endif
                                                </td>

                                                <td>{{ $pap_item->item_name ?? '' }}</td>

                                                <td>{{ $pap_item->SubCategory->name ?? '' }}</td>

                                                <td>{{ $pap_item->PAP->User->name }}</td>

                                                <td>{{ $pap_item->PAP->mooe_co }}</td>

                                                <td>{{ $pap_item->quantity }}</td>

                                                <td>{{ number_format($pap_item->total_cost, 2) }}</td>
                                            </tr>
                                        @endif
                                    @endif
                                @endif
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-12">
                <button class="btn btn-sm btn-primary float-right submit_to_create_ppmp" disabled>Create</button>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {

            $(".not_approved").addClass('d-none');

            $('#example').DataTable();
            $('#not_all_approved').DataTable();
            $('#not_all_approved_wrapper').addClass('d-none');

            $("#selectCategory").change(function() {
                $(this).closest('form').submit();
            });

            let pap_list = [];

            $(document).on('click', '.pap_item', function(){
               if($(this).is(':checked')){
                   pap_list.push($(this).val())
               } else {
                   pap_list.splice($.inArray($(this).val(), pap_list), 1)
               }

               if(pap_list.length >= 1){
                   $('.submit_to_create_ppmp').removeAttr('disabled');
               } else {
                   $('.submit_to_create_ppmp').attr('disabled', 'disabled');
               }
            });

            $('.submit_to_create_ppmp').click(function(){
                let to_route =  "/dashboard/requests/ppmp/{{ $has_category }}/create/" + pap_list.join('-');
                console.log(to_route);
                window.location = to_route;
            });



            $('#show_approved').click(function(){
               if($(this).is(':checked')){
                   $('#example_wrapper').removeClass('d-none');
                   $('#not_all_approved_wrapper').addClass('d-none');
                   $(".not_approved").addClass('d-none');
                   $(".not_approved_count").addClass('d-none');
                   $('.approved_count').removeClass('d-none');

                   let get_the_checkbox = $(".not_approved").attr('data-content');

                   if($(get_the_checkbox).is(':checked')){
                       $(get_the_checkbox).click();
                   }

               } else {
                   $('#example_wrapper').addClass('d-none');
                   $('#not_all_approved_wrapper').removeClass('d-none');

                   $(".not_approved").removeClass('d-none');
                   $(".not_approved_count").removeClass('d-none');
                   $('.approved_count').addClass('d-none');
                   let get_the_checkbox = $(".not_approved").attr('data-content');

                   if($(get_the_checkbox).is(':checked')){
                       $(get_the_checkbox).click();
                   }
               }
            });
        });
    </script>
@endsection()
