@extends('layouts.dashboard')

@section('main')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard/pap') }}">PPMP</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Update PPMP</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">Update PPMP</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('request.ppmp.profile.update.put', ['id' => $ppmp->id]) }}" method="POST">
                            @csrf
                            {{ method_field('PUT') }}
                            <div id="action-container" class="form-row" style="display: none">

                                <div id="action" class="form-group col-3">
                                    <label for="">Action</label>
                                    <select id="" class="form-control" name="action">
                                        <option value="New">Create new entry</option>
                                        <option value="Consolidate">Consolidate to existing</option>
                                    </select>
                                </div>

                                <div id="ppmp-entries" class="form-group col-3" style="display: none">
                                    <label for="">PPMP</label>
                                    <select id="" class="form-control" name="ppmp_id">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">

                                <div class="form-group col-12">
                                    <label for="">PPMP Name</label>
                                    <input type="text" class="form-control" id="" name="ppmp_name" placeholder="" value="{{ $ppmp->ppmp_name }}" required>
                                </div>

                                <div class="form-group col-12">
                                    <label for="">General Description</label>
                                    <textarea name="ppmp_description" id="" cols="" rows="3" class="form-control" required>{{ $ppmp->general_description }}</textarea>
                                </div>
                                <div class="form-group col-3">
                                    <label for="">Extent/Size/Description</label>
                                    <input type="text" class="form-control" id="" name="ppmp_esd" placeholder="" required value="{{ $ppmp->esd }}">
                                </div>
                                <div class="form-group col-3">
                                    <label for="">Mode of Procurement</label>
                                    <select id="" class="form-control" name="mode_of_procurement_id">
                                        @foreach($mode_of_procurement as $modeOfProcurement)
                                            <option value="{{ $modeOfProcurement->id }}" {{ $ppmp->mode_of_procurement_id == $modeOfProcurement->id ? 'selected' : '' }}>
                                                {{ $modeOfProcurement->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <label for="">Date</label>
                                    <input type="text" class="form-control form-date" id="" name="ppmp_date" value="{{ date('Y-m-d', strtotime($ppmp->date)) }}" required>
                                </div>
                                <div class="form-group col-3">

                                    <label for="">Sub Category</label>
                                    <ul>
                                        @php
                                            $categories = [];
                                            foreach ($ppmp->Items as $items){
                                                $i = \App\PAPItems::find($items->pap_item_id);
                                                if(!in_array($i->SubCategory->name, $categories)){
                                                    array_push($categories, $i->SubCategory->name);
                                                }
                                            }
                                        @endphp
                                        @foreach($categories as $subs)
                                            <input type="hidden" name="ppmp_sub_category_id" value="{{ $subs }}" />
                                            <li>{{ $subs }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <hr>

                            <table id="example" class="table " style="width:100%">
                                <thead>
                                <tr>
                                    <th>Item / Requirements</th>
                                    <th>Account Code</th>
                                    <th>End-User</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ppmp_items as $item)

                                    <input type="hidden" value="{{ $item->PAPItem->id }}" name="ppmp_items[]" />
                                    <tr>
                                        <td>{{ $item->PAPItem->item_name }}</td>
                                        <td>{{ $item->PAPItem->SubCategory->name ?? '' }}</td>
                                        <td>{{ $ppmp->User->role }}</td>
                                        <td>{{ $item->PAPItem->quantity }}</td>
                                        <td>{{ number_format($item->PAPItem->unit_cost, 2) }}</td>
                                        <td>{{ number_format($item->PAPItem->total_cost, 2) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="4"></th>
                                    <th>Total Amount</th>
                                    <th>{{ number_format($total_cost, 2) }}</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>

                            <br>

                            <h4>Schedule / Milestones of Activities</h4>
                            <br>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="">Advertisement/Posting of IB/REI</label>
                                    <input type="text" class="form-control form-date" id="" name="advertisement" value="{{ date('Y-m-d', strtotime($ppmp->advertisement)) }}" required>
                                </div>
                                <div class="form-group col">
                                    <label for="">Submission/Opening of Bids</label>
                                    <input type="text" class="form-control form-date" id="" name="submission_of_bids" value="{{ date('Y-m-d', strtotime($ppmp->submission_of_bids)) }}" required>
                                </div>
                                <div class="form-group col">
                                    <label for="">Notice of Award</label>
                                    <input type="text" class="form-control form-date" id="" name="notice_of_award" value="{{ date('Y-m-d', strtotime($ppmp->notice_of_award)) }}" required>
                                </div>
                                <div class="form-group col">
                                    <label for="">Contract Signing</label>
                                    <input type="text" class="form-control form-date" id="" name="contract_signing" value="{{ date('Y-m-d', strtotime($ppmp->contract_signing)) }}" required>
                                </div>
                            </div>

                            <br>
                            <br>

                            <div class="text-right">
                                <a href="{{ route('request.ppmp') }}" class="btn btn-default">Cancel</a>
                                <button id="" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#submitPAP').click(function() {
                Swal.fire({
                    type: 'success',
                    title: 'Success!',
                    html: `Program / Activity / Project Added <br>
                            <strong>PAP Code: 300100200400</strong>`
                })
            });

            const items = JSON.parse(localStorage.getItem('selectedPAPItems'));
            console.log(items);

            let subCategories = [];

            let subCategoryIds = [];

            let totalAmount = 0;
            $.each(items, function(i, data) {
                const unitCost = parseFloat(data.unit_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                const totalCost = parseFloat(data.total_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                totalAmount += parseFloat(data.total_cost);

                $("#example tbody").append(`
                <tr>
                    <td>${data.item_name}</td>
                    <td>${data.sub_category}</td>
                    <td>${data.end_user}</td>
                    <td>${data.quantity}</td>
                    <td>${unitCost}</td>
                    <td>${totalCost}</td>
                    <input type="hidden" name="items[${i}]" value="${data.id}">
                </tr>
                `);

                subCategories[data.sub_category_id] = data.sub_category;
                subCategoryIds[i] = data.sub_category_id;
            });

            $.each(Object.assign({}, subCategories), function(id, subCategory) {
                $("#subCategories").append(`<option value="${id}">${subCategory}</option>`);
            });

            $("#totalAmount").text("P " + totalAmount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

            console.log(subCategories);
            $.post('{{ url('/dashboard/ppmp/getAction') }}', {'_token' : '{{ @csrf_token() }}', 'subCategories' : subCategoryIds}, function(response) {
                console.log(response);
                if(response.length > 0) {
                    $("#action-container").show();
                    $.each(response, function(i, data) {

                        let json = JSON.stringify(data);
                        $("#ppmp-entries select").append(`<option value="${data.id}" data-json='${json}'>${data.general_description}</option>`);
                    });

                } else {
                    $("#action-container").remove();
                }
            });

            $("#action").change(function() {
                if($(this).find('option:selected').val() === 'Consolidate') {
                    $("#ppmp-entries").slideDown();
                } else {
                    $("#ppmp-entries").slideUp();
                }
            });

            $(document).on('change', '#ppmp-entries > select', function() {
                me = $(this).find('option:selected');
                if(me.val() !== "") {
                    let json = JSON.parse(me.attr('data-json'));
                    console.log(json);
                    $("textarea[name='general_description']").prop('disabled', true).val(`${json.general_description}`);
                    $("input[name='esd']").prop('disabled', true).val(`${json.esd}`);
                    $("select[name='mode_of_procurement']").prop('disabled', true);
                    $("select[name='sub_category_id']").prop('disabled', true);

                    $("select[name='mode_of_procurement'] > option").each(function(i, el) {
                        if($(this).val() === json.mode_of_procurement) {
                            $("select[name='mode_of_procurement'] > option").removeAttr('selected');
                            $(this).prop('selected', true);
                        }
                    });

                    $("select[name='sub_category_id'] > option").each(function(i, el) {
                        if($(this).val() === json.sub_category_id) {
                            $("select[name='sub_category_id'] > option").removeAttr('selected');
                            $(this).prop('selected', true);
                        }
                    });

                    flatpickr('input[name=date]').destroy();
                    // console.log(json.sub_category_id);



                    $(".form-date").prop('disabled', true);

                    $('input[name="date"]').attr('value', json.date);
                    $('input[name="date"]').flatpickr({
                        enableTime: false,
                        altInput: true,
                        altFormat: "F d, Y",
                        dateFormat: "Y-m-d",
                        defaultDate: json.date
                    });

                    $('input[name="advertisement"]').attr('value', json.date);
                    $('input[name="advertisement"]').flatpickr({
                        enableTime: false,
                        altInput: true,
                        altFormat: "F d, Y",
                        dateFormat: "Y-m-d",
                        defaultDate: json.advertisement
                    });

                    $('input[name="submission_of_bids"]').attr('value', json.date);
                    $('input[name="submission_of_bids"]').flatpickr({
                        enableTime: false,
                        altInput: true,
                        altFormat: "F d, Y",
                        dateFormat: "Y-m-d",
                        defaultDate: json.submission_of_bids
                    });

                    $('input[name="notice_of_award"]').attr('value', json.date);
                    $('input[name="notice_of_award"]').flatpickr({
                        enableTime: false,
                        altInput: true,
                        altFormat: "F d, Y",
                        dateFormat: "Y-m-d",
                        defaultDate: json.notice_of_award
                    });

                    $('input[name="contract_signing"]').attr('value', json.date);
                    $('input[name="contract_signing"]').flatpickr({
                        enableTime: false,
                        altInput: true,
                        altFormat: "F d, Y",
                        dateFormat: "Y-m-d",
                        defaultDate: json.contract_signing
                    });

                    $("#example > tbody > tr").remove();


                    let totalAmount = 0;

                    $.each(json.ppmp_items, function(i, data) {
                        const unitCost = parseFloat(data.unit_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        const totalCost = parseFloat(data.total_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        totalAmount += parseFloat(data.total_cost);

                        $("#example tbody").append(`
                        <tr>
                            <td>${data.item_name}</td>
                            <td>${data.sub_category}</td>
                            <td>${data.end_user}</td>
                            <td>${data.quantity}</td>
                            <td>${unitCost}</td>
                            <td>${totalCost}</td>
                        </tr>
                        `);
                    });

                    const items = JSON.parse(localStorage.getItem('selectedPAPItems'));

                    $.each(items, function(i, data) {
                        const unitCost = parseFloat(data.unit_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        const totalCost = parseFloat(data.total_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        totalAmount += parseFloat(data.total_cost);

                        $("#example tbody").append(`
                        <tr class="bg-warning">
                            <td>${data.item_name}</td>
                            <td>${data.sub_category}</td>
                            <td>${data.end_user}</td>
                            <td>${data.quantity}</td>
                            <td>${unitCost}</td>
                            <td>${totalCost}</td>
                            <input type="hidden" name="items[${i}]" value="${data.id}">
                        </tr>
                        `);
                    });

                    $("#totalAmount").text("P " + totalAmount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

                } else {
                    location.reload();
                }
            });

        });
    </script>
@endsection()
