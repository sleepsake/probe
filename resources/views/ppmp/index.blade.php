@extends('layouts.dashboard')

@section('main')

    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">PPMP</li>
                    </ol>
                </nav>

            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">{{ date('Y') }} - PROGRAM ACTIVITY PROJECTS (PAPs)</h2>
                    </div>

                    <!-- <div class="col-auto">
                        <a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add new entry</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">PAP</a>
                            </li>
                            <li class="nav-item">
                                @if(\App\PPMP::count() == 0)
                                <a class="nav-link disabled" href="#">PPMP</a>
                                @else
                                    <a class="nav-link" href="{{ url('/dashboard/ppmp/items') }}">PPMP</a>
                                @endif
                            </li>
                        </ul>

                        <hr>

                        <div>
                            <form action="">
                                <div class="form-row">
                                    <div class="form-group col-4">
                                        {{--<label>PAP Type</label>--}}
                                        {{--<select id="selectPAPType" class="form-control" name="type">--}}
                                            {{--<option value="All" {{ Request::get('type') == 'All' ? 'selected' : '' }}>All</option>--}}
                                            {{--<option value="Program" {{ Request::get('type') == 'Program' ? 'selected' : '' }}>Program</option>--}}
                                            {{--<option value="Activity" {{ Request::get('type') == 'Activity' ? 'selected' : '' }}>Activity</option>--}}
                                            {{--<option value="Project" {{ Request::get('type') == 'Project' ? 'selected' : '' }}>Project</option>--}}
                                        {{--</select>--}}
                                        <label>Category</label>
                                        <select id="selectCategory" class="form-control" name="category">
                                            @foreach(\App\Categories::orderBy('name')->get() as $category)
                                                <option value="{{ $category->id }}" {{ request('category') == $category->id ? 'selected' : '' }}>{{ $category->name }} ({{ count(DB::select("
                                                    SELECT
                                                    p.code,
                                                    p.mooe_co,
                                                    p.user_id,
                                                    pi.*
                                                    FROM pap_items pi
                                                    LEFT JOIN pap p ON p.id = pi.pap_id
                                                    WHERE pi.for_procurement = 'P' AND p.status IN ('Submitted', 'Approved') AND pi.category_id = '{$category->id}'
                                                    AND pi.id NOT IN (SELECT pap_item_id as id FROM ppmp_items)
                                                ")) }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-3">
                                        <label class="d-block">&nbsp;</label>
                                        <button type="button" id="goToPPMPForm" class="btn btn-primary d-inline-block mt-1">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>PAP Code</th>
                                <th>Item Description</th>
                                <th>Category</th>
                                <th>User</th>
                                <th>MOOE</th>
                                <th>Quantity</th>
                                <th>Amount</th>
                                <th style="display: none;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ppmps as $pap)
                                {{--{{ dd($pap) }}--}}
                                <?php
                                $pap->category = \App\Categories::find($pap->category_id) ? \App\Categories::find($pap->category_id)->name : '';
                                $pap->sub_category = \App\SubCategories::find($pap->sub_category_id) ? \App\SubCategories::find($pap->sub_category_id)->name : '';
                                $pap->end_user = \App\User::find($pap->user_id)->department;
                                ?>
                                <tr data-json="{{ collect($pap) }}">
                                    <td></td>
                                    <td>
                                        {{ $pap->code }}
                                        {{--<a href="{{ route('ppmp_update', $pap->pap_id) }}">{{ $pap->code }}</a>--}}
                                    </td>
                                    <td>{{ $pap->item_name }}</td>
                                    <td>
                                        {{ \App\SubCategories::find($pap->sub_category_id) ? \App\SubCategories::find($pap->sub_category_id)->name : '' }}
                                        {{--<br>--}}
                                        {{--<small>{{ \App\Categories::find($pap->category_id) ? \App\Categories::find($pap->category_id)->name : '' }}</small>--}}
                                    </td>
                                    <td>{{ \App\User::find($pap->user_id)->department }}</td>
                                    <td>{{ $pap->mooe_co }}</td>
                                    <td>{{ $pap->quantity }}</td>
                                    <td>P {{ number_format($pap->total_cost ?? 0, 2) }}</td>
                                    <td style="display: none;">{{ collect($pap) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            // $('#example').DataTable();
            var table = $('#example').DataTable( {
                columnDefs: [ {
                    orderable: false,
                    className: 'select-checkbox',
                    targets:   0
                } ],
                select: {
                    style:    'multi',
                    selector: 'td:first-child'
                },
                order: [[ 1, 'asc' ]]
            } );

            $(document).on('click', '#example tbody td:first-child', function() {
                // alert('a');
                // console.log(table.rows({selected: true}).data().toArray());
                // console.log(table.rows({selected: true}));
                // table.rows({selected: true}).each(function() {
                //     console.log($(this).closest('tr').data('json'));
                // });
            })

            $(document).on('click', '#goToPPMPForm', function() {
                const data = table.rows({selected: true}).data().toArray();

                let selectedItems = {};
                if(data.length > 0) {

                    $.each(Object.assign({}, data), function(i, d) {
                        selectedItems[i] = JSON.parse(d[8]);
                    });

                    localStorage.setItem("selectedPAPItems", JSON.stringify(selectedItems));

                    location.href = "{{ url('/dashboard/ppmp/create') }}";
                }
            });

            // $("#selectPAPType").change(function() {
            //     $(this).closest('form').submit();
            // })

            $("#selectCategory").change(function() {
                $(this).closest('form').submit();
            })
        });
    </script>
@endsection()
