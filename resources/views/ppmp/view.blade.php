@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard/ppmp') }}">PPMP</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Update Program Activity Project</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">UPDATE PROGRAM ACTIVITY PROJECTS (APB)</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST">
                            @csrf

                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="">Code</label>
                                    <p>{{ $pap->code }}</p>
                                </div>
                                <div class="form-group col-6">
                                    <label for="">Project Description</label>
                                    <p>{{ $pap->project_description }}</p>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="">Date</label>
                                    <p>{{ date('F j, Y', strtotime($pap->date)) }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">PAP Type</label>
                                    <p>{{ $pap->type }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">Program</label>
                                    <p>{{ $pap->program }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">Cluster</label>
                                    <p>{{ $pap->cluster }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">Source of Funds</label>
                                    <p>{{ $pap->source_of_funds }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">MOOE/CO</label>
                                    <p>{{ $pap->mooe_co }}</p>
                                </div>
                                <div class="form-group col">
                                    <label for="">Remarks</label>
                                    <p>{{ $pap->remarks }}</p>
                                </div>
                            </div>

                            <br>

                            <table id="items" class="table " style="width:100%">
                                <thead>
                                <tr>
                                    <th>Item / Requirements</th>
                                    <th>Account Title</th>
                                    <th>Quantity</th>
                                    <th>Unit Cost</th>
                                    <th>Total Amount</th>
                                    <th>Procurement (P) or <br>Non Procurement (NP)</th>
                                    <th>Remarks</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $x = 0; ?>
                                @foreach($pap->Items as $item)
                                    <tr data-json="{{ $item }}">
                                        <td>{{ $item->item_name }}</td>
                                        <td>{{ $item->category }}</td>
                                        <td>{{ $item->quantity }}</td>
                                        <td>{{ $item->unit_cost }}</td>
                                        <td>{{ $item->total_cost }}</td>
                                        <td>{{ $item->for_procurement }}</td>
                                        <td>
                                            <p>{{ $item->remarks }}</p>
                                        </td>
                                    </tr>
                                    <?php $x++; ?>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="3"></th>
                                    <th>Total Amount</th>
                                    <th id="totalAmount">{{ number_format($pap->Items->sum('total_cost'), 2) }}</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>

                            <br>

                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="">Advertisement/Posting of IB/REI</label>
                                    <input type="text" class="form-control form-date" id="" name="advertisement" value="{{ $pap->PPMP ? $pap->PPMP->advertisement : date('Y-m-d') }}" required>
                                </div>
                                <div class="form-group col">
                                    <label for="">Submission/Opening of Bids</label>
                                    <input type="text" class="form-control form-date" id="" name="submission_of_bids" value="{{ $pap->PPMP ? $pap->PPMP->submission_of_bids : date('Y-m-d') }}" required>
                                </div>
                                <div class="form-group col">
                                    <label for="">Notice of Award</label>
                                    <input type="text" class="form-control form-date" id="" name="notice_of_award" value="{{ $pap->PPMP ? $pap->PPMP->notice_of_award : date('Y-m-d') }}" required>
                                </div>
                                <div class="form-group col">
                                    <label for="">Contract Signing</label>
                                    <input type="text" class="form-control form-date" id="" name="contract_signing" value="{{ $pap->PPMP ? $pap->PPMP->contract_signing : date('Y-m-d') }}" required>
                                </div>
                            </div>

                            <br>
                            <br>

                            <div class="text-right">
                                <?php /*
                                @if($pap->status == 'Approved')
                                    <a href="{{ url('/dashboard/apb') }}" class="btn btn-primary">Back to list</a>
                                @else

                                @endif
                                */ ?>
                                <a href="{{ url('/dashboard/ppmp') }}" class="btn btn-default">Cancel</a>
                                <button class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {

        });
    </script>
@endsection()
