@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">APB</li>
                    </ol>
                </nav>

                {{--{{ url('/PVAO-GAA-2019.pdf') }}--}}
                <a href="{{ url('/apb-generate') }}" class="btn btn-sm btn-primary mt-5 float-right" target="_blank">Generate APB</a>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">{{ date('Y') }} - ANNUAL PLAN and BUDGET (APB)</h2>
                    </div>

                    <!-- <div class="col-auto">
                        <a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add new entry</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/dashboard/apb') }}">PAP ({{ \App\PAP::whereIn('status', ['For Revision', 'Submitted'])->count() }})</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/dashboard/apb') }}?status=Approved">APB ({{ \App\PAP::where('status', 'Approved')->count() }})</a>
                            </li>
                        </ul>

                        <hr>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Project</br>Description</th>
                                <th>PAP</br>Type</th>
                                <th>Quantity</th>
                                <th>Program</th>
                                <th>Cluster</th>
                                <th>Estimated</br>Budget</th>
                                <th>Source</br>of Funds</th>
                                <th>Status</th>
                                <th>Timestamp</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($apbs as $pap)
                                <tr>
                                    <td>
                                        @if($pap->status == 'Submitted')
                                            <a href="{{ route('apb_update', $pap->id) }}">{{ $pap->code }}</a>
                                        @else
                                            {{ $pap->code }}
                                        @endif
                                    </td>
                                    <td>{{ $pap->project_description }}</td>
                                    <td>{{ $pap->type }}</td>
                                    <td>{{ $pap->items->sum('quantity') }}</td>
                                    <td>{{ $pap->program }}</td>
                                    <td>{{ $pap->clusterCode() }}</td>
                                    <td>P {{ number_format($pap->items->sum('total_cost') ?? 0, 2) }}</td>
                                    <td>{{ $pap->source_of_funds }}</td>
                                    <td class="">{!! $pap->statusFormat() !!}</td>
                                    <td data-order="{{ date('Ymd', strtotime($pap->created_at)) }}">{{ date('F j, Y', strtotime($pap->created_at)) }}</td>
                                    <td>{{ $pap->remarks }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                "order": [[ 9, "desc" ]]
            } );
        });
    </script>
@endsection()