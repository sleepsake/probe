<table>
    <thead>
    <tr>
        <th rowspan="3" style="text-align: center; vertical-align: middle;">Particulars</th>
        <th colspan="12" style="text-align: center;">GENERAL ADMINISTRATION AND SUPPORT</th>
        <th colspan="38" style="text-align: center;">OPERATIONS</th>
    </tr>
    <tr>
        <th colspan="12" style="text-align: center;">GAS</th>
        <th colspan="22" style="text-align: center;">MFO 1: Administration of Veterans' Pension and Benefits Program</th>
        <th colspan="10" style="text-align: center;">MFO 2: Preservation and Development Services for Military Shrines</th>
        <th colspan="6" style="text-align: center;">MFO 3: Policy Formulation for the promotion of veterans' welfare</th>
    </tr>
    <tr>
        <th colspan="12" style="text-align: center;">GMS</th>
        <th colspan="11" style="text-align: center;">MFO 1.1</th>
        <th colspan="2" style="text-align: center;">MFO 1.2</th>
        <th colspan="9" style="text-align: center;">MFO 1.1</th>
        <th colspan="5" style="text-align: center;">MFO 2.1</th>
        <th colspan="5" style="text-align: center;">MFO 2.2</th>
        <th colspan="6" style="text-align: center;">MFO 3.1</th>
    </tr>


    <tr>
        <th style="text-align: center;">Object of Expenditures</th>

        <!-- General Management and Supervision -->
        <th style="text-align: center;">OA</th>
        <th style="text-align: center;">OA PAO</th>
        <th style="text-align: center;">Admin - GSS</th>
        <th style="text-align: center;">Admin - PMS</th>
        <th style="text-align: center;">Admin</th>
        <th style="text-align: center;">MID</th>
        <th style="text-align: center;">LAD</th>
        <th style="text-align: center;">PMD</th>
        <th style="text-align: center;">FD</th>
        <th style="text-align: center;">VMHD</th>
        <th style="text-align: center;">OA-OVA</th>
        <th style="text-align: center;">TOT</th>

        <!-- Processing of veterans' claims -->
        <th style="text-align: center;">FD</th>
        <th style="text-align: center;">CD</th>
        <th style="text-align: center;">VRMD</th>
        <th style="text-align: center;">LAD</th>
        <th style="text-align: center;">MID</th>
        <th style="text-align: center;">VAMD</th>
        <th style="text-align: center;">Admin</th>
        <th style="text-align: center;">Admin - GSS</th>
        <th style="text-align: center;">OA PAO</th>
        <th style="text-align: center;">PMD</th>
        <th style="text-align: center;">TOT1</th>

        <!--  For educational benefits, expanded hospitalization program and burial benefits -->
        <th style="text-align: center;">CD</th>
        <th style="text-align: center;">TOT2</th>

        <!-- For the investigation, verification of records, strengthening of internal control system and conduct of management and systems audit -->
        <th style="text-align: center;">VRMD</th>
        <th style="text-align: center;">LAD</th>
        <th style="text-align: center;">PMD</th>
        <th style="text-align: center;">OA PAO</th>
        <th style="text-align: center;">MID</th>
        <th style="text-align: center;">CD</th>
        <th style="text-align: center;">Admin</th>
        <th style="text-align: center;">Admin - GSS</th>
        <th style="text-align: center;">TOT3</th>

        <!-- Administration and management of national military shrines -->
        <th style="text-align: center;">VMHD</th>
        <th style="text-align: center;">MID</th>
        <th style="text-align: center;">Admin</th>
        <th style="text-align: center;">Admin - GSS</th>
        <th style="text-align: center;">TOT</th>

        <!-- Celebration of veteran-related events -->
        <th style="text-align: center;">VMHD</th>
        <th style="text-align: center;">Admin - GSS</th>
        <th style="text-align: center;">Admin</th>
        <th style="text-align: center;">OA-PAO</th>
        <th style="text-align: center;">TOT2</th>

        <!-- Policy Formulation for the promotion of veterans' welfare -->
        <th style="text-align: center;">VAMD</th>
        <th style="text-align: center;">MID</th>
        <th style="text-align: center;">OA PAO</th>
        <th style="text-align: center;">Admin</th>
        <th style="text-align: center;">Admin - GSS</th>
        <th style="text-align: center;">TOT</th>
    </tr>

    </thead>
    <tbody>

    <?php
    $clusters = [
        'GAS' => [
            'OA',
            'OA PAO',
            'Admin - GSS',
            'Admin - PMS',
            'Admin',
            'MID',
            'LAD',
            'PMD',
            'FD',
            'VMHD',
            'OA-OVA',
        ],
        "Processing of veterans' claims" => [
            'FD',
            'CD',
            'VRMD',
            'LAD',
            'MID',
            'VAMD',
            'Admin',
            'Admin - GSS',
            'OA PAO',
            'PMD'
        ],
        'For educational benefits, expanded hospitalization program and burial benefits' => [
            'CD',
        ],
        'For the investigation, verification of records, strengthening of internal control system and conduct of management and systems audit' => [
            'VRMD',
            'LAD',
            'PMD',
            'OA PAO',
            'MID',
            'CD',
            'Admin',
            'Admin - GSS',
        ],
        'Administration and management of national military shrines' => [
            'VMHD',
            'MID',
            'Admin',
            'Admin - GSS',
        ],
        'Celebration of veteran-related events' => [
            'VMHD',
            'Admin - GSS',
            'Admin',
            'OA-PAO',
        ],
        "Policy Formulation for the promotion of veterans' welfare" => [
            'VAMD',
            'MID',
            'OA PAO',
            'Admin',
            'Admin - GSS',
        ]
    ];
    ?>

    @foreach(\App\Categories::orderBy('name')->get() as $category)
        <tr>
            <th style="text-align: left; font-weight: bold;">{{ $category->name }}</th>

            @for($x = 0; $x < 42; $x++)
                <td></td>
            @endfor
        </tr>

        @foreach($category->SubCategories->sortBy('name') as $subCategory)
            <tr>
                <td style="padding-left: 20px;">{{ $subCategory->name }}</td>
                @foreach($clusters as $cluster => $c)
                    <?php
                    $total = 0;
                    $data = [];
                    $result = \Illuminate\Support\Facades\DB::select("SELECT
                                    u.department,
                                    SUM(`total_cost`) as total_cost
                                FROM users u
                                LEFT JOIN pap p ON p.user_id = u.id
                                INNER JOIN pap_items pi ON pi.`pap_id` = p.`id`
                                WHERE
                                    p.cluster = ?
                                    AND  pi.`sub_category_id` = {$subCategory->id}
                                    AND p.`status` IN ('Submitted', 'Approved')
                                GROUP BY u.department", [$cluster]);
                    foreach($result as $r) {
                        $data[$r->department] = $r;
                    }
                    ?>
                    @foreach($c as $department)
                        @php $total += $data[$department]->total_cost ?? 0; @endphp
                        <td>{{ !empty($data[$department]) ? $data[$department]->total_cost : '' }}</td>
                    @endforeach
                    <td>{{ $total == 0 ? '' : $total }}</td>
                @endforeach
            </tr>
        @endforeach

        <tr>
            @for($x = 0; $x < 43; $x++)
                <td></td>
            @endfor
        </tr>
    @endforeach
    </tbody>
</table>