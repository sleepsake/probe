@extends('layouts.dashboard')

@section('main')
    <div class="container cm-container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">PAP</li>
                    </ol>
                </nav>

                {{--<a href="{{ url('/dashboard/pap/create') }}" class="btn btn-sm btn-primary mt-5 float-right">Create <i class="fa fa-plus"></i></a>--}}
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">List of PAP For Review</h2>
                    </div>

                    <!-- <div class="col-auto">
                        <a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add new entry</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="container cm-container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">

                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($status == "All")? "active":"";?>" href="{{ route('request.review') }}">All Status</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($status == "Submitted")? "active":"";?>" href="{{ route('request.review') }}?status=Submitted">Submitted</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($status == "For Revision")? "active":"";?>" href="{{ route('request.review') }}?status=For Revision">For Revision</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($status == "Approved")? "active":"";?>" href="{{ route('request.review') }}?status=Approved">Approved</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($status == "Rejected")? "active":"";?>" href="{{ route('request.review') }}?status=Rejected">Rejected</a>
                            </li>
                        </ul><br>
                        <hr>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:90px">Code</th>
                                    <th>Project Description</th>
                                    <th style="width:75px">PAP Type</th>
                                    <th style="width:75px">Program</th>
                                    <th style="width:75px">Cluster</th>
                                    <th style="width:115px">Budget Estimate</th>
                                    {{-- <th style="width:65px">Function</th> --}}
                                    <th>Created By</th>
                                    <th style="width:115px">Timestamp</th>
                                    <th style="width:90px">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($paps as $pap)
                                <tr>
                                    <td>
                                        @if(in_array($pap->status, ['Draft', 'For Revision']))
                                            <a href="{{ route('pap_update', $pap->id) }}">{{ $pap->code }}</a>
                                        @else
                                            {{ $pap->code }}
                                        @endif
                                    </td>
                                    <td>{{ $pap->project_description }}</td>
                                    <td>{{ optional($pap->type)->name }}</td>
                                    <td>{{ optional($pap->cluster)->name }}</td>
                                    <td>
                                        <p>{{optional((new \App\ClusterSubcategory)->get_by_id($pap->cluster_id))->name}}</p>
                                    </td>
                                    <td>P {{ number_format($pap->items->sum('total_cost') ?? 0, 2) }}</td>
                                    {{-- <td>
                                        @php $rand = rand(0,2) @endphp

                                        @if ($rand == 0)
                                            <span class="badge badge-warning">Pending</span>
                                        @elseif ($rand == 1)
                                            <span class="badge badge-success">Approved</span>
                                        @else
                                            <span class="badge badge-danger">Rejected</span>
                                        @endif
                                     </td> --}}
                                    <td>{{$pap->User->name}}</td>
                                    <td data-order="{{ date('Ymd', strtotime($pap->created_at)) }}">{{ date('F j, Y', strtotime($pap->created_at)) }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{route('request.review.profile', $pap->id)}}" class="btn btn-sm btn-outline-info">Review <i class="fa fa-caret-right"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                "order": [[ 8, "desc" ]]
            } );
        });
    </script>
@endsection()
