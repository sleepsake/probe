@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard/pap') }}">PAP</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Program Activity Project</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">CREATE PROGRAM ACTIVITY PROJECTS (PAPs)</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="">Code</label>
                                    <input type="text" class="form-control" id="" placeholder="">
                                </div>
                                <div class="form-group col-6">
                                    <label for="">Project Description</label>
                                    <input type="text" class="form-control" id="" placeholder="">
                                </div>
                                <div class="form-group col">
                                    <label for="">Date</label>
                                    <input type="text" class="form-control" id="" placeholder="">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-3">
                                    <label for="">Program</label>
                                    <select id="" class="form-control">
                                        <option>Choose..</option>
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <label for="">Cluster</label>
                                    <select id="" class="form-control">
                                        <option>Choose..</option>
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <label for="">Source of Funds</label>
                                    <select id="" class="form-control">
                                        <option>Choose..</option>
                                    </select>
                                </div>
                            </div>

                            <hr>

                            <button type="button" class="btn btn-sm btn-primary mb-5" data-toggle="modal" data-target="#addItemModal"><i class="fa fa-plus"></i> Add item</button>


                            <table id="example" class="table " style="width:100%">
                                <thead>
                                <tr>
                                    <th>Item / Requirements</th>
                                    <th>Account Title</th>
                                    <th>Quantity</th>
                                    <th>Unit Cost</th>
                                    <th>Total Amount</th>
                                    <th>P / NP</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Meals</td>
                                        <td>REPRESENTATION TAXES</td>
                                        <td>50</td>
                                        <td>P 1,500.00</td>
                                        <td>P 75,000.00</td>
                                        <td>P</td>
                                    </tr>
                                    <tr>
                                        <td>Tarpaulin</td>
                                        <td>OTHER SUPPLIES AND MATERIAL EXPENSES</td>
                                        <td>1</td>
                                        <td>P 3,000.00</td>
                                        <td>P 3,000.00</td>
                                        <td>P</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3"></th>
                                        <th>Total Amount</th>
                                        <th>P 78,000,000.00</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>

                            <div class="text-right">
                                <a href="{{ url('/dashboard/pap') }}" class="btn btn-default">Cancel</a>
                                <button type="button" id="submitPAP" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addItemModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 80%; width: 700px;">
            <div class="modal-content">
                <div class="form">
                    <div class="modal-body">
                        <div class="inline-header mb-4">
                            <h5 class="h5 font-weight-semibold">ADD ITEM / REQUIREMENT</h5>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="">Item Name</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-4">
                                <label for="">Quantity</label>
                                <input type="text" class="form-control">
                            </div>

                            <div class="form-group col-4">
                                <label for="">Unit Cost</label>
                                <input type="text" class="form-control">
                            </div>

                            <div class="form-group col-4">
                                <label for="">Total Cost</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-8">
                                <label for="">Item Name</label>
                                <select name="" id="" class="form-control">
                                    <option value="">Representation Expenses</option>
                                </select>
                            </div>

                            <div class="form-group col-4">
                                <label for=""></label>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="forProcurement" checked>
                                    <label for="aozora" class="custom-control-label mt-3" for="forProcurement"><strong>For Procurement</strong></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="">Technical Specifications / Requirements</label>
                                <textarea name="" id="" cols="" rows="3" class="form-control">Supply and delivery of meals and catering services for various meetings and conferences and in-house trainings and seminars for CY 2020
                            </textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Add Item</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#submitPAP').click(function() {
                Swal.fire({
                    type: 'success',
                    title: 'Success!',
                    html: `Program / Activity / Project Added <br>
                            <strong>PAP Code: 300100200400</strong>`
                })
            });
        });
    </script>
@endsection()