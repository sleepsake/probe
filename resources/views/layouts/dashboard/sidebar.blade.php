<aside class="cell-item cell-sidebar overflow-auto">
    <div id="sidebar">
        <nav>

            @if(Auth::user()->role != 'User Admin')
            <div id="sidebar-nav-articles" class="page-group page-first">
				<button class="btn btn-link w-100 btn-sidebar-collapse" data-toggle="collapse" data-target="#sidebar-article-submenu" aria-expanded="false">
					<div class="d-flex flex-row align-items-center">
						<div class="d-flex flex-row align-items-center flex-grow-1"> <!-- Add this div if there's an icon for the link -->
							<i class="fad fa-list fa-fw mr-2"></i>
							<span>Project Activity Plan</span>
						</div>

						<i class="fas fa-chevron-down fa-sm icon-sidebar-dropdown"></i>
					</div>
				</button>

				<div id="sidebar-article-submenu" class="collapse page-group page-second page-submenu show">
					<ul class="list-unstyled sidebar-submenu">
						<li class="nav-item"><a href="{{ url('/dashboard/pap') }}" class="nav-link"><span>All</span></a></li>
						<li class="nav-item"><a href="{{ url('/dashboard/pap') }}" class="nav-link"><span>My Drafts</span></a></li>
						<li class="nav-item"><a href="{{ url('/dashboard/pap') }}" class="nav-link"><span>Pending</span></a></li>
						<li class="nav-item"><a href="{{ url('/dashboard/pap') }}" class="nav-link"><span>Accepted</span></a></li>
						<li class="nav-item"><a href="{{ url('/dashboard/pap') }}" class="nav-link"><span>Rejected</span></a></li>
					</ul>
				</div>
			</div>
            @endif

            @if(in_array(Auth::user()->role, ['Budget Officer', 'Super Admin']))
                <div id="" class="page-group page-first">
                    <a href="{{ url('/dashboard/apb') }}" class="btn btn-link w-100">
                        <div class="d-flex flex-row align-items-center">
                            <div class="d-flex flex-row align-items-center">
                                <span>APB</span>
                            </div>
                        </div>
                    </a>
                </div>
            @endif

            @if(in_array(Auth::user()->role, ['Integrator', 'Super Admin']))
            <div id="sidebar-nav-home" class="page-group page-first">
                <a href="{{ url('/dashboard/ppmp') }}" class="btn btn-link w-100">
                    <div class="d-flex flex-row align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <span>PPMP</span>
                        </div>
                    </div>
                </a>
            </div>
            @endif

            {{--            @if(in_array(Auth::user()->role, ['Budget Officer', 'Super Admin']))--}}
            <div id="" class="page-group page-first">
                <a href="{{ url('/dashboard/app') }}" class="btn btn-link w-100">
                    <div class="d-flex flex-row align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <span>APP</span>
                        </div>
                    </div>
                </a>
            </div>
            {{--@endif--}}




            @if(in_array(Auth::user()->role, ['Budget Office', 'Super Admin']))
            <div id="sidebar-nav-home" class="page-group page-first">
                <a href="{{ url('/dashboard/cse') }}" class="btn btn-link w-100">
                    <div class="d-flex flex-row align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <span>CSE</span>
                        </div>
                    </div>
                </a>
            </div>
            @endif

            {{--<div id="sidebar-nav-two" class="page-group page-first">--}}
                {{--<a href="#" class="btn btn-link w-100">--}}
                    {{--<div class="d-flex flex-row align-items-center">--}}
                        {{--<div class="d-flex flex-row align-items-center">--}}
                            {{--<i class="fad fa-link fa-fw mr-2"></i>--}}
                            {{--<span>About Us</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}

            {{--<div id="sidebar-nav-three" class="page-group page-first">--}}
                {{--<button class="btn btn-link w-100 btn-sidebar-collapse" data-toggle="collapse" data-target="#sidebar-nav-three-submenu" aria-expanded="false">--}}
                    {{--<div class="d-flex flex-row align-items-center">--}}
                        {{--<div class="d-flex flex-row align-items-center flex-grow-1"> <!-- Add this div if there's an icon for the link -->--}}
                            {{--<i class="fad fa-link fa-fw mr-2"></i>--}}
                            {{--<span>Menu three</span>--}}
                        {{--</div>--}}

                        {{--<i class="fas fa-chevron-down fa-sm icon-sidebar-dropdown"></i>--}}
                    {{--</div>--}}
                {{--</button>--}}

                {{--<div id="sidebar-nav-three-submenu" class="collapse page-group page-second page-submenu">--}}
                    {{--<ul class="list-unstyled sidebar-submenu">--}}
                        {{--<li class="nav-item"><a href="#" class="nav-link"><span>Menu 3-1</span></a></li>--}}
                        {{--<li class="nav-item">--}}
                            {{--<button class="btn btn-link border-0 w-100 btn-sidebar-collapse" data-toggle="collapse" data-target="#sidebar-nav-three-submenu-two">--}}
                                {{--<div class="d-flex flex-row align-items-center">--}}
                                    {{--<span class="flex-grow-1">Menu 3-2 with a very long title</span>--}}
                                    {{--<i class="fas fa-chevron-down fa-sm icon-sidebar-dropdown"></i>--}}
                                {{--</div>--}}
                            {{--</button>--}}

                            {{--<div id="sidebar-nav-three-submenu-two" class="collapse page-group page-third page-submenu">--}}
                                {{--<ul class="list-unstyled sidebar-submenu">--}}
                                    {{--<li class="nav-item"><a href="#" class="nav-link"><span>Menu 3-2-1</span></a></li>--}}
                                    {{--<li class="nav-item"><a href="#" class="nav-link"><span>Menu 3-2-2</span></a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item"><a href="#" class="nav-link"><span>Menu 3-3</span></a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        </nav>
    </div>
</aside>
