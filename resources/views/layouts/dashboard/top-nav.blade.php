<nav class="navbar navbar-expand-md bg-white position-fixed w-100 shadow-sm cm-nav">
    <a href="#" class="navbar-brand">
        {{--<img src="assets/CM-Logo.png" alt="" class="img-fluid img-navbar-logo">--}}
        {{--Managment of Assets, Accounting, Planning and Procurement System (MAPS)--}}
        PROBE <span style="font-size:12px">Procurement Management System</span>
    </a>

    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="#"><i class="far fa-home"></i> Home</a>
            </li>

            @if(Auth::user()->role != 'User Admin')
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="far fa-folder-plus"></i> Project Requests
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a href="{{ url('/dashboard/pap/create') }}" class="dropdown-item"><span><i class="far fa-plus"></i> Create PAP</span></a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('pap') }}" class="dropdown-item"><span>All Status</span></a>
                    <a href="{{ route('pap.status', 'Submitted') }}" class="dropdown-item"><span>Pending</span></a>
                    <a href="{{ route('pap.status', 'For Revision') }}" class="dropdown-item"><span>Revision</span></a>
                    <a href="{{ route('pap.status', 'Approved') }}" class="dropdown-item"><span>Accepted</span></a>
                    <a href="{{ route('pap.status', 'Rejected') }}" class="dropdown-item"><span>Rejected</span></a>
                    <div class="dropdown-divider"></div>
                    @if(Auth::user()->role == 'Super Admin')
                    <a href="{{ route('request.pap.vault') }}" class="dropdown-item"><span><i class="far fa-cabinet-filing"></i> PAP Vault</span></a>
                    @endif
                </div>
            </li>
            @endif

            @if(in_array(Auth::user()->role, ['Budget Officer', 'Super Admin']))
            <li class="nav-item">
                <a class="nav-link" href="{{ route('request.review') }}"><i class="far fa-file-signature"></i>
                     Budget Review
                     <span class="badge badge-danger">{{ \App\PAP::where('user_id', Auth::user()->id)->whereNotIn('status', ['Approved', 'Rejected'])->count() }}</span>
                </a>
            </li>
            @endif

            @if(in_array(Auth::user()->role, ['Integrator', 'Super Admin']))
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="far fa-file-invoice"></i>  Procurement Management
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a href="{{ url('/dashboard/requests/ppmp/1') }}" class="dropdown-item"><span><i class="far fa-plus"></i> Create PPMP</span></a>
                    <div class="dropdown-divider"></div>
                    <a href="{{route('request.ppmp.mine')}}" class="dropdown-item"><span><span>My PPMP List</span></a>
                    <div class="dropdown-divider"></div>
                    @if(Auth::user()->role == 'Super Admin')
                    <a href="{{ route('request.ppmp.vault') }}" class="dropdown-item"><span><i class="far fa-cabinet-filing"></i> PPMP Vault</span></a>
                    @endif
                </div>
            </li>
            @endif

            @if(in_array(Auth::user()->role, ['BAC', 'Super Admin']))
            <li class="nav-item">
                <a class="nav-link" href="{{ route('app.list') }}"><i class="far fa-calendar"></i>
                     Annual Procurement Plan
                </a>
            </li>
            @endif

            <!-- TODO: Admin Only -->
            @if(in_array(Auth::user()->role, ['User Admin', 'Super Admin']))
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="far fa-cogs"></i> Settings
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    @if(in_array(Auth::user()->role, ['Super Admin']))
                        <a href="{{route('dropdowns')}}" class="dropdown-item"><span>Dropdown Management</span></a>
                        <a href="{{route('banner.form')}}" class="dropdown-item"><span>Banner Management</span></a>
                    @endif
                    <a href="{{ url("/dashboard/user-management") }}" class="dropdown-item"><span>User Management</span></a>
                </div>
            </li>
            @endif

        </ul>
    </div>

    <ul class="navbar-nav">

        <li class="nav-item dropdown" style="min-width: 180px;">
            <a href="#" class="nav-link dropdown-toggle text-right" id="navAccount" role="button" data-toggle="dropdown" aria-haspopup="true"
               aria-expanded="false"><span><i class="fad fa-user fa-fw mr-2"></i>{{ Auth::user()->name }}</span>
           </a>
            <div class="dropdown-menu">
                {{--<a href="#" class="dropdown-item"><i class="fad fa-cog fa-fw fa-swap-opacity mr-2"></i>Settings</a>--}}
                @if(in_array(Auth::user()->role, ['User Admin', 'Super Admin']))
                    <a href="{{ url("/dashboard/user-management") }}" class="dropdown-item"><i class="fad fa-users fa-fw fa-swap-opacity mr-2"></i>User Management</a>
                    <div class="dropdown-divider"></div>
                @endif

                <a href="{{ url('/logout') }}" class="dropdown-item"><i class="fad fa-sign-out-alt fa-fw mr-2"></i>Logout</a>
            </div>

        </li>

        {{--<li class="nav-item dropdown">--}}
            {{--<a href="#" class="nav-link dropdown-toggle" id="navSettings" role="button" data-toggle="dropdown" aria-haspopup="true"--}}
               {{--aria-expanded="false"><span><i class="fad fa-cog fa-fw fa-swap-opacity mr-2"></i>Settings</span></a>--}}

            {{--<div class="dropdown-menu has-icons">--}}
                {{--<a href="#" class="dropdown-item">--}}
					{{--<span class="d-flex align-items-center w-100">--}}
						{{--<div class="flex-grow-1">Visit website</div><i class="fad fa-external-link-alt fa-fw theme-inherit-color"></i>--}}
					{{--</span>--}}
                {{--</a>--}}
                {{--<a href="#" class="dropdown-item">Dashboard settings</a>--}}
                {{--<div class="dropdown-divider"></div>--}}
                {{--<a href="#" class="dropdown-item">About Layercake</a>--}}
            {{--</div>--}}
        {{--</li>--}}

        {{--<li class="nav-item">--}}
            {{--<a href="#" class="nav-link"><span><i class="fad fa-sign-out-alt fa-fw mr-2"></i>Log out</span></a>--}}
        {{--</li>--}}
    </ul>
</nav>
