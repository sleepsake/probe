<!DOCTYPE html>
<html lang="en">
<head>
    <title>PROBE</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/fc-3.2.5/fh-3.1.4/r-2.2.2/sl-1.3.0/datatables.min.css"/>
    <link rel="stylesheet" href="{{ asset('/plugins/cm-dashboard/css/cm-bs.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,400i,600,600i,700,700i,800,800i&display=swap&subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/qin2oxh.css">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"       crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.2/flatpickr.min.css">
    <link rel="stylesheet" href="{{ asset('/plugins/cm-dashboard/css/cm-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/cm-dashboard/css/cm-base.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/cm-override.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <?php /*
    <!-- Missing Scripts -->
    <link rel="stylesheet" href="{{ asset('/plugins/cm-dashboard/css/cm-tables.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/cm-dashboard/css/cm-nav.css') }}">
    */ ?>

    <style>
        #top-nav-banner {
            background: #302f80;
            padding: 10px 16px;
            color: #FFF;
        }

        .form-control[readonly] {
            background-color: #ced4da !important;
            opacity: 1;
        }
    </style>
</head>
<body>

@include('layouts.dashboard.top-nav')

<section class="body-content d-flex flex-column">
    <div class="main-body flex-grow-1">
        <section id="top-nav-banner" class="container-fluid" style="margin-top: 53px;">
            <div class="row" style="margin-top:15px;margin-bottom:15px">
                <div class="col">Veterans Affairs Management Division (VAMD)</div>
                {{--<div class="col text-center"><small>OIC, VAMD</small> ASEC RAUL Z. CABALLES</div>--}}
                <div class="col text-right">As of {{ date('F d - H:iA') }}</div>
            </div>
        </section>

        <section class="grid-container ">
            <main id="homeContent" class="bg-light pt-5 pb-7 overflow-auto" style="min-height: 100vh">
                @yield('main')
            </main>
        </section>
    </div>
</section>

@include('layouts.dashboard.scripts')

@if(session('pop-success'))
    <script>
        Swal.fire({
            title: 'Success!',
            text: '{{ session('pop-success') }}',
            type: 'success',
            confirmButtonText: 'Confirm'
        })
    </script>
@endif

@if(session('pop-error'))
    <script>
        Swal.fire({
            title: 'Error!',
            text: '{{ session('pop-error') }}',
            type: 'error',
            confirmButtonText: 'Confirm'
        })
    </script>
@endif
</body>
</html>
