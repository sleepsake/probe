@extends('layouts.dashboard')

@section('main')
    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-md-10 offset-md-1">
                <div class="card">
                    <div class="card-body">
                        <h2 class="h2 d-inline-block">Banner Details</h2>

                        <form method="POST" action="{{ route('banner.form.update') }}">
                            @csrf

                            <div class="form-group">
                                <label for="">System Short Name</label>
                                <input type="text" class="form-control" id="" name="system_short_name" value="{{ $data->system_short_name }}" required>
                            </div>
                            <div class="form-group">
                                <label for="">System Long Name</label>
                                <input type="text" class="form-control" id="" name="system_long_name" value="{{ $data->system_long_name }}" required>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="">Organization Name</label>--}}
                                {{--<input type="email" class="form-control" id="" name="email" value="{{ old('email') }}" required>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="">OIC</label>--}}
                                {{--<input type="email" class="form-control" id="" name="email" value="{{ old('email') }}" required>--}}
                            {{--</div>--}}
                            <div class="text-center">
                                {{-- <a href="{{ url('/dashboard/user-management') }}" class="btn btn-default">Cancel</a> --}}
                                <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script>
        $(document).ready(function() {

        });
    </script>
@endsection()
