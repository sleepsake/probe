<button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#delete_unit_{{ $measurement->id }}">
    <i class="fa fa-trash"></i>
</button>

<div class="modal fade" id="delete_unit_{{ $measurement->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Unit of Measurement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route("dropdowns.delete.units") }}" method="POST">
                @csrf
                {{ method_field('DELETE') }}
                <div class="modal-body">
                    <input type="hidden" name="measurement_id" value="{{ $measurement->id }}" />
                    <div class="form-group">
                        <label for="program_type">Measurement Type</label>
                        <input type="text" class="form-control" id="program_type"
                               name="program_type" placeholder="Project" value="{{ $measurement->name }}" readonly/>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
