@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dropdown</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">Dropdown Management</h2><hr>

                        <div class="alert subtle">
                            <i class="fa fa-exclamation-circle"></i> Select a section to manage.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-4">
                <a href="{{route('dropdowns.programTypes')}}" class="card-selection">
                    <div class="card">
                        <div class="card-body">
                            <i class="fa fa-list"></i><br>
                            PAP Program Types
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-4">
                <a href="{{route('dropdowns.programClusters')}}" class="card-selection">
                    <div class="card">
                        <div class="card-body">
                            <i class="fa fa-list"></i><br>
                            Program and Clusters
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-4">
                <a href="{{route('dropdowns.categories')}}" class="card-selection">
                    <div class="card">
                        <div class="card-body">
                            <i class="fa fa-list"></i><br>
                            Item Categories
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-4">
                <a href="{{route('dropdowns.units')}}" class="card-selection">
                    <div class="card">
                        <div class="card-body">
                            <i class="fa fa-list"></i><br>
                            Units of Measure
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-4">
                <a href="{{route('dropdowns.mode_of_procurement')}}" class="card-selection">
                    <div class="card">
                        <div class="card-body">
                            <i class="fa fa-list"></i><br>
                            Mode of Procurement
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-4">
                <a href="{{route('dropdowns.divisions')}}" class="card-selection">
                    <div class="card">
                        <div class="card-body">
                            <i class="fa fa-list"></i><br>
                            Divisions
                        </div>
                    </div>
                </a>
            </div>

        </div>
    </div>

@endsection()
