@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dropdown</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">Item Category Management</h2><hr>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-3">
                @php $activeSidebar = "item-categories"; @endphp
                @include('management.dropdown.sidebar')
            </div>

            {{--            @php--}}
            {{--                $categories = ['Traveling Expense', 'Training and Scholarship', 'Utility', 'Extraordinary & Misc', 'Prfessional Service'];--}}
            {{--            @endphp--}}

            <div class="col-5 border-right">
                <button type="button" class="btn-sm btn btn-outline-info float-right" data-toggle="modal" data-target="#add_item_category">
                    Add Item Category
                </button>

                <div class="modal fade" id="add_item_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Item Category</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{ route("dropdowns.save.categories") }}" method="POST">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="item_category_name">Item Category Name</label>
                                        <input type="text" class="form-control" id="item_category_name" name="item_category_name"
                                               placeholder="Traveling Expense"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="item_category_name">Procurement</label>
                                        <select id="" class="form-control" name="is_procurement" required>
                                            <option value="P">Procurable</option>
                                            <option value="NP">Non-Procurable</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-outline-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <h5>Category List</h5>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <th>P / N</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($categories as $key => $category)
                        @if ($category_id == $category->id)
                            @php $active = "table-active"; @endphp
                        @else
                            @php $active = ""; @endphp
                        @endif

                        <tr class="{{ $active }}">
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->is_procurement }}</td>
                            <td>
                                <div class="btn-group">
                                    @include('management.dropdown.itemcategories._includes._update')
                                    @include('management.dropdown.itemcategories._includes._delete')
                                </div>
                            </td>
                            <td>
                                <a href="{{ route('dropdowns.item.subcategory', ['category_id' => $category->id ]) }}">
                                    <i class="fa fa-caret-right"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col">
                <button type="button" class="btn-sm btn btn-outline-info float-right" data-toggle="modal" data-target="#add_item_subcategory">
                    Add Item SubCategory
                </button>

                <div class="modal fade" id="add_item_subcategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Item Subcategory</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <form action="{{ route("dropdowns.save.item.subcategory", ['category_id' => $c->id]) }}" method="POST">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="item_subcategory_name">Item SubCategory Name</label>
                                        <input type="text" class="form-control" id="item_subcategory_name" name="item_subcategory_name"
                                               placeholder=""/>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-outline-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <h5>{{ $c->name }}</h5>
                <table class="table">
                <thead>
                    <tr>
                        <th>Category Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($c->SubCategories as $key => $subcategory)
                        <tr>
                            <td>{{ $subcategory->name }}</td>
                            <td>
                                <div class="btn-group">
                                    @include('management.dropdown.itemcategories._includes._subcategory_update')
                                    @include('management.dropdown.itemcategories._includes._subcategory_delete')
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection()
