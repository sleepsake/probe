<button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#delete_subcategory_{{ $subcategory->id }}">
    <i class="fa fa-trash"></i>
</button>

<div class="modal fade" id="delete_subcategory_{{ $subcategory->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Item SubCategory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route("dropdowns.delete.item.subcategory", ['category_id' => $category_id]) }}" method="POST">
                @csrf
                {{ method_field('DELETE') }}
                <div class="modal-body">
                    <input type="hidden" name="item_subcategory_id" value="{{ $category->id }}" />
                    <div class="form-group">
                        <label for="item_subcategory_name">Cluster Name</label>
                        <input type="text" class="form-control" id="item_subcategory_name"
                               name="item_subcategory_name" placeholder="Project" value="{{ $subcategory->name }}" readonly/>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
