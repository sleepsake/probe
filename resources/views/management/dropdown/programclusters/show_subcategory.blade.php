@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dropdown</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">Program Cluster Management</h2><hr>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-3">
                @php $activeSidebar = "program-clusters"; @endphp
                @include('management.dropdown.sidebar')
            </div>

            <div class="col">
                <button type="button" class="btn-sm btn btn-outline-info float-right" data-toggle="modal" data-target="#add_cluster">
                    Add Cluster
                </button>

                <div class="modal fade" id="add_cluster" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Program Cluster</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{ route("dropdowns.save.programClusters") }}" method="POST">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="program_cluster_name">Program Cluster</label>
                                        <input type="text" class="form-control" id="program_cluster_name" name="program_cluster_name"
                                               placeholder="GAS"/>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-outline-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <h5>Program Cluster List</h5>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($clusters as $key => $cluster)
                        @if ($cluster->id == $cluster_id)
                            @php $active = "table-active"; @endphp
                        @else
                            @php $active = ""; @endphp
                        @endif

                        <tr class="{{ $active }}">
                            <td>{{ $cluster->name }}</td>
                            <td>
                                <div class="btn-group">
                                    @include('management.dropdown.programclusters._includes._update')
                                    @include('management.dropdown.programclusters._includes._delete')
                                </div>
                            </td>
                            <td>
                                <a href="{{ route('dropdowns.programClusters.subcategory', ['cluster_id' => $cluster->id]) }}">
                                    <i class="fa fa-caret-right"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col">
                <button type="button" class="btn-sm btn btn-outline-info float-right" data-toggle="modal" data-target="#add_subcategory">
                    Add SubCategory
                </button>

                <div class="modal fade" id="add_subcategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Subcategory</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{ route("dropdowns.save.programClusters.subcategory", ['cluster_id' => $cluster_id]) }}" method="POST">
                                @csrf
                                <div class="modal-body">
                                    <input type="hidden" name="cluster_id" value="{{ $cluster_id }}" />
                                    <div class="form-group">
                                        <label for="subcategory_name">Subcategory</label>
                                        <input type="text" class="form-control" id="subcategory_name" name="subcategory_name"
                                               placeholder="GAS"/>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-outline-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <h5>{{ $current_cluster->name }}</h5>
                <table class="table">
                <thead>
                    <tr>
                        <th>Category Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($current_cluster->SubCategories as $key => $subcategory)
                        <tr>
                            <td>{{ $subcategory->name }}</td>
                            <td>
                                <div class="btn-group">
                                    @include('management.dropdown.programclusters._includes._subcategory_update')
                                    @include('management.dropdown.programclusters._includes._subcategory_delete')
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection()
