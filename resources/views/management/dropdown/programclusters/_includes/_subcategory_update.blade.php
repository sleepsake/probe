<button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#update_subcategory_{{ $subcategory->id }}">
    <i class="fa fa-edit"></i>
</button>

<div class="modal fade" id="update_subcategory_{{ $subcategory->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Subcategory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route("dropdowns.update.programClusters.subcategory", ['cluster_id' => $cluster->id]) }}" method="POST">
                @csrf
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <input type="hidden" name="subcategory_id" value="{{ $cluster->id }}" />
                    <div class="form-group">
                        <label for="subcategory_name">Subcategory Name</label>
                        <input type="text" class="form-control" id="subcategory_name" name="subcategory_name"
                               placeholder="Boxes, Pieces" value="{{ $subcategory->name }}"/>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-outline-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
