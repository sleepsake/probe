<button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#update_program_type_{{ $program->id }}">
    <i class="fa fa-edit"></i>
</button>

<div class="modal fade" id="update_program_type_{{ $program->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Program Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route("dropdowns.update.programTypes") }}" method="POST">
                @csrf
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <input type="hidden" name="program_id" value="{{ $program->id }}" />
                    <div class="form-group">
                        <label for="program_type">Program Type</label>
                        <input type="text" class="form-control" id="program_type"
                               name="program_type" placeholder="Project" value="{{ $program->name }}"/>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-outline-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
