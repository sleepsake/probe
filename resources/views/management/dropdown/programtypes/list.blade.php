@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dropdown</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">Program Types Management</h2><hr>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-3">
                @php $activeSidebar = "program-types"; @endphp
                @include('management.dropdown.sidebar')
            </div>

            <div class="col">
                <button type="button" class="btn-sm btn btn-outline-info float-right" data-toggle="modal" data-target="#add_program_type">
                    Add Program Type
                </button>

                <div class="modal fade" id="add_program_type" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Program Type</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{ route("dropdowns.save.programTypes") }}" method="POST">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="program_type">Program Type</label>
                                        <input type="text" class="form-control" id="program_type" name="program_type" placeholder="Project"/>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-outline-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


                <h5>Program Type List</h5>
                <table class="table">
                <thead>
                    <tr>
                        <th>Category Name</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($program_types as $key => $program)
                        <tr class="">
                            <td>{{ $program->name }}</td>
                            <td>
                                <div class="btn-group">
                                    @include('management.dropdown.programtypes._includes._update')

                                    @include('management.dropdown.programtypes._includes._delete')
                                </div>
                            </td>
                            <td> <i class="fa fa-caret-right"></i> </td>
                        </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection()
