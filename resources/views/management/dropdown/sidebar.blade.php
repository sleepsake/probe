<nav class="nav flex-column nav-pills">
  <a class="nav-link <?php echo ($activeSidebar == "program-types")? "active":"";?>" href="{{route('dropdowns.programTypes')}}">Program Types</a>
  <a class="nav-link <?php echo ($activeSidebar == "program-clusters")? "active":"";?>" href="{{route('dropdowns.programClusters')}}">Program Clusters</a>
  <a class="nav-link <?php echo ($activeSidebar == "item-categories")? "active":"";?>" href="{{route('dropdowns.categories')}}">Item Categories</a>
  <a class="nav-link <?php echo ($activeSidebar == "units")? "active":"";?>" href="{{route('dropdowns.units')}}">Units of Measures</a>
  <a class="nav-link <?php echo ($activeSidebar == "mode-of-procurement")? "active":"";?>" href="{{route('dropdowns.mode_of_procurement')}}">Mode of Procurement</a>
  <a class="nav-link <?php echo ($activeSidebar == "divisions")? "active":"";?>" href="{{route('dropdowns.divisions')}}">Divisions</a>
</nav>
