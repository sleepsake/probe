<button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#update_division_{{ $division->id }}">
    <i class="fa fa-edit"></i>
</button>

<div class="modal fade" id="update_division_{{ $division->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Mode of Procurement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route("dropdowns.update.divisions") }}" method="POST">
                @csrf
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <input type="hidden" name="division_id" value="{{ $division->id }}" />
                    <div class="form-group">
                        <label for="measurement_type">Division Name</label>
                        <input type="text" class="form-control" id="division_name" name="division_name"
                               placeholder="Boxes, Pieces" value="{{ $division->name }}"/>
                    </div>

                    <div class="form-group">
                        <label for="division_description">Division Description</label>
                        <textarea class="form-control" id="division_description" name="division_description" rows="3">{{ $division->description }}</textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-outline-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
