<button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#delete_mode_{{ $procurement->id }}">
    <i class="fa fa-trash"></i>
</button>

<div class="modal fade" id="delete_mode_{{ $procurement->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Mode of Procurement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route("dropdowns.delete.mode_of_procurement") }}" method="POST">
                @csrf
                {{ method_field('DELETE') }}
                <div class="modal-body">
                    <input type="hidden" name="procurement_id" value="{{ $procurement->id }}" />
                    <div class="form-group">
                        <label for="program_type">Measurement Type</label>
                        <input type="text" class="form-control" id="procurement_name"
                               name="procurement_name" placeholder="Project" value="{{ $procurement->name }}" readonly/>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
