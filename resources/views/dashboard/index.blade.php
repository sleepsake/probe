@extends('layouts.dashboard')

@section('main')
    {{--<div class="container">--}}
        {{----}}
        {{--<div class="row">--}}
            {{--<div class="col-12">--}}
                {{--<nav aria-label="breadcrumb">--}}
                    {{--<ol class="breadcrumb">--}}
                        {{--<li class="breadcrumb-item"><a href="#">Home</a></li>--}}
                        {{--<li class="breadcrumb-item"><a href="#">Library</a></li>--}}
                        {{--<li class="breadcrumb-item active" aria-current="page">Data</li>--}}
                    {{--</ol>--}}
                {{--</nav>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="row mb-2">--}}
            {{--<div class="col-12">--}}
                {{--<div class="row">--}}
                    {{--<div class="col">--}}
                        {{--<h2 class="h2 d-inline-block">Press <span data-toggle="tooltip" data-placement="right" title="Edit page title"><a href="#"--}}
                                                                                                                                          {{--class="btn btn-link py-0" data-toggle="modal" data-target="#editPageTitleModal"><i--}}
                                            {{--class="fad fa-edit theme-cm"></i></a></span></h2>--}}
                    {{--</div>--}}

                    {{--<!-- <div class="col-auto">--}}
                        {{--<a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add new entry</a>--}}
                    {{--</div> -->--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="container">--}}
        {{--<div class="row card-sections">--}}

            {{--<!-- Add another column for new cards -->--}}
            {{--<div class="col-12">--}}
                {{--<div class="card">--}}
                    {{--<div class="card-body">--}}
                        {{--<h3 class="h3 font-weight-medium">Add new entry</h3>--}}

                        {{--<form class="needs-validation" novalidate>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="" class="required">Title</label>--}}
                                {{--<input type="text" class="form-control" placeholder="Love Live! Sunshine!!" required>--}}

                                {{--<!-- Validation messages -->--}}
                                {{--<div class="valid-feedback">Looks good!</div>--}}
                                {{--<div class="invalid-feedback">This field is required.</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group form-multiple-row-input">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col">--}}
                                        {{--<label for="">Cast</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-auto">--}}
                                        {{--<button class="btn btn-outline-primary btn-sm py-0 btn-add-field">--}}
                                            {{--<i class="fas fa-plus mr-1"></i>Add new field--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<input type="text" class="form-control" placeholder="Takami Chika (CV. Inami Anju)" required>--}}

                                {{--<!-- Validation messages -->--}}
                                {{--<div class="valid-feedback">Looks good!</div>--}}
                                {{--<div class="invalid-feedback">This field is required.</div>--}}

                                {{--<input type="text" class="form-control" placeholder="Watanabe You (CV. Saitou Shuka)"--}}
                                       {{--value="Watanabe You (CV. Saitou Shuka) [read-only text field]" readonly>--}}

                                {{--<input type="text" class="form-control" placeholder="Kurosawa Ruby (CV. Furihata Ai)"--}}
                                       {{--value="Kurosawa Ruby (CV. Furihata Ai) [disabled text field]" disabled>--}}
                            {{--</div>--}}

                            {{--<div class="form-row">--}}
                                {{--<div class="form-group col-6">--}}
                                    {{--<label for="">Date</label>--}}
                                    {{--<input type="text" class="form-control form-date" placeholder="Enter a date..." readonly>--}}
                                {{--</div>--}}

                                {{--<div class="form-group col-6">--}}
                                    {{--<label for="">Time</label>--}}
                                    {{--<input type="text" class="form-control form-time" placeholder="Enter a time..." readonly>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label for="">Select a series</label>--}}
                                {{--<select name="" id="" class="form-control">--}}
                                    {{--<option value="">-- Select one --</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}

                            {{--<div class="form-row">--}}
                                {{--<div class="form-group col-4">--}}
                                    {{--<label for="">Songs (List format)<span class="required"></span><br /><span class="small">Select all that may--}}
											{{--apply</span></label>--}}
                                    {{--<div class="custom-control custom-checkbox">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="aozora">--}}
                                        {{--<label for="aozora" class="custom-control-label">青空Jumping Heart</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="custom-control custom-checkbox">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="miraiboku">--}}
                                        {{--<label for="miraiboku" class="custom-control-label">未来の僕らは知ってるよ</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="custom-control custom-checkbox">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="yumekataru">--}}
                                        {{--<label for="yumekataru" class="custom-control-label">ユメ語るよりユメ歌おう</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group col-8">--}}
                                    {{--<label for="" class="custom-label-inline">Lives (Inline format)<br /><span class="small">Select all that may--}}
											{{--apply</span></label>--}}
                                    {{--<div class="custom-control custom-checkbox custom-control-inline">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="suteppu">--}}
                                        {{--<label for="suteppu" class="custom-control-label">Step ZERO to ONE!</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="custom-control custom-checkbox custom-control-inline">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="hpt">--}}
                                        {{--<label for="hpt" class="custom-control-label">HAPPY PARTY TRAIN</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="custom-control custom-checkbox custom-control-inline">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="wandefuru">--}}
                                        {{--<label for="wandefuru" class="custom-control-label">WONDERFUL STORIES</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="custom-control custom-checkbox custom-control-inline">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="yousoro">--}}
                                        {{--<label for="yousoro" class="custom-control-label">Sailing to the Sunshine!</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="custom-control custom-checkbox custom-control-inline">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="rainbow">--}}
                                        {{--<label for="rainbow" class="custom-control-label">Next SPARKLING!!</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-row">--}}
                                {{--<div class="form-group col-4">--}}
                                    {{--<label for="">Idol Group (List Format)</label>--}}
                                    {{--<div class="custom-control custom-radio">--}}
                                        {{--<input type="radio" id="muse" name="idolGroup" class="custom-control-input">--}}
                                        {{--<label for="muse" class="custom-control-label">µ's</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="custom-control custom-radio">--}}
                                        {{--<input type="radio" id="aqours" name="idolGroup" class="custom-control-input">--}}
                                        {{--<label for="aqours" class="custom-control-label">Aqours</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="custom-control custom-radio">--}}
                                        {{--<input type="radio" id="nhsic" name="idolGroup" class="custom-control-input">--}}
                                        {{--<label for="nhsic" class="custom-control-label">Nijigasaki High School Idol Club</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group col-8">--}}
                                    {{--<label for="" class="custom-label-inline">Idol Group (Inline Format)</label>--}}
                                    {{--<div class="custom-control custom-radio custom-control-inline">--}}
                                        {{--<input type="radio" id="muse" name="idolGroup" class="custom-control-input">--}}
                                        {{--<label for="muse" class="custom-control-label">µ's</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="custom-control custom-radio custom-control-inline">--}}
                                        {{--<input type="radio" id="aqours" name="idolGroup" class="custom-control-input">--}}
                                        {{--<label for="aqours" class="custom-control-label">Aqours</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="custom-control custom-radio custom-control-inline">--}}
                                        {{--<input type="radio" id="nhsic" name="idolGroup" class="custom-control-input">--}}
                                        {{--<label for="nhsic" class="custom-control-label">Nijigasaki High School Idol Club</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group form-actions">--}}
                                {{--<div class="d-flex justify-content-end">--}}
                                    {{--<button type="submit" class="btn btn-success">Save changes</button>--}}
                                    {{--<button class="btn btn-outline-secondary">Cancel</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-12">--}}
                {{--<div class="card">--}}
                    {{--<div class="card-body">--}}
                        {{--<div class="mb-5">--}}
                            {{--<h5 class="h5">Hey, it's just another card. Cards acts as a section of the page.</h5>--}}
                        {{--</div>--}}

                        {{--<div>--}}
                            {{--<p>Pagination:</p>--}}
                            {{--<nav aria-label="Page navigation example">--}}
                                {{--<ul class="pagination">--}}
                                    {{--<li class="page-item previous disabled"><a class="page-link" href="#">Previous</a></li>--}}
                                    {{--<li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                                    {{--<li class="page-item next"><a class="page-link" href="#">Next</a></li>--}}
                                {{--</ul>--}}
                            {{--</nav>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection()