@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">User Management</li>
                    </ol>
                </nav>

                <a href="{{ url('/dashboard/user-management/add') }}" class="btn btn-sm btn-primary mt-5 float-right">Add User</a>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">User Management</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Division</th>
                                    <th>Role</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>

                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->Division->name ?? "" }}</td>
                                        <td>{{ $user->role }}</td>
                                        <td><a href="{{ url("/dashboard/user-management/{$user->id}") }}" class="btn btn-sm btn-primary">Update</a></td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
@endsection()