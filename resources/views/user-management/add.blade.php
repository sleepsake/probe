@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard/user-management') }}">User Management</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add User</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">User Management <small>(Add User)</small></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        <form method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="">Username *</label>
                                <input type="email" class="form-control" id="" name="email" value="{{ old('email') }}" required>
                            </div>


                            <div class="form-group">
                                <label for="">Name *</label>
                                <input type="text" class="form-control" id="" name="name" value="{{ old('name') }}" required>
                            </div>


                            <div class="form-group">
                                <label for="">Password *</label>
                                <input type="password" class="form-control" name="password" id="" placeholder="" required>
                            </div>


                            <div class="form-group">
                                <label for="">Confirm Password *</label>
                                <input type="password" class="form-control" name="password_confirmation" id="" placeholder="" required>
                            </div>


                            <div class="form-group">
                                <label for="">Division *</label>
                                <select id="" class="form-control" name="division_id" required>
                                    @foreach (\App\Division::orderBy('name')->get() as $division)
                                        <option value="{{ $division->id }}" {{ old('division_id') == $division->id ? 'selected' : '' }}>{{ $division->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Role *</label>
                                <select id="role_select" class="form-control" name="role" required>
                                    <option value="PAP User" {{ old('role') == 'PAP User' ? 'selected' : '' }}>PAP User</option>
                                    <option value="Budget Officer" {{ old('role') == 'Budget Officer' ? 'selected' : '' }}>Budget Officer</option>
                                    <option value="Integrator" {{ old('role') == 'Integrator' ? 'selected' : '' }}>Integrator</option>
                                    <option value="BAC" {{ old('role') == 'BAC' ? 'selected' : '' }}>BAC</option>
                                    <option value="User Admin" {{ old('role') == 'Admin' ? 'selected' : '' }}>Admin</option>
                                    <option value="Super Admin" {{ old('role') == 'Super Admin' ? 'selected' : '' }}>Super Admin</option>
                                </select>
                            </div>

                            <div class="form-group" style="" id="allowed_categories">
                                <label for="">Allowed Categories</label>
                                @foreach ($ppmpCategory as $key => $c)
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="categories[]" value="{{$c->id}}" id="{{$c->id}}">
                                        <label class="form-check-label" for="{{$c->id}}">
                                            {{$c->name}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>

                            <br>

                            <div class="text-center">
                                <a href="{{ url('/dashboard/user-management') }}" class="btn btn-default">Cancel</a>
                                <button id="" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script>
        $(document).ready(function() {

            $(document).on("change","#role_select",function(e){
                role = $("#role_select").val();

                if (role != "User Admin")
                {
                    $("#allowed_categories").show();
                }
                else
                {
                    $("#allowed_categories").hide();
                }
            });

        });
    </script>
@endsection()
