@extends('layouts.dashboard')

@section('main')
    <div class="container cm-container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">PAP</li>
                    </ol>
                </nav>

                <a href="{{ route('pap_create') }}" class="btn btn-sm btn-primary mt-5 float-right">Create <i class="fa fa-plus"></i></a>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">{{ date('Y') }} - PROGRAM ACTIVITY PROJECTS (PAPs)</h2>
                    </div>

                    <!-- <div class="col-auto">
                        <a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add new entry</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="container cm-container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">

                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($status == "All")? "active":"";?>" href="{{ route('pap.status') }}">All Status</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($status == "Submitted")? "active":"";?>" href="{{ route('pap.status', 'Submitted') }}">Pending</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($status == "For Revision")? "active":"";?>" href="{{ route('pap.status', 'For Revision') }}">For Revision</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($status == "Approved")? "active":"";?>" href="{{ route('pap.status', 'Approved') }}">Approved</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($status == "Rejected")? "active":"";?>" href="{{ route('pap.status', 'Rejected') }}">Rejected</a>
                            </li>
                        </ul><br>
                        <hr>
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:115px">Timestamp</th>
                                    <th style="width:75px">Code</th>
                                    <th>Project</br>Description</th>
                                    <th style="width:75px">PAP Type</th>
                                    <th style="width:75px">Program</th>
                                    <th style="width:75px">Cluster</th>


                                    <th style="width:65px">Status</th>
                                    @if ($status == "Accepted" || $status == "Revision" || $status == "Rejected")
                                        <th>
                                            Reviewer
                                        </th>
                                    @endif
                                    {{-- <th style="width:65px">Function</th> --}}
                                    <th style="width:115px">Budget Estimate</th>
                                    <th style="width:75px">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($paps as $pap)
                                <tr>
                                    <td data-order="{{ date('Ymd', strtotime($pap->created_at)) }}">{{ date('F j, Y', strtotime($pap->created_at)) }}</td>
                                    <td>
                                        @if(in_array($pap->status, ['Draft', 'For Revision']))
                                            <a href="{{ route('pap_update', $pap->id) }}">{{ $pap->code }}</a>
                                        @else
                                            {{ $pap->code }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ $pap->project_description }}
                                    </td>
                                    <td>{{ optional($pap->type)->name }}</td>
                                    <td>{{ optional($pap->cluster)->name }}</td>
                                    <td>{{(new \App\ClusterSubcategory)->get_by_id($pap->cluster_id)->name}}</td>
                                    <td>
                                        @if ($pap->status == "Submitted")
                                            <span class="badge badge-info">Submitted</span>
                                        @elseif ($pap->status == "For Revision")
                                            <span class="badge badge-warning">For Revision</span>
                                        @elseif ($pap->status == "Draft")
                                            <span class="badge badge-dark">Draft</span>
                                        @elseif ($pap->status == "Approved")
                                            <span class="badge badge-success">Approved</span>
                                        @else
                                            <span class="badge badge-danger">Rejected</span>
                                        @endif

                                    </td>
                                    @if ($status == "Accepted" || $status == "Revision" || $status == "Rejected")
                                        <td>
                                            Bruce Wayne
                                        </td>
                                    @endif
                                    {{-- <td>
                                        @php $rand = rand(0,2) @endphp

                                        @if ($rand == 0)
                                            <span class="badge badge-warning">Pending</span>
                                        @elseif ($rand == 1)
                                            <span class="badge badge-success">Approved</span>
                                        @else
                                            <span class="badge badge-danger">Rejected</span>
                                        @endif
                                     </td> --}}
                                    <td class="number">P {{ number_format($pap->items->sum('total_cost') ?? 0, 2) }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{route('request.profile', $pap->id)}}" class="btn btn-sm btn-primary">View <i class="fa fa-caret-right"></i></a>
                                            <form action="{{route('pap_delete', $pap->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-light"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                "order": [[ 8, "desc" ]]
            } );
        });
    </script>
@endsection()
