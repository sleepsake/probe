@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('pap.status', ['status' => 'All']) }}">Requests</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$pap->code}}</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">

                        @switch($pap->status)
                            @case('For Revision')
                                <h3><span class="badge badge-info" style="float:right">For Revision</span></h3>
                                @break
                            @case('Submitted')
                                <h3><span class="badge badge-info" style="float:right">Submitted</span></h3>
                                @break
                            @case('Approved')
                                <h3><span class="badge badge-success" style="float:right">Approved</span></h3>
                            @default
                        @endswitch

                        <h2 class="h2 d-inline-block">{{$pap->code}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Details</h5>


                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="">Code</label>
                                    <p>{{ $pap->code }}</p>
                                </div>
                                <div class="form-group col-8">
                                    <label for="">Project Description</label>
                                    <p>{{ $pap->project_description }}</p>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="">Date</label>
                                    <p>{{ date('F j, Y', strtotime($pap->date)) }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">PAP Type</label>
                                    <p>{{ optional($pap->type)->name }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">Program</label>
                                    <p>{{ optional($pap->cluster)->name }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">Cluster</label>
                                    <p>{{(new \App\ClusterSubcategory)->get_by_id($pap->cluster_id)->name}}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">Source of Funds</label>
                                    <p>{{ $pap->source_of_funds }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">MOOE/CO</label>
                                    <p>{{ $pap->mooe_co }}</p>
                                </div>
                                <div class="form-group col-4"></div>
                                <div class="form-group col-4">
                                    <label for="" style="text-align:center;width:100%">Created By</label>
                                    <p style="text-align:center">{{ $pap->user->name }}</p>
                                </div>
                                {{-- <div class="form-group col">
                                    <label for="">Remarks</label>
                                    @if($pap->status == 'Approved')
                                        <p>{{ $pap->remarks }}</p>
                                    @else
                                        <textarea name="remarks" id="" cols="30" rows="4" class="form-control" placeholder=""></textarea>
                                    @endif

                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card" style="margin-top:10px">
                        <div class="card-body">
                            <h5 class="card-title">Items</h5>
                            <table id="items" class="table " style="width:100%">
                                <thead>
                                <tr>
                                    <th>Account Title</th>
                                    <th>Item / Requirements</th>
                                    <th class="number">Quantity</th>
                                    <th>Unit</th>
                                    <th class="number">Unit Cost</th>
                                    <th class="number">Total Amount</th>
                                    <th>P / NP</th>
                                    <th>Add Remarks</th>
                                </tr>
                                </thead>
                                <tbody>

                                    @foreach ($pap->items as $item)
                                        <tr data-json="">
                                            <td>{{ optional($item->category)->name }}</td>
                                            <td>{{ $item->item_name }}</td>
                                            <td class="number">{{ $item->quantity }}</td>
                                            <td>{{ optional($item->UnitOfMeasure)->name }}</td>
                                            <td class="number">P{{ $item->unit_cost }}</td>
                                            <td class="number">P{{ number_format($item->unit_cost * $item->quantity, 2) }}</td>
                                            <td> Yes </td>
                                            <td>
                                                <form action="{{ route('request.item.add_remarks', $item->id) }}" method="post" onsubmit="submitAddRemark(event, '{{$item->id}}', '{{ route('request.item.add_remarks') }}')" id="form-{{ $item->id }}">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{ $item->id }}">
                                                    {{-- <input type="text" name="remarks" class="form-control" value="{{ $item->remarks }}" id="remarks-{{ $item->id }}" onkeyup="addRemarks(event, '{{$item->remarks}}', '{{$item->id}}')"> --}}

                                                    <textarea name="remarks" id="remarks-{{ $item->id }}" rows="3" onkeyup="addRemarks(event, '{{$item->id}}')" data-remark="{{ $item->remarks }}">{{ $item->remarks }}</textarea>
                                                    <br>
                                                    
                                                    <button type="submit" class="btn btn-sm btn-dark mt-2 d-none" id="btn-remarks-{{ $item->id }}">Add Remarks</button>
                                                </form>
                                            </td>
                                        </tr>
                                        <tr>
                                            @if ($item->specifications)
                                            <td colspan="8" style="border-top: transparent;">
                                                <div class="alert" style="background-color:#eee">
                                                    <b>Specifications</b><br>
                                                    {{ $item->specifications }}
                                                </div>
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach

                                </tbody>
                                <tfoot>
                                {{-- <tr>
                                    <th colspan="3"></th>
                                    <th>Total Amount</th>
                                    <th id="totalAmount">{{ number_format($pap->Items->sum('total_cost'), 2) }}</th>
                                    <th></th>
                                    <th></th>
                                </tr> --}}
                                </tfoot>
                            </table>


                            <div class="form-group col">
                                <label for="">General Remarks</label>
                                @if($pap->status == 'Approved')
                                    <p>{{ $pap->remarks }}</p>
                                @else
                                <form onsubmit="submitAddRemark(event, '{{$pap->id}}', '{{ route('request.update_remarks', $pap->id) }}')"
                                    id="form-{{$pap->id}}" action="{{ route('request.update_remarks', $pap->id) }}" method="post"
                                    style="display:inline">
                                    @csrf
                                    <textarea name="remarks" id="" cols="30" rows="4" class="form-control"
                                        onkeyup="addRemarks(event, '{{$pap->id}}')" data-remark="{{ $item->remarks }}">{{ $pap->remarks }}</textarea>
                                    <button class="btn btn-primary mt-3 d-none" id="btn-remarks-{{$pap->id}}">Save Remarks</button>
                                </form>

                                @endif

                            </div>


                            <div class="text-right">


                                @if($pap->status == 'Approved' || $pap->status == 'For Revision')
                                    <a href="{{ url('/dashboard/apb') }}" class="btn btn-primary">Back to list</a>
                                @else
                                    <a href="{{ url('/dashboard/apb') }}" class="btn btn-default">Cancel</a>

                                <form action="{{ route('request.status.update', [$pap->id, 'Rejected']) }}" method="post" style="display:inline">
                                    @csrf
                                    <button name="submit" value="Rejected" class="btn btn-danger"
                                        onclick="return confirm('Are you sure you want to Reject this?')">Reject</button>
                                </form>

                                <form action="{{ route('request.status.update', [$pap->id, 'For Revision']) }}" method="post" style="display:inline">
                                    @csrf
                                    <button name="submit" value="For Revision" class="btn btn-warning"
                                        onclick="return confirm('Are you sure you want to save this as For Revision?')">For Revision</button>
                                </form>

                                <form action="{{ route('request.status.update', [$pap->id, 'Approved']) }}" method="post" style="display:inline">
                                    @csrf
                                    <button name="action" value="Approved" class="btn btn-primary"><i class="fa fa-send"></i>
                                        Approve</button>
                                </form>

                                @endif

                            </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

   
    <script>
        
    
        function addRemarks(e, id) {
            var new_remarks = e.currentTarget.value;
            var old = $(e.currentTarget).data('remark');
            resize(e.currentTarget);
            console.log(id);
            if(new_remarks != old)
                $('#btn-remarks-'+id).removeClass('d-none');
            else
                $('#btn-remarks-'+id).addClass('d-none');
        }


        function submitAddRemark(e, id, link) {
            e.preventDefault();
            var old_input = $('#btn-remarks-'+id).html();
            $('#btn-remarks-'+id).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');
            $.ajax({
                type: "POST",
                url: link,
                data: $('#form-'+id).serialize(),
                success: function() {
                    $('#btn-remarks-'+id).addClass('d-none').html(old_input);
                }
            });
        }
        
        function resize(element){
            if ($(element).outerHeight() > 162) {
                while($(element).outerHeight() < element.scrollHeight) {
                    $(element).height($(element).height()+1);
                };

                while($(element).outerHeight() > element.scrollHeight) {
                    $(element).height($(element).height()-1);
                };   

            } else {
                while($(element).outerHeight() < element.scrollHeight) {
                    if ($(element).height())
                    $(element).height($(element).height()+1);
                };
            }
        }
    </script>
@endsection()
