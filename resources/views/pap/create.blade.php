@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard/pap') }}">PAP</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Program Activity Project</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">CREATE PROGRAM ACTIVITY PROJECTS (PAPs)</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST">
                            @csrf

                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="">Code</label>
                                    <input type="text" class="form-control" id="" name="code" placeholder="" required>
                                </div>
                                <div class="form-group col-6">
                                    <label for="">Project Description</label>
                                    <input type="text" class="form-control" id="" name="project_description" placeholder="" required>
                                </div>
                                <div class="form-group col">
                                    <label for="">Date</label>
                                    <input type="text" class="form-control form-date" id="" name="date" value="{{ date('Y-m-d') }}" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-3">
                                    <label for="">PAP Type</label>
                                    <select id="" class="form-control" name="program_type_id" required>
                                        <option value="" selected disabled>Select type</option>
                                        @foreach(\App\ProgramType::where('is_deleted', 0)->get() as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <label for="">Program</label>
                                    <select id="selectProgram" class="form-control" name="program_id" required>
                                        <option value="" selected disabled>Select program</option>
                                        @foreach(\App\Cluster::where('is_deleted', 0)->get() as $program)
                                            <option value="{{ $program->id }}" data-clusters='{{ $program->SubCategories }}'>{{ $program->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <label for="">Cluster</label>
                                    <select id="selectCluster" class="form-control" name="cluster_id" required>
                                        <option value="" selected disabled>Select cluster</option>
                                        {{-- <option value="GAS">GAS 1.1</option> --}}
                                    </select>
                                </div>

                                {{--<div class="form-group col-3">--}}
                                    {{--<label for="">Source of Funds</label>--}}
                                    {{--<select id="" class="form-control" name="source_of_funds">--}}
                                        {{--<option value="GOP">GOP</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<div class="form-group col-3">--}}
                                    {{--<label for="">MOOE/CO</label>--}}
                                    {{--<select id="" class="form-control" name="mooe_co">--}}
                                        {{--<option value="MOOE">MOOE</option>--}}
                                        {{--<option value="Capital Outlay">Capital Outlay</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            </div>

                            <hr>

                            <button type="button" class="btn btn-sm btn-primary mb-5" data-toggle="modal" data-target="#addItemModal"><i class="fa fa-plus"></i> Add item</button>


                            <table id="items" class="table " style="width:100%">
                                <thead>
                                <tr>
                                    <th>Item / Requirements</th>
                                    <th>Account Title</th>
                                    <th>Quantity</th>
                                    <th>Unit Cost</th>
                                    <th>Total Amount</th>
                                    <th>P / NP</th>
                                    <th>Specifications</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3"></th>
                                        <th>Total Amount</th>
                                        <th id="totalAmount"></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>

                            <br>

                            {{-- <div class="form-row">
                                <div class="form-group col">
                                    <label for="">Advertisement/Posting of IB/REI</label>
                                    <input type="text" class="form-control form-date" id="" name="advertisement" value="{{ date('Y-m-d') }}" required>
                                </div>
                                <div class="form-group col">
                                    <label for="">Submission/Opening of Bids</label>
                                    <input type="text" class="form-control form-date" id="" name="submission_of_bids" value="{{ date('Y-m-d') }}" required>
                                </div>
                                <div class="form-group col">
                                    <label for="">Notice of Award</label>
                                    <input type="text" class="form-control form-date" id="" name="notice_of_award" value="{{ date('Y-m-d') }}" required>
                                </div>
                                <div class="form-group col">
                                    <label for="">Contract Signing</label>
                                    <input type="text" class="form-control form-date" id="" name="contract_signing" value="{{ date('Y-m-d') }}" required>
                                </div>
                            </div> --}}

                            <br>
                            <br>

                            <div class="text-right">
                                <a href="{{ url('/dashboard/pap') }}" class="btn btn-default">Cancel</a>
                                <button id="submitPAP" name="action" value="draft" class="btn btn-warning" onclick="return confirm('Are you sure you want to save this as draft?')">Save Draft</button>
                                <button id="submitPAP" name="action" value="submit" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addItemModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 80%; width: 700px;">
            <div class="modal-content">
                <div class="form">
                    <div class="modal-body">
                        <div class="inline-header mb-4">
                            <h5 class="h5 font-weight-semibold">ADD ITEM / REQUIREMENT</h5>
                        </div>

                        <form id="add_item_form" action="">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="">Item Name</label>
                                    <input type="text" id="addItemName" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-3">
                                    <label for="">Quantity</label>
                                    <input type="text" id="addItemQuantity" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g, '')" required>
                                </div>

                                <div class="form-group col-3">
                                    <label for="">Unit of Measure</label>
                                    <select class="form-control" id="addItemUnitMeasure">
                                        @foreach(\App\UnitOfMeasure::where('is_deleted', 0)->get() as $unit)
                                            <option value="{{$unit->id}}">{{$unit->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-3">
                                    <label for="">Unit Cost</label>
                                    <input type="text" id="addItemUnitCost" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>

                                <div class="form-group col-3">
                                    <label for="">Total Cost</label>
                                    <input type="text" id="addItemTotalCost" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="">Category</label>
                                    <select name="" class="form-control selectCategory">
                                        <option value="" selected disabled></option>
                                        @foreach(\App\Categories::where('is_deleted', 0)->get() as $category)
                                            @if($loop->first)
                                                @php
                                                    $firstSubCategories = $category->SubCategories;
                                                @endphp
                                            @endif
                                            <option value="{{ $category->id }}" data-json='{{ collect($category->SubCategories) }}' data-procurement="{{ $category->is_procurement }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-4">
                                    <label for="">Sub-Category</label>
                                    <select name="" class="form-control selectSubCategory" disabled>
                                        <option value="" selected disabled></option>
                                        @foreach(\App\Categories::get()->first()->SubCategories as $subCategory)
                                            <option value="{{ $subCategory->id }}">{{ $subCategory->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="">Technical Specifications / Requirements</label>
                                    <textarea name="" id="addItemSpecifications" cols="" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary modal-close" data-dismiss="modal">Close</button>
                        <button type="submit" id="addItem" class="btn btn-success">Add Item</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="updateItemModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 80%; width: 700px;">
            <div class="modal-content">
                <div class="form">
                    <div class="modal-body">
                        <div class="inline-header mb-4">
                            <h5 class="h5 font-weight-semibold">UPDATE ITEM / REQUIREMENT</h5>
                        </div>

                        <form action="">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="">Item Name</label>
                                    <input type="text" id="updateItemName" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-3">
                                    <label for="">Quantity</label>
                                    <input type="text" id="updateItemQuantity" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g, '')" required>
                                </div>

                                <div class="form-group col-3">
                                    <label for="">Unit of Measure</label>
                                    <select class="form-control" name="updateItemUnit" id="updateItemUnit">
                                        @foreach (\App\UnitOfMeasure::where('is_deleted', 0)->get() as $unit)
                                            <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-3">
                                    <label for="">Unit Cost</label>
                                    <input type="text" id="updateItemUnitCost" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>

                                <div class="form-group col-3">
                                    <label for="">Total Cost</label>
                                    <input type="text" id="updateItemTotalCost" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="">Category</label>
                                    <select name=""  class="form-control selectCategory">
                                        @foreach(\App\Categories::where('is_deleted', 0)->get() as $category)
                                            @if($loop->first)
                                                @php
                                                    $firstSubCategories = $category->SubCategories;
                                                @endphp
                                            @endif
                                            <option value="{{ $category->id }}" data-json='{{ collect($category->SubCategories) }}' data-procurement="{{ $category->is_procurement }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">Sub-Category</label>
                                    <select name="" class="form-control selectSubCategory">

                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="">Technical Specifications / Requirements</label>
                                    <textarea name="" id="updateItemSpecifications" cols="" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary modal-close" data-dismiss="modal">Close</button>
                        <button type="submit" id="updateItem" class="btn btn-success">Update Item</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pap.script')
@endsection()
