@extends('layouts.dashboard')

@section('main')
    <div class="container cm-container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">PAP</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h2 class="h2 d-inline-block">PROGRAM ACTIVITY PROJECTS (PAPs)</h2>

                        <div class="alert subtle">
                            <i class="fa fa-exclamation-circle"></i> Select a year to see all <b>approved</b> PAP Archives for that year.
                        </div>

                    </div>

                    <!-- <div class="col-auto">
                        <a href="#" class="btn btn-primary"><i class="fas fa-plus mr-1"></i>Add new entry</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="container cm-container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                @php
                    $years = ['2020','2019','2018','2017','2016','2015']
                @endphp
                <nav  class="nav nav-pills selector nav-justified cm-pills">
                    @foreach($years as $y)
                        <a href="{{route('request.pap.vault', $y)}}" class="nav-item nav-link <?php echo ($y == $year)? "active":"";?>" style="margin-right:5px;margin-top:5px">{{$y}}</a>
                    @endforeach
                </nav>

                <div class="card" style="margin-top:10px">
                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:115px">Timestamp</th>
                                    <th style="width:75px">Code</th>
                                    <th>Project</br>Description</th>
                                    <th style="width:75px">PAP Type</th>
                                    <th style="width:75px">Program</th>
                                    <th style="width:75px">Cluster</th>
                                    <th>Reviewer</th>
                                    {{-- <th style="width:65px">Function</th> --}}
                                    <th style="width:115px">Budget Estimate</th>
                                    <th style="width:75px">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($paps as $pap)
                                <tr>
                                    <td data-order="{{ date('Ymd', strtotime($pap->created_at)) }}">{{ date('F j, Y', strtotime($pap->created_at)) }}</td>
                                    <td>
                                        @if(in_array($pap->status, ['Draft', 'For Revision']))
                                            <a href="{{ route('pap_update', $pap->id) }}">{{ $pap->code }}</a>
                                        @else
                                            {{ $pap->code }}
                                        @endif
                                    </td>
                                    <td>{{ $pap->project_description }}</td>
                                    <td>{{ optional($pap->type)->name }}</td>
                                    <td>{{ $pap->Type->name ?? '' }}</td>
                                    <td>{{ $pap->Cluster->name ?? '' }}</td>
{{--                                    <td>{{ optional((new \App\ClusterSubcategory)->get_by_id($pap->cluster_id)->name) }}</td>--}}

                                    <td>{{$pap->Approver->name}}</td>


                                    <td class="number">P {{ number_format($pap->items->sum('total_cost') ?? 0, 2) }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{route('request.profile', $pap->id)}}" class="btn btn-sm btn-primary">View <i class="fa fa-caret-right"></i></a>
                                            <a href="#" class="btn btn-sm btn-light"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                "order": [[ 8, "desc" ]]
            } );
        });
    </script>
@endsection()
