<script>
  $(document).ready(function() {
      // $('#submitPAP').click(function() {
      //     Swal.fire({
      //         type: 'success',
      //         title: 'Success!',
      //         html: `Program / Activity / Project Added <br>
      //                 <strong>PAP Code: 300100200400</strong>`
      //     })
      // });

      // $("#addItemUnitCost, #addItemQuantity").on('keyup', function() {
      //     var totalCost = 0;
      //     if($("#addItemQuantity").val().length > 0 && $("#addItemUnitCost").val().length > 0) {
      //         const quantity = parseInt($("#addItemQuantity").val());
      //         const price = parseFloat($("#addItemUnitCost").val());
      //         totalCost = quantity * price;
      //     }
      //
      //     $("#addItemTotalCost").val(totalCost);
      // });

      $(document).on('change', ".selectCategory", (function() {
          const subCategories = $(this).find('option:selected').data('json');
          $(".selectSubCategory").removeAttr('disabled', 'disabled');
          $(".selectSubCategory > option").remove();
          $.each(subCategories, function(index, me) {
              $(".selectSubCategory").append(`<option value="${me.id}">${me.name}</option>`);
          });
      }));

      $(document).on('change', "#forProcurement", (function() {
          const me = $(this);

          if(me.is(':checked') === true) {
              const categories = me.data("p");
              $(".selectCategory > option").remove();
              var x = 0;
              $.each(categories, function(category, subcategories) {
                  const json = JSON.stringify(subcategories);
                  $(".selectCategory").append(`<option value="${category}" data-json='${json}'>${category}</option>`);
                  if(x == 0) {
                      $(".selectSubCategory > option").remove();
                      $.each(subcategories, function(index, me) {
                          $(".selectSubCategory").append(`<option value="${me}">${me}</option>`);
                      });
                  }
                  x++;
              });
          } else {
              const categories = me.data("np");
              $(".selectCategory > option").remove();
              var x = 0;
              $.each(categories, function(category, subcategories) {
                  const json = JSON.stringify(subcategories);
                  $(".selectCategory").append(`<option value="${category}" data-json='${json}'>${category}</option>`);
                  if(x == 0) {
                      $(".selectSubCategory > option").remove();
                      $.each(subcategories, function(index, me) {
                          $(".selectSubCategory").append(`<option value="${me}">${me}</option>`);
                      });
                  }
                  x++;
              });
          }
      }));

      var x = {{ !empty($count) ? $index + 1 : 0 }};
      console.log(x);
      $("#addItem").click(function() {
          const item = {};
          item.item_name = $("#addItemName").val().trim();
          item.quantity = $("#addItemQuantity").val().trim();
          item.unit_cost = $("#addItemUnitCost").val().trim();
          item.total_cost = $("#addItemTotalCost").val().trim();
          item.unit_of_measure_id = $("#addItemModal #addItemUnitMeasure > option:selected");
          item.unit_of_measure_id_val = $("#addItemModal #addItemUnitMeasure > option:selected").val();
          item.category = $("#addItemModal .selectCategory > option:selected");
          item.category_val = $("#addItemModal .selectCategory > option:selected").val();
          item.sub_category = $("#addItemModal .selectSubCategory > option:selected");
          item.sub_category_val = $("#addItemModal .selectSubCategory > option:selected").val();
          item.for_procurement = item.category.attr('data-procurement');
          item.specifications = $("#addItemSpecifications").val();


          if(item.item_name.length > 0 && item.quantity > 0 && item.unit_cost > 0 && item.total_cost > 0) {
              // console.log(item);

              const json = JSON.stringify(item);
              const p = item.for_procurement;
              const unitCost = parseFloat(item.unit_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
              const totalCost = parseFloat(item.total_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

              $("table#items > tbody").append(`
                      <tr data-index='${x}' data-json='${json}'>
                          <td>${item.item_name}</td>
                          <td>${item.category.text()}</td>
                          <td>${item.quantity} ${item.unit_of_measure_id.text()}</td>
                          <td>${unitCost}</td>
                          <td>${totalCost}</td>
                          <td>${p}</td>
                          <td>${item.specifications}</td>
                          <td>
                              <button type="button" class="btn btn-sm btn-outline-danger removeItem m-1"><i class="fa fa-trash"></i></button>
                              <button type="button" class="btn btn-sm btn-outline-primary updateItem m-1"><i class="fa fa-edit"></i></button>
                          </td>
                          <input type="hidden" name="item[${x}][item_name]" value="${item.item_name}">
                          <input type="hidden" name="item[${x}][quantity]" value="${item.quantity}">
                          <input type="hidden" name="item[${x}][unit_cost]" value="${item.unit_cost}">
                          <input type="hidden" name="item[${x}][total_cost]" value="${item.total_cost}">
                          <input type="hidden" name="item[${x}][category_id]" value="${item.category.val()}">
                          <input type="hidden" name="item[${x}][unit_of_measure_id]" value="${item.unit_of_measure_id.val()}">
                          <input type="hidden" name="item[${x}][sub_category_id]" value="${item.sub_category.val()}">
                          <input type="hidden" name="item[${x}][for_procurement]" value="${p}">
                          <input type="hidden" name="item[${x}][specifications]" value="${item.specifications}">
                      </tr>`);
              $(this).closest('.form').find('form').trigger('reset');
              $("#addItemModal").modal('hide');
              x++;
              updateTotalAmount();
          } else {
              if($("#addItemName").val().trim().length == 0) {
                  return $("#addItemName").focus();
              }
              if($("#addItemQuantity").val().trim().length == 0) {
                  return $("#addItemQuantity").focus();
              }
              if($("#addItemUnitCost").val().trim().length == 0) {
                  return $("#addItemUnitCost").focus();
              }
              if($("#addItemTotalCost").val().trim().length == 0) {
                  return $("#addItemTotalCost").focus();
              }
          }

          $('#add_item_form').trigger('reset');
          $(".selectSubCategory").attr('disabled', 'disabled');
      });

      function updateTotalAmount()
      {
          var totalAmount = 0;

          $("table#items > tbody > tr").each(function(i, me) {
              const index = $(this).data('index');
              let amount =  $(this).children('input[name="item['+index+'][total_cost]"]').val();
              totalAmount += parseFloat(amount);
          });


          $("#totalAmount").text('P ' + totalAmount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
      }

      $(".modal-close").click(function() {
          $(this).closest('.modal-content').find('form').trigger('reset');
      });

      $("#selectProgram").change(function() {
          const clusters = $(this).find('option:selected').data('clusters');
          // console.log(clusters);

          $("#selectCluster > option").remove();

          $.each(clusters, function(index, cluster) {
              $("#selectCluster").append(`<option value="${cluster.id}">${cluster.name}</option>`);
          });
      });

      $(document).on('click', '.removeItem', function() {
          if(confirm('Are you sure you want to remove this item?') === true) {
              $(this).closest('tr').remove();

              updateTotalAmount();
          }
      });

      let selectedItem;
      $(document).on('click', '.updateItem', function() {
          selectedItem = $(this).closest('tr');
          let selectedItemDetails = $.parseJSON(selectedItem.attr('data-json'));
          $("#updateItemName").val(selectedItemDetails.item_name);
          $("#updateItemQuantity").val(selectedItemDetails.quantity);
          $('#updateItemUnit').val(selectedItemDetails.unit_of_measure_id_val || selectedItemDetails.unit_of_measure_id);
          $("#updateItemUnitCost").val(selectedItemDetails.unit_cost);
          $("#updateItemTotalCost").val(selectedItemDetails.total_cost);
          $("#updateItemSpecifications").val(selectedItemDetails.specifications);
          if(selectedItemDetails.for_procurement == 'P') {
              $("#updateItemModal").find(".custom-control-label.mt-3").trigger('click');
          }

          if(selectedItemDetails.for_procurement == 'NP' && $("#updateItemModal").find('input[type="checkbox"]').prop('checked') === true) {
              $("#updateItemModal").find(".custom-control-label.mt-3").trigger('click');
          }
          console.log(selectedItemDetails);
          console.log(selectedItemDetails.category_val || selectedItemDetails.category_id);
          $("#updateItemModal").find('.selectCategory > option').each(function(el) {
              if($(this).val() == (selectedItemDetails.category_val || selectedItemDetails.category_id)) {
                  // console.log(true);
                  $(this).prop('selected', true).trigger('change');

                  $("#updateItemModal").find('.selectSubCategory > option').remove();

                  const subCategories = $(this).data('json');
                  $.each(subCategories, function(index, subcategory) {
                      $("#updateItemModal .selectSubCategory").append(`<option value="${subcategory.id}">${subcategory.name}</option>`);
                  });

              }
          });

          $("#updateItemModal").find('.selectSubCategory > option').each(function(el) {
              if($(this).val() == (selectedItemDetails.sub_category_val || selectedItemDetails.sub_category_id)) {
                  $(this).prop('selected', true).trigger('change');
              }
          });


          $("#updateItemModal").modal('show');
      });

      $("#updateItem").click(function() {
          const item = {};
          item.item_name = $("#updateItemName").val().trim();
          item.quantity = $("#updateItemQuantity").val().trim();
          item.unit_cost = $("#updateItemUnitCost").val().trim();
          item.total_cost = $("#updateItemTotalCost").val().trim();
          // console.log($("#updateItemModal .selectCategory > option:selected").attr('data-procurement'));
          item.unit_of_measure_id = $("#updateItemModal #updateItemUnit > option:selected").val();
          item.category_id = $("#updateItemModal .selectCategory > option:selected").val();
          item.sub_category_id = $("#updateItemModal .selectSubCategory > option:selected").val();
          item.for_procurement = $("#updateItemModal .selectCategory > option:selected").attr('data-procurement');
          item.specifications = $("#updateItemSpecifications").val();


          if(item.item_name.length > 0 && item.quantity > 0 && item.unit_cost > 0 && item.total_cost > 0) {
              // console.log(item);

              const json = JSON.stringify(item);
              const p = item.for_procurement;
              const unitCost = parseFloat(item.unit_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
              const totalCost = parseFloat(item.total_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
              const index = selectedItem.data('index');
              selectedItem.attr('data-json', json);
              selectedItem.find('td').eq(0).text(`${item.item_name}`);
              selectedItem.find('td').eq(1).text(`${$("#updateItemModal .selectCategory > option:selected").text()}`);
              selectedItem.find('td').eq(2).text(`${item.quantity}`);
              selectedItem.find('td').eq(3).text(`${unitCost}`);
              selectedItem.find('td').eq(4).text(`${totalCost}`);
              selectedItem.find('td').eq(5).text(`${p}`);
              selectedItem.find('td').eq(6).text(`${item.specifications}`);
              selectedItem.find('input[name="item['+index+'][item_name]"]').val(`${item.item_name}`);
              selectedItem.find('input[name="item['+index+'][quantity]"]').val(`${item.quantity}`);
              selectedItem.find('input[name="item['+index+'][unit_cost]"]').val(`${item.unit_cost}`);
              selectedItem.find('input[name="item['+index+'][total_cost]"]').val(`${item.total_cost}`);
              selectedItem.find('input[name="item['+index+'][unit_of_measure_id]"]').val(`${item.unit_of_measure_id}`);
              selectedItem.find('input[name="item['+index+'][category_id]"]').val(`${item.category_id}`);
              selectedItem.find('input[name="item['+index+'][sub_category_id]"]').val(`${item.sub_category_id}`);
              selectedItem.find('input[name="item['+index+'][for_procurement]"]').val(`${p}`);
              selectedItem.find('input[name="item['+index+'][specifications]"]').val(`${item.specifications}`);

              // $(this).closest('.form').find('form').trigger('reset');
              $("#updateItemModal").modal('hide');
              updateTotalAmount();
              x++;
          } else {
              if($("#updateItemName").val().trim().length == 0) {
                  return $("#updateItemName").focus();
              }
              if($("#updateItemQuantity").val().trim().length == 0) {
                  return $("#updateItemQuantity").focus();
              }
              if($("#updateItemUnitCost").val().trim().length == 0) {
                  return $("#updateItemUnitCost").focus();
              }
              if($("#updateItemTotalCost").val().trim().length == 0) {
                  return $("#updateItemTotalCost").focus();
              }

          }
      });

      $("button[data-target='#addItemModal']").click(function() {
          $("#addItemModal .selectSubCategory").empty();
      });

      updateTotalAmount();
  });
</script>