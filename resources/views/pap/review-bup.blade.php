@extends('layouts.dashboard')

@section('main')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb" style="display: inline-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('pap.status', ['status' => 'All']) }}">Requests</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$pap->code}}</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <h3><span class="badge badge-info" style="float:right">Draft</span></h3>
                        <h2 class="h2 d-inline-block">{{$pap->code}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row card-sections">

            <!-- Add another column for new cards -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Details</h5>
                        <form method="POST">
                            @csrf

                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="">Code</label>
                                    <p>{{ $pap->code }}</p>
                                </div>
                                <div class="form-group col-8">
                                    <label for="">Project Description</label>
                                    <p>{{ $pap->project_description }} Now you can browse privately, and other people who use this device won’t see your activity. However, downloads and bookmarks will be saved.</p>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="">Date</label>
                                    <p>{{ date('F j, Y', strtotime($pap->date)) }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">PAP Type</label>
                                    <p>{{ $pap->type }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">Program</label>
                                    <p>{{ $pap->program }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">Cluster</label>
                                    <p>{{ $pap->cluster }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">Source of Funds</label>
                                    <p>{{ $pap->source_of_funds }}</p>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">MOOE/CO</label>
                                    <p>{{ $pap->mooe_co }}</p>
                                </div>
                                <div class="form-group col-4"></div>
                                <div class="form-group col-4">
                                    <label for="" style="text-align:center;width:100%">Created By</label>
                                    <p style="text-align:center">Alfred Pennyworth</p>
                                </div>
                                {{-- <div class="form-group col">
                                    <label for="">Remarks</label>
                                    @if($pap->status == 'Approved')
                                        <p>{{ $pap->remarks }}</p>
                                    @else
                                        <textarea name="remarks" id="" cols="30" rows="4" class="form-control" placeholder=""></textarea>
                                    @endif

                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card" style="margin-top:10px">
                        <div class="card-body">
                            <h5 class="card-title">Items</h5>
                            <table id="items" class="table " style="width:100%">
                                <thead>
                                <tr>
                                    <th>Item / Requirements</th>
                                    <th>Account Title</th>
                                    <th class="number">Quantity</th>
                                    <th>Unit</th>
                                    <th class="number">Unit Cost</th>
                                    <th class="number">Total Amount</th>
                                    <th>P / NP</th>
                                    <th>Add Remarks</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $x = 0; ?>
                                @foreach($pap->Items as $item)
                                    <tr data-json="{{ $item }}">
                                        <td>{{ $item->item_name }}</td>
                                        <td>{{ $item->Category->name }}</td>
                                        <td class="number">{{ $item->quantity }}</td>
                                        <td>Boxes</td>
                                        <td class="number">{{ $item->unit_cost }}</td>
                                        <td class="number">{{ $item->total_cost }}</td>
                                        <td>{{ $item->for_procurement }}</td>
                                        <td>
                                            @if($pap->status == 'Approved')
                                                <p>{{ $item->remarks }}</p>
                                            @else
                                                <input type="text" name="pap_item[{{ $item->id }}][remarks]" class="form-control">
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="border-top: transparent;">
                                            <div><small><strong>Technical Specifications / Requirements</strong></small></div>
                                            {{ $item ->specifications}}
                                        </td>
                                    </tr>
                                    <?php $x++; ?>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="3"></th>
                                    <th>Total Amount</th>
                                    <th id="totalAmount">{{ number_format($pap->Items->sum('total_cost'), 2) }}</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>


                            <div class="form-group col">
                                <label for="">General Remarks</label>
                                @if($pap->status == 'Approved')
                                    <p>{{ $pap->remarks }}</p>
                                @else
                                    <textarea name="remarks" id="" cols="30" rows="4" class="form-control" placeholder=""></textarea>
                                @endif

                            </div>

                            <div class="text-right">
                                @if($pap->status == 'Approved')
                                    <a href="{{ url('/dashboard/apb') }}" class="btn btn-primary">Back to list</a>
                                @else
                                    <a href="{{ url('/dashboard/apb') }}" class="btn btn-default">Cancel</a>
                                    <button id="" name="action" value="for_revision" class="btn btn-warning" onclick="return confirm('Are you sure you want to save this as For Revision?')">For Revision</button>
                                    <button id="" name="action" value="approve" class="btn btn-primary"><i class="fa fa-send"></i> Approve</button>
                                @endif

                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {

        });
    </script>
@endsection()
