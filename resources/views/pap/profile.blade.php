@extends('layouts.dashboard')

@section('main')
<div class="container">

    <div class="row">
        <div class="col-12">
            <nav aria-label="breadcrumb" style="display: inline-block">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('pap.status', ['status' => 'All']) }}">Requests</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{{$pap->code}}</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row mb-2">
        <div class="col-12">
            <div class="row">
                <div class="col">
                    @if($pap->status == 'For Revision')
                    <h3><span class="badge badge-warning" style="float:right">For Revision</span></h3>
                    @endif

                    @if($pap->status == 'Submitted')
                    <h3><span class="badge badge-info" style="float:right">Submitted</span></h3>
                    @endif

                    @if($pap->status == 'Approved')
                    <h3><span class="badge badge-success" style="float:right">Approved</span></h3>
                    @endif

                    @if($pap->status == 'Draft')
                    <h3><span class="badge badge-dark" style="float: right; color: white">Draft</span></h3>
                    @endif

                    @if($pap->status == 'Rejected')
                    <h3><span class="badge badge-danger" style="float: right">Rejected</span></h3>
                    @endif

                    <h2 class="h2 d-inline-block">{{$pap->code}}</h2>
                    @if(in_array($pap->status, ['Draft', 'For Revision']))
                    <a href="{{ route('pap_update', $pap->id) }}" class="btn btn-warning m-2 btn-sm">Edit <i
                            class="fa fa-caret-right"></i></a>
                    @endif
                    @if($pap->status == 'For Revision' && $pap->remarks)
                    <div class="form-group col-12 alert alert-warning font-weight-bold">
                        <label>Remarks</label>
                        <p class="m-0">{{ $pap->remarks }}</p>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row card-sections">

        <!-- Add another column for new cards -->
        <div class="col-12">

            @if ( in_array($_GET['dummystatus'] ?? '', ['revision','rejected']) )
            <div class="card" style="margin-bottom:10px">
                <div class="card-body">
                    <h5 class="card-title">Approver's Remarks</h5>

                    <div class="alert subtle {{$dummyColors[$_GET['dummystatus']]}}">
                        Now you can browse privately, and other people who use this device won’t see your activity.
                        However, downloads and bookmarks will be saved. Now you can browse privately, and other people
                        who use this device won’t see your activity. However, downloads and bookmarks will be saved. Now
                        you can browse privately, and other people who use this device won’t see your activity. However,
                        downloads and bookmarks will be saved.
                    </div>

                    <span class="approver">Bruce Wayne</span>

                </div>
            </div>
            @endif

            <div class="card cm-profile">
                <div class="card-body">
                    <h5 class="card-title">Details</h5>
                    <div class="form-row">
                        <div class="form-group col-4">
                            <label for="">Code</label>
                            <p>{{ $pap->code }}</p>
                        </div>
                        <div class="form-group col-6">
                            <label for="">Project Description</label>
                            <p>{{ $pap->project_description }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-4">
                            <label for="">Date</label>
                            <p>{{ date('F j, Y', strtotime($pap->date)) }}</p>
                        </div>
                        <div class="form-group col-4">
                                <label for="">PAP Type</label>
                                <p>{{ optional($pap->Type)->name }}</p>
                        </div>
                        <div class="form-group col-4">
                            <label for="">Program</label>
                            <p>{{ optional($pap->Cluster)->name }}</p>

                        </div>
                        <div class="form-group col-4">
                            <label for="">Cluster</label>
                            @if($pap->cluster_id == 0)
                            <p>No Cluster Type</p>
                            @else
                            @php
                            $cluster = \App\ClusterSubcategory::where('id', '=', $pap->cluster_id)->first();
                            @endphp
                            @endif
                            <p>{{ $cluster->name ?? '' }}</p>
                        </div>
                        <div class="form-group col-4">
                            <label for="">Source of Funds</label>
                            <p>{{ $pap->source_of_funds }}</p>
                        </div>
                        <div class="form-group col-4">
                            <label for="">MOOE/CO</label>
                            <p>{{ $pap->mooe_co }}</p>
                        </div>
                        {{-- <div class="form-group col">
                                    <label for="">Remarks</label>
                                    @if($pap->status == 'Approved')
                                        <p>{{ $pap->remarks }}</p>
                        @else
                        <textarea name="remarks" id="" cols="30" rows="4" class="form-control"
                            placeholder=""></textarea>
                        @endif

                    </div> --}}
                </div>
            </div>
        </div>
        <div class="card" style="margin-top:10px">
            <div class="card-body">
                <h5 class="card-title">Items</h5>
                <table id="items" class="table " style="width:100%">
                    <thead>
                        <tr>
                            <th>Item / Requirements</th>
                            <th>Category</th>
                            <th>Quantity</th>
                            <th>Unit Cost</th>
                            <th>Total Amount</th>
                            <th>P / NP</th>
                            <th>PPMP</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($pap->Items as $index => $item)
                        <tr data-json="{{ $item }}">
                            <td>{{ $item->item_name }}</td>
                            <td>{{ $item->Category->name }}</td>
                            <td>{{ $item->quantity }} {{ $item->UnitOfMeasure->name }} </td>
                            <td>{{ number_format($item->unit_cost, 2) }}</td>
                            <td>{{ number_format($item->total_cost, 2) }}</td>
                            <td>{{ $item->for_procurement }}</td>
                            @if(\App\PPMPItems::where('pap_item_id', '=', $item->id)->exists())
                            @php
                            $ppmp_item = \App\PPMPItems::where('pap_item_id', '=', $item->id)->first();
                            @endphp
                            <td> <a
                                    href="{{route('request.ppmp.profile', $ppmp_item->PPMP->id)}}">{{ $ppmp_item->PPMP->ppmp_name }}</a>
                            </td>
                            @else
                            <td>Not yet in any PPMP</td>
                            @endif

                            <td>
                                @if(!empty($item->remarks))
                                <a href="#" class="view-remarks" data-toggle="modal" data-target="#viewRemarksModal" data-remarks="{{$item->remarks}}">View Remarks</a>
                                @else

                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8" style="border-top: transparent;">
                                @if ($item->specifications)
                                    <div><small><strong>Technical Specifications / Requirements</strong></small></div>
                                    {{ $item ->specifications}}
                                @endif
                            </td>
                        </tr>
                        {{--                                    @if (isset($_GET['revision']))--}}
                        {{--                                    <tr>--}}
                        {{--                                        <td colspan="7" style="border-top: transparent;">--}}
                        {{--                                            <div class="alert subtle">--}}
                        {{--                                                Change item--}}
                        {{--                                            </div>--}}
                        {{--                                        </td>--}}
                        {{--                                    </tr>--}}
                        {{--                                    @endif--}}

                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3"></th>
                            <th>Total Amount</th>
                            <th id="totalAmount">{{ number_format($pap->Items->sum('total_cost'), 2) }}</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>


                {{--                            @if ($_GET['dummystatus'] ?? '' == "draft")--}}
                {{--                                <div class="text-right">--}}
                {{--                                    <a href="{{ url('/dashboard/apb') }}" class="btn btn-primary"><i
                    class="fa fa-edit"></i> Edit</a>--}}
                {{--                                </div>--}}
                {{--                            @endif--}}

                {{-- <div class="text-right">
                                @if($pap->status == 'Approved')
                                    <a href="{{ url('/dashboard/apb') }}" class="btn btn-primary">Back to list</a>
                @else
                <a href="{{ url('/dashboard/apb') }}" class="btn btn-default">Cancel</a>
                <button id="" name="action" value="for_revision" class="btn btn-warning"
                    onclick="return confirm('Are you sure you want to save this as For Revision?')">For
                    Revision</button>
                <button id="" name="action" value="approve" class="btn btn-primary">
                    <i class="fa fa-send"></i>
                    Approve
                </button>
                @endif

            </div> --}}
        </div>
    </div>
</div>

</div>
</div>

<div class="modal fade" id="viewRemarksModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 80%; width: 700px;">
        <div class="modal-content">
            <div class="modal-header py-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="content"></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".view-remarks").click(function() {
            
            $("#viewRemarksModal .content").text($(this).data('remarks'));
        });
    });
</script>
@endsection()