<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePpmpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ppmp', function (Blueprint $table) {
            $table->dropColumn('pap_id');
            $table->string('general_description', 1024)->after('user_id');
            $table->string('esd', 512)->after('general_description');
            $table->string('mode_of_procurement', 512)->after('esd');
            $table->date('date')->after('mode_of_procurement');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ppmp');
    }
}
