<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterModeOfProcurementInPpmpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ppmp', function (Blueprint $table) {
            $table->dropColumn('mode_of_procurement');

            $table->integer('mode_of_procurement_id')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ppmp', function (Blueprint $table) {
            //
        });
    }
}
