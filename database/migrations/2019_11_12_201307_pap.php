<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pap', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('project_description')->nullable();
            $table->date('date')->nullable();
            $table->string('program', 255);
            $table->string('type', 16);
            $table->string('cluster', 255);
            $table->string('source_of_funds', 64)->nullable();
            $table->string('mooe_co', 16);
            $table->string('status', 16)->default('Draft');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pap');
    }
}
