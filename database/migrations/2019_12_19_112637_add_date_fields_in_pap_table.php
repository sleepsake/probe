<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateFieldsInPapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pap', function (Blueprint $table) {
            $table->date('advertisement')->nullable()->after('mooe_co');
            $table->date('submission_of_bids')->nullable()->after('advertisement');
            $table->date('notice_of_award')->nullable()->after('submission_of_bids');
            $table->date('contract_signing')->nullable()->after('notice_of_award');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pap', function (Blueprint $table) {
            //
        });
    }
}
