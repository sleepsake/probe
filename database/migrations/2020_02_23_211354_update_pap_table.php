<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pap', function (Blueprint $table) {
            $table->dropColumn('program');
            $table->dropColumn('type');
            $table->dropColumn('cluster');

            $table->integer('program_type_id')->after('id');
            $table->integer('program_id')->after('program_type_id');
            $table->integer('cluster_id')->after('program_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
