<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PapItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pap_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pap_id');
            $table->string('item_name');
            $table->integer('quantity');
            $table->decimal('unit_cost', 21);
            $table->decimal('total_cost', 21);
            $table->string('category', 64);
            $table->string('sub_category', 64);
            $table->string('for_procurement', 8);
            $table->string('spefications', 512)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pap_items');
    }
}
